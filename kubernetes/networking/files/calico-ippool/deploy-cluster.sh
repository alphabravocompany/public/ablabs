#!/usr/bin/env bash

echo -e "\n\nCreating Kubernetes Cluster\n=====\n"
kind create cluster --config kind-demo.yaml

echo -e "\n\nAdding Helm Repo\n=====\n"
helm repo add projectcalico https://docs.tigera.io/calico/charts

echo -e "\n\nAdding namespace\n=====\n"
kubectl create namespace tigera-operator

echo -e "\n\nInstall Calico using Helm\n=====\n"
helm install calico projectcalico/tigera-operator \
  --version v3.25.1 \
  --namespace tigera-operator

echo -e "\n\nWatch Pods with the following command:\n=====\nwatch kubectl get pods -n calico-system"

echo -e "\n\nList the IPPools:\n=====\nkubectl calico get ippool"

echo -e "\n\nDelete the default pool:\n=====\nkubectl calico delete ippool default-ipv4-ippool"

echo -e "\n\nAdd LABELS to Worker Nodes\n====="
echo -e "kubectl label nodes kind-worker color=blue"
echo -e "kubectl label nodes kind-worker2 color=green"

echo -e "\n\nApply the Updated IPPools:\n=====\nkubectl calico apply -f pool.yaml"

echo -e "\n\nApply the Deployment:\n=====\nkubectl apply -f deployment.yaml"