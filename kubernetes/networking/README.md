# Kubernetes Networking

## In this section we learned about:

* [Kubernetes Networking](https://kubernetes.io/docs/concepts/cluster-administration/networking/)
* [Kubernetes CNI](https://github.com/containernetworking/cni)
* [Kubernetes DNS](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/)
* [Calico CNI](https://docs.tigera.io/calico/latest/about/)

---

## Lab Overview

# Calico CNI

Kubernetes networking is a crucial aspect of managing container-based applications. It enables communication between different components within and outside the cluster.

In this lab we will use the Calico CNI plugin to configure the networking for our Kubernetes cluster.

---

## Lab Setup

We will be deploying a KIND cluster with Calico CNI. We are using Calico because it supports network policies, but note, not all CNIs support network policies. 

We have written this into a script, but let's take a look at the steps that are being performed.

`./files/calico-ippool/deploy-cluster.sh` {{ open }}

Note that we are using KIND to stand up a Kubernetes cluster and installing Calico CNI using a Helm chart.

Let's run the install script and change into the proper directory.

`cd /ab/labs/kubernetes/networking/files/calico-ippool && ./deploy-cluster.sh && cd /ab/labs/kubernetes/networking` {{ execute }}

We also need to watch for the cluster to be ready and for the Calico API pods to come online. Run the following command and watch for the `calico-apiserver` pods to show as `Running` and `Ready 1/1`.

`watch kubectl get pods -A` {{ execute }}

Press `CTRL+C` to exit the watch.

Now we can start working with Calico.

---

## Lab 1 - Network Policies

Kubernetes network policies are a way to control the flow of traffic between pods. They are a way to define rules that control the ingress and egress traffic to pods.

Calico implements these policies using iptables rules. Let's walk through setting up an environment to test this.

First, let's create a namespace for our test pods.

`kubectl apply -f ./files/calico-policy/01-namespace.yaml` {{ execute }}

Now, we will create a deployment with an `nginx` web server and a service to expose it.

Let's view the files we will be deploying.

`./files/calico-policy/02-deployment.yaml` {{ open }}

`./files/calico-policy/03-service.yaml` {{ open }}

Now, let's deploy the deployment and service.

`kubectl apply -f ./files/calico-policy/02-deployment.yaml` {{ execute }}

`kubectl apply -f ./files/calico-policy/03-service.yaml` {{ execute }}

Now, we will deploy 2 separate pods using the `busybox` image. Note the labels applied to these pod deployments - we will use these to apply network policies.

**Allowed Pod**

`./files/calico-policy/04-pod-busybox-allowed.yaml` {{ open }}

**Denied Pod**

`./files/calico-policy/04-pod-busybox-denied.yaml` {{ open }}

Now, let's deploy the pods.

`kubectl apply -f ./files/calico-policy/04-pod-busybox-allowed.yaml` {{ execute }}

`kubectl apply -f ./files/calico-policy/04-pod-busybox-denied.yaml` {{ execute }}

Let's verify that the pods are running and that we can see them all in the same namespace.

`kubectl get pods -n calico-demo` {{ execute }}

So we can see a before and after, let's run a `wget` test from each pod to the nginx service and see what happens.

**Allowed Pod**

`kubectl exec -it busybox-allowed -n calico-demo -- wget -qO- http://nginx-service` {{ execute }}

**Denied Pod**

`kubectl exec -it busybox-denied -n calico-demo -- wget -qO- http://nginx-service` {{ execute }}

Note that they are both able to successfully reach the nginx service. This is because we have not yet applied any network policies.

Let's take a look at the network policy we will be applying.

`./files/calico-policy/05-policy.yaml` {{ open }}

Now, let's apply the network policy and see how this affects the pods ability to reach the nginx service.

`kubectl apply -f ./files/calico-policy/05-policy.yaml` {{ execute }}

Now, let's run the same `wget` test from each pod to the nginx service and see what happens.

**Allowed Pod**

`kubectl exec -it busybox-allowed -n calico-demo -- wget -qO- http://nginx-service` {{ execute }}

**Denied Pod**

`kubectl exec -it busybox-denied -n calico-demo -- wget --timeout=10 -qO- http://nginx-service` {{ execute }}

Because we have applied a network policy that only allows traffic from pods without a matching label `app: busybox-allowed` as shown in the policy YAML, the `busybox-denied` is no longer able to reach the nginx service.

Let's remove that deployment, pods, and network policy and we can move on to the next lab.

`kubectl delete -f ./files/calico-policy/` {{ execute }}

---

## Lab 2 - IP Pools

In this lab we will work with assigning IP addresses to pods. We will create a new IP pool and assign pods to it.

First, we need to install the `calicoctl` CLI tool. This tool is used to manage certain Calico resources like IPPools.

`curl -L https://github.com/projectcalico/calico/releases/latest/download/calicoctl-linux-amd64 -o kubectl-calico && chmod +x ./kubectl-calico && sudo mv ./kubectl-calico /usr/local/bin` {{ execute }}

Next, lets take a look at the current IP pools.

`kubectl calico get ippool -o wide` {{ execute }}

We can see that there is a default IP pool with a CIDR of `192.168.0.0/16`. This is the IP pool that is used for pods that do not have a specific IP pool assigned to them.

Next, let's add labels to the worker nodes. Labels will define which IP Pools are assigned to the nodes.

`kubectl label nodes kind-worker color=blue` {{ execute }}

`kubectl label nodes kind-worker2 color=green` {{ execute }}

Let's look at the file to create 2 new IP pools. Note that they use the node labels we just added.

`./files/calico-ippool/pool.yaml` {{ open }}

Now, let's apply the new IP pools.

`kubectl apply -f ./files/calico-ippool/pool.yaml` {{ execute }}

We need to remove the default IP pool in order for the other pools to work as the default pool uses the `all()` selector.

`kubectl calico delete ippool default-ipv4-ippool` {{ execute }}

Now, let's take a look at the IP pools again.

`kubectl calico get ippool -o wide` {{ execute }}

We can see that we now have 2 new IP pools. One with a CIDR of `10.0.1.0/24` and another with a CIDR of `10.0.2.0/24`.

Now let's create a deployment to test the IP pools. There is nothing special about this file in terms of where the pods need to be deployed, but because of Kubernetes pod scheduling, we will see that the pods are deployed in an attempt to balance the nodes.

`./files/calico-ippool/deployment.yaml` {{ open }}

Now, let's apply the deployment.

`kubectl apply -f ./files/calico-ippool/deployment.yaml` {{ execute }}

Now, let's take a look at the pods and see where they were deployed and what IP addresses they were assigned.

`kubectl get pods -n calico-ippool-demo -o wide` {{ execute }}

Note the IPs of the pods deployed to each node. We can see that the pods were deployed to each node and that the IPs assigned to the pods are from the IP pools we created.

---

## Closing

In this lab we learned how to use Calico network policies to control the flow of traffic between pods. We also learned how to create and assign IP pools to nodes.

Let's remove the KIND cluster to cleanup the lab environment.

`kind delete cluster` {{ execute }}

---

### Congrats, you have completed the Calico Kubernetes Lab!