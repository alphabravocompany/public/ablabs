# Kubernetes Policies with OPA Gatekeeper

## In this section we learned about:

* [Kubernetes Admission Controllers](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/)
* [Kubernetes Dynamic Admission Control](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/)
* [Open Policy Agent](https://www.openpolicyagent.org/)
* [OPA Gatekeeper](https://open-policy-agent.github.io/gatekeeper/website/docs/)

---

## Lab Overview

# OPA Gatekeeper

OPA Gatekeeper is a Kubernetes-native policy engine that enforces custom policies and governance requirements on Kubernetes resources.

- Uses admission controller webhooks:
  - MutatingWebhookConfiguration
  - ValidatingWebhookConfiguration
- Uses custom resource definitions:
  - Constraints
  - Constraint Templates
  - Mutation Support
- Executed whenever a resource is created, updated, or deleted
- Enforces policies executed by Open Policy Agent
- Provides audit functionality
- Allows users to view objects violating policies

## Benefits of Using Gatekeeper
- Uses Open Policy Agent as the policy engine
- Designed to integrate seamlessly with Kubernetes
- Allows for fine-grained control over K8s resources
- Implemented as a Kubernetes controller
- Intercepts API requests for resource creation, modification, or deletion
- Validates against Rego policies
- Has a policy library to get you started

---

## Lab Setup

For these labs we will be using K3d, which is Rancher K3s running in Docker on your local lab server. It will appear as if there are multiple servers and it will act that way too, but they will actually be Docker containers running the K3s Kubernetes control plane and worker nodes.

To bring your local cluster online, run the following command:

`sudo systemctl stop haproxy && k3d cluster create lab-cluster --volume /ab/k3dvol:/tmp/k3dvol --api-port 16443 --servers 1 --agents 3 -p 80:80@loadbalancer -p 443:443@loadbalancer -p "30000-30010:30000-30010@server:0" && k3d kubeconfig write lab-cluster && cp /home/abtraining/.k3d/kubeconfig-lab-cluster.yaml ~/.kube/config && cd /ab/labs/kubernetes/policies/` {{ execute }}

Now, once the cluster create command completes, we can switch to that context using a `kubectl` command:

`kubectl config use-context k3d-lab-cluster` {{ execute }}

Get cluster info:

`kubectl cluster-info` {{ execute }}

--- 

## Lab 1 - Installing OPA Gatekeeper

We will use Helm to install OPA Gatekeeper. Helm is a package manager for Kubernetes. It allows you to easily install, upgrade, and manage applications on Kubernetes.

This command adds the `gatekeeper` Helm repository to your local Helm client.

`helm repo add gatekeeper https://open-policy-agent.github.io/gatekeeper/charts` {{ execute }}

This command installs the `gatekeeper` Helm chart from the `gatekeeper` repository.

`helm install gatekeeper/gatekeeper --name-template=gatekeeper --namespace gatekeeper-system --create-namespace` {{ execute}}

You can check the health of your installation by running the following command:

`kubectl get all  -n gatekeeper-system` {{ execute }}

--- 

## Lab 2 - Constraint Templates

Constraint Templates are the building blocks of Gatekeeper. They define the schema of a constraint and are used to create instances of that constraint.

- Provides expected input parameters
- Provides Rego necessary to enforce intent

**IMPORTANT NOTE:** A Constraint Template must be deployed before attempting to deploy a Constraint, as a Constraint relies on elements provided by the template.

First, let's list the Constraint Templates that are currently installed:

`kubectl get constrainttemplate` {{ execute }}

Now, let's take a look at a Constraint Template:

`./files/demo-namespace/01-template.yaml` {{ open }}

We can apply this Constraint Template to our cluster using the following command:

`kubectl apply -f ./files/demo-namespace/01-template.yaml` {{ execute }}

Now, let's list the Constraint Templates that are currently installed:

`kubectl get constrainttemplate` {{ execute }}

---

## Lab 3 - Constraints

A constraint is a declaration for a system to meet a given set of requirements.

**Important Note** The constraints API endpoint, `constraints.gatekeeper.sh/v1beta1`, will not exist until a constraint has been deployed. It is not part of the installation process.

Let's see what constraints are currently installed:

`kubectl get constraints` {{ execute }}

---

### Dry Run Constraints

A dry run is a way to test a constraint before it is enforced. This is useful for testing the constraint before it is applied to the cluster.

Let's tale a look at an example of a Dry Run Constraint:

`./files/demo-namespace/02-constraint-dry.yaml` {{ open }}

Let's apply this constraint to our cluster:

`kubectl apply -f ./files/demo-namespace/02-constraint-dry.yaml` {{ execute }}

Now, let's see what constraints are currently installed:

`kubectl get constraints` {{ execute }}

Let's look at the first namespace deployment file:

`./files/demo-namespace/03-namespace-1.yaml` {{ open }}

Now, let's deploy the first namespace:

`kubectl apply -f ./files/demo-namespace/03-namespace-1.yaml` {{ execute }}

The namespace deployment for the first namespace was successful with no warnings or errors.

Now, let's try to deploy a namespace that violates the dry run constraints:

First, let's look at the second namespace deployment file:

`./files/demo-namespace/03-namespace-2.yaml` {{ open }}

Now, let's deploy the second namespace:

`kubectl apply -f ./files/demo-namespace/03-namespace-2.yaml` {{ execute }}

The second namespace actually violates the dry run constraint, but because it is a dry run, no error is reported on the command line. We can see this by looking at the status of the constraint:

`kubectl get constraint ns-must-have-gk -o=jsonpath='{.status.violations}'` {{ execute }}

Let's remove these namespaces and move on the the next section:

`kubectl delete ns demo-namespace-1 demo-namespace-2` {{ execute }}

---

### Warning Constraints

A warning constraint is a way to notify the user that a constraint has been violated. This is useful for notifying the user that a constraint has been violated, but not enforcing it.

Let's take a look at an example of a Warning Constraint:

`./files/demo-namespace/02-constraint-warn.yaml` {{ open }}

Let's apply this constraint to our cluster:

`kubectl apply -f ./files/demo-namespace/02-constraint-warn.yaml` {{ execute }}

Now, let's see what constraints are currently installed:

`kubectl get constraints` {{ execute }}

Let's look at the first namespace deployment file:

`./files/demo-namespace/03-namespace-1.yaml` {{ open }}

Now, let's deploy the first namespace:

`kubectl apply -f ./files/demo-namespace/03-namespace-1.yaml` {{ execute }}

The namespace deployment for the first namespace was successful with no warnings or errors.

Now, let's try to deploy a namespace that violates the dry run constraints:

Let's look at the second namespace deployment file:

`./files/demo-namespace/03-namespace-2.yaml` {{ open }}

Now, let's deploy the second namespace:

`kubectl apply -f ./files/demo-namespace/03-namespace-2.yaml` {{ execute }}

The second namespace actually violates the warn constraint, and a warning is reported on the command line. We can also see this by looking at the status of the constraint:

`kubectl get constraint ns-must-have-gk -o=jsonpath='{.status.violations}'` {{ execute }}

Let's remove these namespaces and move on the the next section:

`kubectl delete ns demo-namespace-1 demo-namespace-2` {{ execute }}

### Required Constraints

A required constraint is a way to enforce a constraint. If the constraint is violated, the resource will not be created.

Let's take a look at an example of a Required Constraint:

`./files/demo-namespace/02-constraint.yaml` {{ open }}

Let's apply this constraint to our cluster:

`kubectl apply -f ./files/demo-namespace/02-constraint.yaml` {{ execute }}

Now, let's see what constraints are currently installed:

`kubectl get constraints` {{ execute }}

Let's look at the first namespace deployment file:

`./files/demo-namespace/03-namespace-1.yaml` {{ open }}

Now, let's deploy the first namespace:

`kubectl apply -f ./files/demo-namespace/03-namespace-1.yaml` {{ execute }}

The namespace deployment for the first namespace was successful with no warnings or errors.

Now, let's try to deploy a namespace that violates constraints without dry-run or warnings:

Let's look at the second namespace deployment file:

`./files/demo-namespace/03-namespace-2.yaml` {{ open }}

Now, let's deploy the second namespace:

`kubectl apply -f ./files/demo-namespace/03-namespace-2.yaml` {{ execute }}

Since this policy is set to required, the second namespace deployment will fail. We can see this on the command line as well as by looking at the status of the constraint:

`kubectl get constraint ns-must-have-gk -o=jsonpath='{.status.violations}'` {{ execute }}

Let's remove these namespaces and constraints and move on the the next section:

`kubectl delete -f ./files/demo-namespace` {{ execute }}

---

## Lab 4  - Mutation

Gatekeeper provides a mutation webhook that allows for the modification of resources before they are created. This is useful for adding default values to resources, or for adding labels to resources.

In this instance, we will be removing privileged containers from our cluster. This is done by adding a `securityContext` to the pod spec that sets `privileged` to `false`.

Let's take a look at an example of a Mutation Constraint:

`./files/demo-mutation/01-assign.yaml` {{ open }}

Note the specific namespace and pod names in the `match` section. This is because we only want to apply this mutation to the `demo-mutation-1` namespace, and only to pods that with the name `nginx`.

Let's apply this mutation to our cluster:

`kubectl apply -f ./files/demo-mutation/01-assign.yaml` {{ execute }}

Now, let's see what mutations are currently installed:

`kubectl get assign` {{ execute }}

Next, lets deploy the `demo-mutation-1` namespace:

`kubectl apply -f ./files/demo-mutation/02-namespace.yaml` {{ execute }}

We can now take a look at the `nginx` pod deployment file:

`./files/demo-mutation/03-pod.yaml` {{ open }}

Note that the pod is specifically set to `privileged: true`.

Now, let's deploy the `nginx` pod into that namespace:

`kubectl apply -f ./files/demo-mutation/03-pod.yaml` {{ execute }}

This was deployed successfully, but let's take a look at the pod spec as it is running in the cluster:

`kubectl -n demo-mutation-1 get pod nginx -o=jsonpath='{.spec.containers[0].securityContext}'` {{ execute }}

You can see that even though the pod was deployed with `privileged: true`, the pod spec was modified to `privileged: false`.

---

## Closing

You have completed the Kubernetes Policies with OPA Gatekeeper Lab. Click the execute button below to clean up your cluster.

`k3d cluster delete lab-cluster && rm ~/.kube/config` {{ execute }}

### Congrats! Kubernetes Policies with OPA Gatekeeper Lab.