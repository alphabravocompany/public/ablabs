#!/usr/bin/env bash

echo -e "\nk -n demo-mutation-1 get pod nginx -o=jsonpath='{.spec.containers[0].securityContext}'"
getOutput=`kubectl -n demo-mutation-1 get pod nginx -o=jsonpath='{.spec.containers[0].securityContext}'`
echo "$getOutput"
