# Kubernetes RBAC

## In this section we learned about:

* [Kubernetes RBAC](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)
* [Kubernetes API](https://kubernetes.io/docs/concepts/overview/kubernetes-api/)

---

### Lab Pre-Prep

Before we run the next commands, we need to make sure that the ports are available on this server. 

Run the following command to release ports 80/443 so we can use Kubernetes ingress on those ports.

`sudo systemctl stop haproxy` {{ execute }}

---

## Lab Overview

### Role-based Access Control
Role-Based Access Control (RBAC) is a policy-neutral access-control mechanism defined around roles and privileges. The components of RBAC such as role-permissions, user-role and role-role relationships make it simple to perform user assignments.

### Why is RBAC Important?
- Allows system sto protect data and business processes
- Uses set rules and roles
- Allows administrators to specify types of actions
- Actions are permitted to a user based on assigned roles

### Benefits of RBAC
- Security
- Selective Access
- Organizational Structure
- Separation of Duties
- Flexibility

---

## Lab Setup

For these labs we will be using K3d, which is Rancher K3s running in Docker on your local lab server. It will appear as if there are multiple servers and it will act that way too, but they will actually be Docker containers running the K3s Kubernetes control plane and worker nodes. Cool huh?

To bring your local cluster online, run the following command:

`k3d cluster create lab-cluster --volume /ab/k3dvol:/tmp/k3dvol --api-port 16443 --servers 1 --agents 3 -p 80:80@loadbalancer -p 443:443@loadbalancer -p "30000-30010:30000-30010@server:0" && k3d kubeconfig write lab-cluster && cp /home/abtraining/.k3d/kubeconfig-lab-cluster.yaml ~/.kube/config` {{ execute }}

Now, once the cluster create command completes, we can switch to that context using a `kubectl` command:

`kubectl config use-context k3d-lab-cluster` {{ execute }}

Get cluster info:

`kubectl cluster-info` {{ execute }}

View nodes in the cluster:

`kubectl get nodes -o wide` {{ execute }}

---

## Lab 1 - Viewing API configurations

Let's view all API resources available in Kubernetes. Running the below command will get a list of all API groups and resources in a K8s cluster.

`kubectl api-resources -o wide` {{ execute }}

Note that there are numerous API groups, all with different verbs associated with them. We will be focusing on the `rbac.authorization.k8s.io/v1` sections in this lab.

We can also scope the above search to only show API resources associated specifically with RBAC.

`kubectl api-resources --api-group=rbac.authorization.k8s.io -o wide` {{ execute }}

Not that there are 4 different kinds listed. In the next sections, we will work with each of these RBAC Kinds.

---

## Lab 2 - RBAC Roles and ClusterRoles

Roles and ClusterRoles are critical components of Kubernetes security, allowing administrators to define fine-grained permissions and access controls to Kubernetes resources based on the specific needs of their organization.

First, let's create a namespace for this lab, a container in that namespace, and change to the proper dir for the labs.

`kubectl create ns rbac-lab && kubectl run nginx --image=nginx --restart=Never --namespace=rbac-lab && cd /ab/labs/kubernetes/security/` {{ execute }}

---

### Roles

### Role

- Sets permissions within a particular namespace.
- A namespace must be specified.
- Contains rules that represent a set of permissions within a namespace.
- Permissions are purely additive. There are no "deny" rules.

Since we created the namespace `rbac-lab`, we can apply a `role` specifically to this namespace.

First, let's see how to create this from the command-line. This is just so you can become familiar with how to do this, but in practice these should be written into Kubernetes manifest files and committed to a code repo.

`kubectl create role pod-reader-cli --verb=get --verb=list --verb=watch --resource=pods -n rbac-lab` {{ execute }}

Now, let's take a look at how the `role` yaml is written.

`./files/role.yaml` {{ open }}

Now we can apply this specifically to the `rbac-lab` namespace.

`kubectl apply -f ./files/role.yaml` {{ execute }}

This creates a role called `pod-reader` in the namespace `rbac-lab` that can only get, watch, and list the `pods` in the namespace, but not perform any of the other `verbs` listed for the `role` API.

We can view these 2 roles in the `rbac-lab` namespace by running the following:

`kubectl get roles -n rbac-lab` {{ execute }}

By modifying which `verbs` are applied, you can very tightly scope the capabilities that a user or process has inside a specific namespace in a cluster.

---

### ClusterRole

- Sets permissions cluster-wide.
- No namespace must be specified.
- Contains rules that represent a set of permissions cluster-wide.
- Permissions are purely additive. There are no "deny" rules.

Just as in the previous lab, let's create a `clusterrole` via the command line.

`kubectl create clusterrole secret-reader-cli --verb=get --verb=list --verb=watch --resource=secrets` {{ execute }}

Now, let's take a look at how the `clusterrole` yaml is written.

`./files/clusterrole.yaml` {{ open }}

Now we can apply this specifically to the `rbac-lab` namespace.

`kubectl apply -f ./files/clusterrole.yaml` {{ execute }}

This creates a cluster role called `secret-reader` that can read secrets across the entire cluster, but not perform any of the other `verbs` listed for the `clusterrole` API.

We can view these 2 cluster roles in the cluster by running the following:

`kubectl get clusterroles -n rbac-lab` {{ execute }}

By modifying which `verbs` are applied, you can very tightly scope the capabilities that a user or process has inside a specific namespace in a cluster.

---

## Lab 3 - RBAC Accounts

In Kubernetes, users are typically created and managed through an authentication mechanism such as a certificate authority (CA), a user database, or an external identity provider like LDAP or OAuth. In this lesson, we will discuss the creation of user accounts and ServiceAccounts.

### ServiceAccount
- Used for application processes which are part of pods
- Designed for specific tasks
- Follows principal of least privilage
- Non-interactive
- Namespaced
- Lightweight creation
- Easy to audit
- Portable
- Designed for scale
- Requires a token for API access

Let's take a look at how the `serviceaccount` yaml is written.

`./files/serviceaccount.yaml` {{ open }}

Now we can apply this to the cluster.

`kubectl apply -f ./files/serviceaccount.yaml` {{ execute }}

We can view this `serviceaccount` using the following:

`kubectl get serviceaccount` {{ execute }}

### User Accounts

- Intended to be global vs namespaced
- Interactive account
- Complex creation
- Created explicitly for individual users
- Requires an authentication mechanism to verify identity
- Access control is manged via RBAC
- Assigned via roles
- Stored in the Kubeconfig file

It is important to note that a User is different than a service account. Therefore, we need some additional processes to create a user.

For simplicity, we will use a bash script to create the user cert, certificate signing request, and then submit it to K8s for approval.

First, let's look at this file:

`./files/create-jane-cert.sh` {{ open }}

Now, let's run this file to generate the certs and submit the CSR.

`./files/create-jane-cert.sh` {{ execute }}

Next, let's see the pending CSR in Kubernetes:

`kubectl get csr` {{ execute }}

Now, we can approve this CSR.

`kubectl certificate approve jane` {{ execute }}

And then we verify that the CSR is approved and issued.

`kubectl get csr` {{ execute }}

Now that is done, let's add this user to the local Kubeconfig located at `~/.kube/config`.

In the below command, we export the certificate out of K8s and into the file `jane.crt`

`kubectl get csr jane -o jsonpath='{.status.certificate}'  | base64 -d > jane.crt` {{ execute }}

Next we add the credentials for `jane` into the `~/.kube/config` file.

`kubectl config set-credentials jane --client-key=jane.key --client-certificate=jane.crt --embed-certs=true` {{ execute }}

Now we create the `context` for `jane` in the `~/.kube/config` file.

`kubectl config set-context jane --cluster=k3d-lab-cluster --user=jane` {{ execute }}

Now, let's switch to the context `jane`.

`kubectl config use-context jane` {{ execute }}

And let's see if `jane` can see the pods running in the `rbac-lab` namespace.

`kubectl get pods -n rbac-lab` {{ execute }}

You should receive the below error because while the user has Authenticated to the cluster, they are not Authorized to do anything yet.

`Error from server (Forbidden): pods is forbidden: User "system:serviceaccount:rbac-lab:jane" cannot list resource "pods" in API group "" in the namespace "rbac-lab`

Let's change context back to the admin user and in the next lab we will add RoleBindings to the user `jane` so she can see pods in that namespace.

`kubectl config use-context k3d-lab-cluster` {{ execute }}


---

## Lab 4 - RoleBinding and ClusterRoleBinding

RoleBindings and ClusterRoleBindings are used to associate Roles and ClusterRoles with specific users or groups of users, allowing them to access Kubernetes resources.

- Used to grant or restrict access to Kubernetes resources
- Ensure that users only have access to the resources they need to perform their intended tasks
- Proper configuration is an important aspect of Kubernetes security and access control.

### RoleBinding
- May reference any Role within a given namespace.
- Grants the permissions defined in a Role to a list of subjects:
  - Users
  - Groups
  - Service accounts
- Acts as the glue between subjects and Roles.
- Subjects only have permissions within the given namespace.
- Can reference a ClusterRole and bind it to the RoleBinding namespace.

As we saw before, the user `jane` is Authenticated to the cluster, but cannot do anything. 

Let's take a look at the RoleBinding YAML that will allow `jane` to view the pods in the `rbac-lab` namespace.

`./files/role-binding.yaml` {{ open }}

Now, lets apply that file and switch to the `jane` user to see what is different.

`kubectl apply -f ./files/role-binding.yaml` {{ execute }}

Now, let's switch to the context `jane`.

`kubectl config use-context jane` {{ execute }}

And let's see if `jane` can see the pods running in the `rbac-lab` namespace.

`kubectl get pods -n rbac-lab` {{ execute }}

You should see something similar to the following because `jane` has now been Authorized to view the pods:

```
NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          5m31s
```

Now, let's change back to the admin context and continue with the lab.

`kubectl config use-context k3d-lab-cluster` {{ execute }}

### ClusterRoleBinding
- Allows binding a ClusterRole to all namespaces in a cluster.
- Grants the permissions defined in a ClusterRole to a list of subjects:
  - Users
  - Groups
  - Service accounts
- Acts as the glue between subjects and ClusterRoles.
- Subjects have permissions cluster-wide.
- Possible security issue. ClusterRoleBindings should be reviewed regularly.

In this section, will apply the a ClusterRoleBinding that allows `jane` to use the ClusterRole `secret-reader` we added in Lab 2.

First, lets take a look at the YAML file.

`./files/cluster-role-binding.yaml` {{ open }}

Now, let's apply this role.

`kubectl apply -f ./files/cluster-role-binding.yaml` {{ execute }}

Previously, `jane` could only see the pods in a specific namespace, but now `jane` should also be able to read the secrets across the entire cluster.

First, lets create a secret to be read:

`kubectl create secret generic rbac-lab-secret --from-file=./files/secret.txt --namespace=rbac-lab` {{ execute }}

Let's change to the `jane` context.

`kubectl config use-context jane` {{ execute }}

And let's see if `jane` can list and view secrets on the cluster.

`kubectl get secrets -A` {{ execute }}

Running the below command should show you the decoded secret `rbac-lab-secret`.

`kubectl get secret rbac-lab-secret -n rbac-lab -o json | jq '.data' | jq 'map_values(@base64d)'` {{ execute }}

## Closing

You have completed the Kubernetes RBAC Lab. Click the execute button below to clean up your cluster.

`k3d cluster delete lab-cluster && rm ~/.kube/config` {{ execute }}

### Congrats! You have completed the Kubernetes RBAC Lab.