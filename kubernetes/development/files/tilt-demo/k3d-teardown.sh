#!/usr/bin/env bash

k3d cluster list
k3d cluster delete demo
k3d cluster list
k3d registry list
k3d registry delete demo.localhost
k3d registry list