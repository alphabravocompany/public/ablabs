#!/usr/bin/env bash

docker stop registry
k3d registry list
k3d registry create demo.localhost --port 5000
k3d registry list
k3d cluster list
k3d cluster create demo --registry-use k3d-demo.localhost:5000
k3d cluster list