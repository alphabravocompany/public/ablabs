# Development Tools for Developing in Kubernetes

## In this section we learned about:

* [Tilt](https://tilt.dev/)
* [Skaffold](https://skaffold.dev/)

---

## Lab Overview

Developing applications around Kubernetes involves creating, deploying, and managing containerized applications within a Kubernetes cluster.

We will be using Tilt and Skaffold to help us with this process.

---

## Lab Setup

We will exposing the Tilt and Skaffold tools to our local system via a web browser. We have pre-configured HAProxy to route traffic to the Tilt and Skaffold.

Click below to start HAProxy:

`sudo systemctl start haproxy` {{ execute }}

## Lab 1 - Tilt

Tilt is a tool for developing Kubernetes applications. It provides a web UI that allows you to see your application's status and logs, and make changes to your code and see the results in real time.

First, let's deploy a K3d cluster for Tilt to use:

`cd /ab/labs/kubernetes/development && ./files/tilt-demo/k3d-setup.sh && cd /ab/labs/kubernetes/development/files/tilt-demo` {{ execute }}

Next, install Tilt on the local system:

`curl -fsSL https://raw.githubusercontent.com/tilt-dev/tilt/master/scripts/install.sh | bash` {{ execute }}

Next, we will take a look at the files we will be using for this lab:

**Dockerfile** - This is just a simple Dockerfile using nodejs and live-server to serve a static HTML file and it exposing port 8000.

`./files/tilt-demo/Dockerfile` {{ open }}

**Kubernetes Deployment** - This is the deployment file that tilt will use to deploy our application to the cluster.

`./files/tilt-demo/k8s-pod.yaml` {{ open }}

**Tiltfile** - The Tiltfile is the heart of Tilt's configuration, serving as the blueprint for how Tilt should manage a project. Note that is automatically building the Dockerfile, pushing it to the local registry (which is running on the local system so it does not try to push to Docker Hub), deploying the Kubernetes manifest to the cluster, and exposing port 8000 to the local system.

`./files/tilt-demo/Tiltfile` {{ open }}

Now that we have all these files in place, let's start Tilt:

`tilt up` {{ execute }}

Visit https://tilt.LABSERVERNAME/r/(all)/overview to see the Tilt UI.

In Tilt, we can see when application is running in the cluster and we can see the logs for the application.

Watch the logs in the Tilt terminal. Wait for Tilt to show the following message to let you know that Tilt has finished building the application and is serving it:

```
Serving "/app" at http://127.0.0.1:8000
Ready for changes
```

Once you see the above message, you can access the application in the browser here: https://8000.LABSERVERNAME

Let's make a change to the application. Open the index.html file and change the text to "Hello World!".

`./files/tilt-demo/index.html` {{ open }}

Go back to the Tilt and you can see that it automatically detected the change and is rebuilding the application. Once the application is rebuilt, we can see the change in the browser. 

*NOTE*: You may need to refresh the browser to see the change as we are passing this through a proxy and it may be caching the response.

We are done with the Tilt Lab. Hit `CTRL + C` in the terminal to stop Tilt.

Let's clean up the cluster:

`./k3d-teardown.sh` {{ execute }}

---

## Lab 2 - Skaffold

Skaffold streamlines the process of building, deploying, and iterating on applications in Kubernetes.

- Automates repetitive tasks
- Provides a consistent development environment
- Command line tool
- IDE integration

First, let's deploy a K3d cluster for Skaffold to use:

`cd /ab/labs/kubernetes/development && ./files/skaffold-demo/k3d-setup.sh && cd /ab/labs/kubernetes/development/files/skaffold-demo` {{ execute }}

Now, we need to install Skaffold on the local system:

`curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64 && sudo install skaffold /usr/local/bin/` {{ execute }}

Next, we will take a look at the `skaffold.yaml` file that is the central configuration file for Skaffold:

`./files/skaffold-demo/skaffold.yaml` {{ open }}

Much like Tilt, Skaffold is using this file to define what files it should watch for changes, what container image to build, and what Kubernetes manifest to deploy.

The Dockerfile and Kubernetes manifest are similar to the ones we used in the Tilt lab.

`./files/skaffold-demo/Dockerfile` {{ open }}

`./files/skaffold-demo/k8s-pod.yaml` {{ open }}

Now that we have all these files in place, let's start Skaffold:

`skaffold dev` {{ execute }}

Skaffold will build the container image and deploy the Kubernetes manifest to the cluster.

Watch the terminal for the following message to let you know that Skaffold has finished building the application and is serving it:

```
[ab-skaffold] Serving "/app" at http://127.0.0.1:8000
[ab-skaffold] Ready for changes
```

Once you see the above message, you can access the application in the browser here: https://8000.LABSERVERNAME

Let's make a change to the application. Open the index.html file and change the text to "Hello World!".

`./files/skaffold-demo/index.html` {{ open }}

You can see that Skaffold detected the change and is rebuilding the application. Once the application is rebuilt, we can see the change in the browser.

*NOTE*: You may need to refresh the browser to see the change as we are passing this through a proxy and it may be caching the response.

We are done with the Skaffold Lab. Hit `CTRL + C` in the terminal to stop Skaffold.

Let's clean up the cluster:

`./k3d-teardown.sh` {{ execute }}

---

### Congrats! Development Tools for Developing in Kubernetes Lab.
