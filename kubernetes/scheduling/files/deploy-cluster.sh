#!/usr/bin/env bash

echo -e "\n\nCreating Kubernetes Cluster\n=====\n"
kind create cluster --config ./files/kind-demo.yaml

echo -e "\n\nAdding the Namespace\n=====\n"
kubectl apply -f ./files/01-namespace.yaml