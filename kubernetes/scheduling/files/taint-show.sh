#!/usr/bin/env bash

echo -e "\n\nShow Taints for Marketing\n=====\n"
kubectl get nodes kind-worker -o jsonpath='{range .spec.taints[*]}{.key}={.value}:{.effect}{"\n"}{end}'

echo -e "\n\nShow Taints for Engineering\n=====\n"
kubectl get nodes kind-worker2 -o jsonpath='{range .spec.taints[*]}{.key}={.value}:{.effect}{"\n"}{end}'