#!/usr/bin/env bash

kubectl get nodes \
  --selector='!node-role.kubernetes.io/control-plane' \
  -o jsonpath='
      {range .items[*]}
        {"\n - "}{.metadata.name}{":"}
          {"Team: "}{.metadata.labels.team}
          {"Env: "}{.metadata.labels.environment}
          {"Region: "}{.metadata.labels.region}
      {end}'