#!/usr/bin/env bash

echo -e "\n\nRemoving Taints\n=====\n"
kubectl taint nodes kind-worker team=marketing:NoSchedule-
kubectl taint nodes kind-worker2 team=engineering:NoSchedule-