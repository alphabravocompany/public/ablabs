#!/usr/bin/env bash

echo -e "\n\nSet a Taint for Marketing\n=====\n"
kubectl taint nodes kind-worker team=marketing:NoSchedule

echo -e "\n\nSet a Taint for Engineering\n=====\n"
kubectl taint nodes kind-worker2 team=engineering:NoSchedule