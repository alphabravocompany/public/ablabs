# Kubernetes Scheduling

## In this section we learned about:

* [Kubernetes Scheduling](https://kubernetes.io/docs/concepts/scheduling-eviction/)

---

## Lab Concepts Overview

## Scheduling in Kubernetes
Scheduling is a key aspect of Kubernetes, as it is responsible for determining which Node should run a particular Pod. The Kubernetes scheduler analyzes various factors to make informed decisions about where to place Pods in a cluster.

### The Kubernetes Scheduler
The Kubernetes scheduler is a component of the Kubernetes control plane, which manages the overall state of the cluster.

- Is one of several components that make up the Kubernetes control plane
- Runs on the control plane Node in a Pod
- Runs as a single instance, but is designed to be deployed in a highly available state
- Uses a leader election system to maintain an active/standby configuration in HA
- Can be fine-tuned via configuration or command-line flags

### Scheduler Benefits
- Continuously watches the API for new/updated Pods
- Uses algorithmic decision making for Pod placement
- Provides optimal resource utilization
- Allows for cluster high availability
- Cluster scalability
- Affinity and Anti-affinity rules
- Maintainability of the cluster
- Customization for extending or replacing the scheduler

### Key Scheduling Concepts
There are several key concepts related to the scheduler which users should be aware:

- Nodes
- Pods
- Resource Requests and Limits
- Quality of Service Classes
- Affinity and Anti-Affinity for Nodes and Pods
- Taints and Tolerations
- Labels
- Selectors
- Priority and Pre-emption
- Pod Disruption Budgets

---

## Lab Setup

For these labs we will be using Kind. Kind is a tool for running local Kubernetes clusters using Docker container “nodes”.
kind was primarily designed for testing Kubernetes itself, but may be used for local development or CI.

To bring your local cluster online, run the following command:

`cd /ab/labs/kubernetes/scheduling && ./files/deploy-cluster.sh` {{ execute }}

Get cluster info:

`kubectl cluster-info` {{ execute }}

View nodes in the cluster:

`kubectl get nodes -o wide` {{ execute }}

---

## Lab 1 - Viewing Cluster Config

The Kind cluster was deployed from a YAML file with some specific labels applied. Click open below to view the file.

`./files/kind-demo.yaml` {{ open }}

Note the labels are different for each of the workers. This will come into play when we are applying specific configurations that do and do not tolerate those labels.

---

## Lab 2 - Creating pods with nodeName spec

In this lab, we will deploy 2 nginx pods, one to each worker as defined by the "spec.nodeName" field. 

First let's create a `namespace`

`kubectl apply -f ./files/01-namespace.yaml` {{ execute }}

Click below to view the deployment file. Note that each pod is specified to run on a different worker node.

`./files/02-nodename.yaml` {{ open }}

Let's apply this deployment.

`kubectl apply -f ./files/02-nodename.yaml` {{ execute }}

To watch the deployment happen, run:

`watch -n1 kubectl get pods -n schedule-demo -o=custom-columns=NAME:.metadata.name,NODE:.spec.nodeName,STATUS:.status.phase` {{ execute }}

Press "CTRL + C" in the terminal window to exit the watch.

Now we can view these pods running on each of the worker nodes:

`kubectl get pods -n schedule-demo -o wide` {{ execute }}

Let's remove these pods:

`kubectl delete -f ./files/02-nodename.yaml` {{ execute }}

---

## Lab 2 - Creating pods with nodeSelector

In this lab, we will deploy 2 nginx pods, one to each worker as defined by the "spec.nodeSelector" field. This will look for a specific label on the node and deploy to the appropriate node.

Click below to view the deployment file. Note that each pod is specified to run on a different worker node.

`./files/03-nodeSelector.yaml` {{ open }}

Let's apply this deployment.

`kubectl apply -f ./files/03-nodeSelector.yaml` {{ execute }}

To watch the deployment happen, run:

`watch -n1 kubectl get pods -n schedule-demo -o=custom-columns=NAME:.metadata.name,NODE:.spec.nodeName,STATUS:.status.phase` {{ execute }}

Press "CTRL + C" in the terminal window to exit the watch.

Now we can view these new pods running on each of the worker nodes based on the nodeSelector labels:

`kubectl get pods -n schedule-demo -o wide` {{ execute }}

We can now remove those pods.

`kubectl delete -f ./files/03-nodeSelector.yaml` {{ execute }}

---

## Lab 3 - Tainting Nodes and deploying with a "bad" configuration

In this lab, we will taint the worker nodes with the "NoSchedule" label, not allowing them to run any workloads.

View the file we will use to add this label here:

`./files/taint-set.sh` {{ open }}

Let's apply these taints:

`./files/taint-set.sh` {{ execute }}

Now, let's look at K8s YAML that does not work with these taints in place. Note that there is nothing in here that specifies "NoSchedule" is tolerated.

`./files/04-toleration-bad.yaml` {{ open }}

Let's apply this and see what happens.

`kubectl apply -f ./files/04-toleration-bad.yaml` {{ execute }}

We can watch and see that these pods are stuck in a "Pending" state.

`watch -n1 kubectl get pods -n schedule-demo -o=custom-columns=NAME:.metadata.name,NODE:.spec.nodeName,STATUS:.status.phase` {{ execute }}

Press "CTRL + C" in the terminal window to exit the watch.

Let's remove these pods:

`kubectl delete -f ./files/04-toleration-bad.yaml` {{ execute }}

---

## Lab 4 - Tainting Nodes and deploying with a "good" configuration

Now, let's look at K8s YAML that does WILL work with these taints in place. This is because the taint is tolerated in the config.

`./files/04-toleration-good.yaml` {{ open }}

Let's apply this and see what happens.

`kubectl apply -f ./files/04-toleration-good.yaml` {{ execute }}

We can watch and see that these pods are able to deploy because they are tolerant of the `NoSchedule` taint.

`watch -n1 kubectl get pods -n schedule-demo -o=custom-columns=NAME:.metadata.name,NODE:.spec.nodeName,STATUS:.status.phase` {{ execute }}

Press "CTRL + C" in the terminal window to exit the watch.

Let's remove these pods and the taints for now:

`kubectl delete -f ./files/04-toleration-good.yaml && ./files/taint-remove.sh` {{ execute }}

---

## Lab 5 - Working with nodeAffinity

Node affinity is conceptually similar to `nodeSelector`, where the scheduler takes into account user-defined Node affinity rules which attract Pods to specific Nodes based on their labels.

### Using "Preferred"

First, lets open a new terminal to watch what is happening.

Start by clicking the "[|]" box in the terminal window to "Split Terminal"

Then run the following command:

`watch -n1 kubectl get pods -n schedule-demo -o=custom-columns=NAME:.metadata.name,NODE:.spec.nodeName,STATUS:.status.phase` {{ execute "T2" }}

Now, lets view the file that uses "preferred" `nodeAffinity`.

`./files/05-nodeAffinity-preferred.yaml` {{ open }}

Let's apply that file and see what happens.

`kubectl apply -f ./files/05-nodeAffinity-preferred.yaml` {{ execute "T1" }}

Note in the "watch" terminal that the pods are deployed to both worker nodes.

Let's delete these pods for now.

`kubectl delete -f ./files/05-nodeAffinity-preferred.yaml` {{ execute }}

Now, let's taint one of the nodes and try the deployment again.

`kubectl taint nodes kind-worker2 team=engineering:NoSchedule` {{ execute "T1" }}

`kubectl apply -f ./files/05-nodeAffinity-preferred.yaml` {{ execute "T1" }}

Note in the "watch" windows that because only defined a "preferred" node, both pods will still deploy to a single node.

Let's un-taint the node and remove these pods for now. We can keep the "watch" terminal open as we will be using it in the next lab.

`kubectl taint nodes kind-worker2 team=engineering:NoSchedule- && kubectl delete -f ./files/05-nodeAffinity-preferred.yaml` {{ execute "T1" }}

### Using "Required"

Let's view the file that uses "required" `nodeAffinity`.

`./files/05-nodeAffinity-required.yaml` {{ open }}

Let's apply that file and see what happens.

`kubectl apply -f ./files/05-nodeAffinity-required.yaml` {{ execute "T1" }}

Note in the "watch" terminal that the pods are deployed to both worker nodes.

Let's delete these pods then taint the nodes.

`kubectl delete -f ./files/05-nodeAffinity-required.yaml` {{ execute "T1" }}

`kubectl taint nodes kind-worker2 team=engineering:NoSchedule` {{ execute "T1" }}

Let's apply that file and see what happens with the taint in place.

`kubectl apply -f ./files/05-nodeAffinity-required.yaml` {{ execute "T1" }}

Note that only one of the pods can deploy due to the "required" `nodeAffinity` rule. The other pod stays as "Pending".

Let's remove the taint.

`kubectl taint nodes kind-worker2 team=engineering:NoSchedule-` {{ execute }}

You can see that as soon as we remove the taint, the pod is able to be scheduled on its target node and goes to a "Running" state.

We can remove these pods and move on to the next section. We can keep the "watch" terminal open as we will be using it in the next lab.

`kubectl delete -f ./files/05-nodeAffinity-required.yaml` {{ execute }}

### Using "Soft Weights"

Let's view the file that uses "soft weights" with `nodeAffinity`.

`./files/05-nodeAffinity-soft-weight.yaml` {{ open }}

Let's apply that file and see what happens.

`kubectl apply -f ./files/05-nodeAffinity-soft-weight.yaml` {{ execute "T1" }}

Note in the "watch" terminal that the pod is deployed as expected to its higher weighted node.

Let's delete these pods then taint the nodes.

`kubectl delete -f ./files/05-nodeAffinity-soft-weight.yaml` {{ execute "T1" }}

`kubectl taint nodes kind-worker team=marketing:NoSchedule` {{ execute "T1" }}

Let's apply that file and see what happens with the taint in place.

`kubectl apply -f ./files/05-nodeAffinity-soft-weight.yaml` {{ execute "T1" }}

Note that because the other lower weighted preference was matched, the pod was still able to deploy.

Let's remove the taint.

`kubectl taint nodes kind-worker team=marketing:NoSchedule-` {{ execute }}

You can see that as soon as we remove the taint... nothing happens. Kubernetes will not try to reschedule the pod to its higher weighted preferred node until the pods has an issue and needs to be restarted, or a user or some other process deletes and redeploys the pod.

We can remove these pods and move on to the next section.

`kubectl delete -f ./files/05-nodeAffinity-soft-weight.yaml` {{ execute }}

---

## Lab 6 - Working with Pod Resources

Let's view the file that we will use for this lab.

`./files/06-pod-resources.yaml` {{ open }}

Note that there are 3 different sections. Only use one of the "Guaranteed" or "Burstable" configs in your pod specs. You can also deploy with no resource specification, but we recommend that you don't do that. It is best practice to always define resource specs.

- Guaranteed: This section assures that the pod will immediately get and only be able to use up to what memory and CPU are defined because they are the same.
- Burstable: This section assures that the pod will have at least `100m` cpu and `256Mi` memory, but may burst up to `500m` cpu and `1Gi` memory.
- Not Meeting Requirements: The last one is to show that because no nodes meet that huge requirements defined, so the pod will never by scheduled.

Try uncommenting each of the 3 sections and applying to see the result. When you run the `describe` command, make note of the `containers` section and the `Events` section to see what is going on. Make sure you only uncomment one of the 3 sections at a time.

Apply the config:

`kubectl apply -f ./files/06-pod-resources.yaml` {{ execute "T1" }}

View the pod specs:

`kubectl describe pods marketing-nginx -n schedule-demo` {{ execute "T1" }}

Delete the pod, edit the file, rinse and repeat.

`kubectl delete -f ./files/06-pod-resources.yaml` {{ execute "T1" }}

---

## Lab 7 - Working with priorityClass

- [PriorityClass](https://kubernetes.io/docs/concepts/scheduling-eviction/pod-priority-preemption/#priorityclass)

A PriorityClass is a non-namespaced object that defines a mapping from a priority class name to the integer value of the priority. The name is specified in the `name` field of the PriorityClass object's metadata. The value is specified in the required `value` field. The higher the value, the higher the priority. The name of a PriorityClass object must be a valid DNS subdomain name, and it cannot be prefixed with `system-`.

For this lab, we will simply view what this file looks like as we don't want to intentially try to overload this cluster and force it to choose to run only pods with the higher priority.

`./files/07-priorityClass.yaml` {{ open }}

---

## Lab 8 - Pod Affinity

Node affinity is conceptually similar to nodeSelector, allowing you to constrain which nodes your Pod can be scheduled on based on node labels. There are two types of node affinity:

Lets take a look at the following file.

`./files/08-podAffinity.yaml` {{ open }}

Let's deploy it to see where the pods get placed.

`kubectl apply -f ./files/08-podAffinity.yaml` {{ execute "T1" }}

Note that they all are deployed on `kind-worker`.

Let's delete the deployment.

`kubectl delete -f ./files/08-podAffinity.yaml` {{ execute "T1" }}

Now, update the `08-podAffinity.yaml` file, commenting out `nodeName: kind-worker` and uncommenting `nodeName: kind-worker2`. Save the file and apply again.

`kubectl apply -f ./files/08-podAffinity.yaml` {{ execute "T1" }}

Now all pods should be deployed on `kind-worker2`.

Let's delete the deployment and move to the next lab.

`kubectl delete -f ./files/08-podAffinity.yaml` {{ execute "T1" }}

---

## Lab 9 - Pod AntiAffinity

Anti-affinity allow you to constrain which nodes your Pods can be scheduled on based on the labels of Pods already running on that node, instead of the node labels.

Lets take a look at the following file.

`./files/09-podAntiaffinity.yaml` {{ open }}

Let's deploy it to see where the pods get placed.

`kubectl apply -f ./files/09-podAntiaffinity.yaml` {{ execute "T1" }}

You should see that because the `database` deployment does not tolerate running on the same node as the `webserver` pod, all of the `database` pods are on one node, and the `webserver` is running on the other.

Let's delete this for now.

`kubectl delete -f ./files/09-podAntiaffinity.yaml` {{ execute "T1" }}

---

## Lab 10 - Pod Disruption Budget

As an application owner, you can create a PodDisruptionBudget (PDB) for each application. A PDB limits the number of Pods of a replicated application that are down simultaneously from voluntary disruptions. For example, a quorum-based application would like to ensure that the number of replicas running is never brought below the number needed for a quorum. A web front end might want to ensure that the number of replicas serving load never falls below a certain percentage of the total.

Lets take a look at the following file.

`./files/10-podDisruptionBudget.yaml` {{ open }}

Let's deploy it to see where the pods get placed.

`kubectl apply -f ./files/10-podDisruptionBudget.yaml` {{ execute "T1" }}

Now, lets see the nodes in the cluster and describe the Pod Disruption Budget.

`kubectl get nodes` {{ execute "T1" }}

`kubectl get pdb demo-pdb -n schedule-demo` {{ execute "T1" }}

`kubectl describe pdb demo-pdb -n schedule-demo` {{ execute "T1" }}

Now, let's drain and worker-2 and see what happens.

`kubectl drain kind-worker2 --ignore-daemonsets` {{ execute "T1" }}

Note that pod status is now pending because the spec allows a for a disruption of 1, with a minimum of 1 available.

Now, lets `uncordon` the node.

`kubectl uncordon kind-worker2` {{ execute "T1" }}

As soon as we `uncordon`, notice that the pod comes back online.

You should update the `10-podDisruptionBudget.yaml` file by changing `minAvailable` to 2, then reapply the file and run through the above steps again.

When you try to `cordon` the node now, you should see an error message similar to the following:

`error when evicting pods/"demo-pdb-web-1-766f468d46-p26ps" -n "schedule-demo" (will retry after 5s): Cannot evict pod as it would violate the pod's disruption budget.`

---

## Closing

You have now completed the Kubernetes Scheduling lab. If you have any questions, please let us know.

Before you exit, please run the below command to clean up your Kind cluster environment.

`kind delete cluster` {{ execute }}

---

### Congrats! You have completed the Kubernetes RBAC Lab.