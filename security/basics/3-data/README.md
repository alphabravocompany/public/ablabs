# Security Basics - Data

### This lab will cover the following topics:

- Setting File Ownership
- File Permissions - The Numeric Method
- File Permissions - The Symbolic Method
- Limiting Special Permissions
- Using the Sticky Bit

<br/>
<br/>

---
---

## Lab 1 - Setting File Ownership

This lab will cover how to set file ownership on an Ubuntu system. The ownership of a file or directory is used to control who has access to the file or directory.

<br/>

---

<br/>

### Change to the Working Directory
`cd /ab/labs/security/basics/3-data` {{ execute }}

<br/>

---

<br/>

### Create a Directory and File

Create a directory named `/tmp/secret`:
`mkdir -p /tmp/secret` {{ execute }}

Create a file in the `/tmp/secret` directory named `secret.txt`:
`touch /tmp/secret/secret.txt` {{ execute }}

Verify the directory was created. Note the ownership of the directory:
`ls -la /tmp | grep secret` {{ execute }}

Verify the file was created. Note the ownership of the file:
`ls -la /tmp/secret | grep secret.txt` {{ execute }}

<br/>

---

<br/>

### Change the Ownership of a Directory and File

When the `/tmp/secret` and `/tmp/secret/secret.txt` files were created, the ownership was set to the user that created the files.

Change the ownership of `/tmp/secret` directory to the `root` user and group:
`sudo chown root:root /tmp/secret` {{ execute }}

Verify the ownership of the `/tmp/secret` directory:
`ls -la /tmp | grep secret` {{ execute }}

Change the ownership of the `/tmp/secret/secret.txt` file to the `root` user and group:
`sudo chown root:root /tmp/secret/secret.txt` {{ execute }}

Verify the ownership of the `/tmp/secret/secret.txt` file:
`ls -la /tmp/secret | grep secret.txt` {{ execute }}

> **NOTE:** Both the `/tmp/secret` directory and `/tmp/secret/secret.txt` file are now owned by the `root` user and group.

<br/>

---

<br/>

### Change the Ownership of a Directory and File Recursively

It is useful to change the ownership of both the directory and any files contained within. This can be done using the `-R` option.

Create the group `developers`:
`sudo groupadd developers` {{ execute }}

Change the ownership of the `/tmp/secret` directory, and any files it contains, to the `root` user and `developers` group recursively:
`sudo chown -R root:developers /tmp/secret` {{ execute }}

Verify the ownership of the `/tmp/secret` directory:
`ls -la /tmp | grep secret` {{ execute }}

Verify the ownership of the `/tmp/secret/secret.txt` file:
`ls -la /tmp/secret | grep secret.txt` {{ execute }}

> **NOTE:** Both the `/tmp/secret` directory and `/tmp/secret/secret.txt` file are now owned by the `root` user and `developers` group.

<br/>
<br/>

---
---

## Lab 2 - File Permissions - The Numeric Method

This lab will cover how to set file permissions using the numeric method on an Ubuntu system. The numeric method is used to set permissions using octal values. The numeric method is the most common method used to set permissions.

<br/>

---

<br/>

### Set a Directory Permission

Get the current permissions of the `/tmp/secret` directory:
`ls -la /tmp | grep secret` {{ execute }}

> **NOTE:** The permissions of the `/tmp/secret` directory are currently set to `drwxr-xr-x`. The developer group can access the directory.

Allow read/write/execute permissions for the owner only:
`sudo chmod 700 /tmp/secret` {{ execute }}

Verify the permissions of the `/tmp/secret` directory:
`ls -la /tmp | grep secret` {{ execute }}

> **NOTE:** The permissions of the `/tmp/secret` directory are now set to `drwx------`. The developer group cannot access the directory, regardless of ownership.

<br/>

---

<br/>

### Set a File Permission

Allow read-only access for the owner:
`sudo chmod 400 /tmp/secret/secret.txt` {{ execute }}

Verify the permissions of the `/tmp/secret/secret.txt` file:
`ls -la /tmp/secret/secret.txt` {{ execute }}

> **NOTE:** Because permissions have been modified, a `permission denied` error will be displayed. This is expected.

Use `sudo` to verify the permissions of the `/tmp/secret/secret.txt` file:
`sudo ls -la /tmp/secret/secret.txt` {{ execute }}

> **NOTE:** The permissions of the `/tmp/secret/secret.txt` file are now set to `-r--------`. Only the root user can read the file. The root user cannot modify or execute the file, regardless of ownership.

<br/>

---

<br/>

### Set Directory Permissions for the Owner and Group

Allow execute permissions for the owner, write permissions for the group:
`sudo chmod 760 /tmp/secret` {{ execute }}

Verify the permissions of the `/tmp/secret` directory:
`ls -la /tmp | grep secret` {{ execute }}

> **NOTE:** The permissions of the `/tmp/secret` directory are now set to `drwxrw----`. The developer group can now access the directory and write to it.

<br/>

---

<br/>

### Set File Permissions for the Owner and Group

Allow read-only permissions for the owner and group:
`sudo chmod 440 /tmp/secret/secret.txt` {{ execute }}

Verify the permissions of the `/tmp/secret/secret.txt` file:
`sudo ls -la /tmp/secret | grep secret.txt` {{ execute }}

> **NOTE:** The permissions of the `/tmp/secret/secret.txt` file are now set to `-r--r-----`. Both the root user and developer group can now access the file, but cannot modify it.

<br/>
<br/>

---
---

## Lab 3 - File Permissions - The Symbolic Method

This lab will cover how to set file permissions using the symbolic method on an Ubuntu system. The symbolic method is used to set permissions using a combination of letters and symbols. The symbolic method is used to set permissions using a who, what, and which format.

<br/>

---

<br/>

### Set a File for the Owner to Execute

Attempt to execute the `/tmp/secret/secret.txt` file:
`sudo /tmp/secret/secret.txt` {{ execute }}

> **NOTE:** The `/tmp/secret/secret.txt` file cannot be executed because the execute permission is not set. An error will be displayed stating `command not found`.

Allow the owner to execute the file:
`sudo chmod u+x /tmp/secret/secret.txt` {{ execute }}

> **NOTE:** The `+` indicates permissions are being added. The `u` indicates the owner. The `x` indicates the execute permission.

Attempt to execute the `/tmp/secret/secret.txt` file:
`sudo /tmp/secret/secret.txt` {{ execute }}

> **NOTE:** The `/tmp/secret/secret.txt` file can now be executed because the execute permission is set for the owner. No error will be displayed.

<br/>

---

<br/>

### Remove the Read Permission for the Group

While the `+` adds permissions, the `-` removes permissions. The `g` specifies the group, and the `r` specifies the read permission.

Review the permissions of the `/tmp/secret/secret.txt` file:
`sudo ls -la /tmp/secret | grep secret.txt` {{ execute }}

> **NOTE:** The permissions of the `/tmp/secret/secret.txt` file are currently set to `-r-xr-----`. The developer group can read the file.

Remove the write permission for the group:
`sudo chmod g-r /tmp/secret/secret.txt` {{ execute }}

Verify the permissions of the `/tmp/secret/secret.txt` file:
`sudo ls -la /tmp/secret | grep secret.txt` {{ execute }}

> **NOTE:** The permissions of the `/tmp/secret/secret.txt` file are now set to `-r-x------`. The developer group can no longer read the file.

<br/>

---

<br/>

### Allow All Users to Execute the File

Allow all users to execute the file:
`sudo chmod a+x /tmp/secret/secret.txt` {{ execute }}

Verify the permissions of the `/tmp/secret/secret.txt` file:
`sudo ls -la /tmp/secret | grep secret.txt` {{ execute }}

> **NOTE:** The permissions of the `/tmp/secret/secret.txt` file are now set to `-r-x--x--x`. All users can now execute the file.

> **NOTE:** If the users do not have the rights to access the directory, it will not be possible to access the file to execute it.


<br/>
<br/>

---
---

## Lab 4 - Limiting Special Permissions

This lab will cover how to limit special permissions on an Ubuntu system. Special permissions are used to allow users to execute a file with the permissions of the file owner or group owner.

> **NOTE:** For the lab, a special binary file is provided to show how special permissions work. Most distributions block the use of special permissions on non-binary files, such as shell scripts.

<br/>

---

<br/>

### Build the `suid_demo` File

This lab will use a binary file named `suid_demo` to demonstrate how special permissions work. The `suid_demo` file is a simple C program that will display the real and effective user IDs of the user executing the file.

Build the `suid_demo` file:
`gcc -o suid_demo suid_demo.c` {{ execute }}

Verify the `suid_demo` file is present:
`ls -la | grep suid_demo` {{ execute }}

<br/>

---

<br/>

### Set the Permissions of the `suid_demo` File

Set the permissions so the `suid_demo` file can be read and executed by all users:
`sudo chmod a+rx suid_demo` {{ execute }}

Set the ownership to the `root` user:
`sudo chown root:root suid_demo` {{ execute }}

Verify the permissions of the `suid_demo` file:
`ls -la | grep suid_demo` {{ execute }}

> **NOTE:** The permissions of the `suid_demo` file should be set to `-rwxr-xr-x`. All users can now execute the `suid_demo` file.

<br/>

---

<br/>

### Run the File as the Current User

Get the user ID of the current user:
`id -u` {{ execute }}

Run the `suid_demo` file as the current user:
`./suid_demo` {{ execute }}

The output of the file should be as follows:
```bash
Real UID: {currentUserId}
Effective UID: {currentUserId}
```

> **NOTE:** This output demonstrates that the `suid_demo` file is executed with the permissions of the current user, and has no special rights.

<br/>

---

<br/>

### Set the Ownership of the `suid_demo` File to Root

Get the user ID of the root user, which should be `0`:
`id -u root` {{ execute }}

Set the SUID permission on the `suid_demo` file:
`sudo chmod u+s suid_demo` {{ execute }}

Verify the permissions of the `suid_demo` file:
`ls -la | grep suid_demo` {{ execute }}

> **NOTE:** The permissions of the `suid_demo` file should be set to `-rwsr-xr-x`. Note the user permissions are set to `rws`` instead of `rwx`. This indicates the SUID permission is set for the owner, which will allow users to execute the file with the permissions of the file owner.

Run the `suid_demo` file again as the current user:
`./suid_demo` {{ execute }}

The output of the file should be as follows:
```bash
Real UID: {currentUserId}
Effective UID: 0
```

> **NOTE:** Because the SUID permission is set, the `suid_demo` file is executed with the permissions of the file owner, which is the `root` user. The `Effective UID` is set to `0`, which is the user ID of the `root` user.

> **NOTE:** The same principal applies to the SGID permission. The SGID permission is set for the group owner. This will allow users to execute the file with the permissions of the group owner.

<br/>

---

<br/>

### Remove the SUID and/or SGID Permissions

To remove the SUID permission, use the following command:
`sudo chmod u-s suid_demo` {{ execute }}

Verify the permissions of the `suid_demo` file:
`ls -la | grep suid_demo` {{ execute }}

Execute the `suid_demo` file as the current user:
`./suid_demo` {{ execute }}

> **NOTE:** Since the SUID has been removed, the `suid_demo` file is executed with the permissions of the current user, improving the security of the file.  Further security can be achieved by removing the execute permission for all users.

<br/>

---

<br/>

### Find all Files with the SUID and/or SGID Permissions Set

It is possible to search the entire system for files with the SUID and/or SGID permissions set. This can be done using the `find` command.

Find all files with the SUID permission set and write to a file:
`sudo find / -perm -u=s -type f 2>/dev/null > /tmp/suid_files.txt` {{ execute }}

> **NOTE:** This command may take a while to run as it is searching the entire system.

#### What's Happening Here?

- `sudo` - Run the command as the `root` user.
- `find` - The command to find files.
- `/` - The directory to start the search, in this case, the root directory and all subdirectories.
- `-perm` - The option to search for files with specific permissions.
- `-u=s` - The option to search for files with the SUID permission set.
- `-type f` - The option to search for files only.
- `2>/dev/null` - Redirect any errors to `/dev/null`.
- `>` - Redirect the output to a file.
- `/tmp/suid_files.txt` - The file to write the output to.

Verify the contents of the `/tmp/suid_files.txt` file:
`cat /tmp/suid_files.txt` {{ execute }}

Find all files with the SGID permission set and write to a file:
`sudo find / -perm -g=s -type f 2>/dev/null > /tmp/sgid_files.txt` {{ execute }}

> **NOTE:** This command is identical to the previous command, except the `-u=s` option has been changed to `-g=s` to search for files with the SGID permission set.

Verify the contents of the `/tmp/sgid_files.txt` file:
`cat /tmp/sgid_files.txt` {{ execute }}

Find all files with either the SUID or SGID permission set and write to a file:
`sudo find / -perm -g=s -o -perm -u=s -type f 2>/dev/null > /tmp/suid_sgid_files.txt` {{ execute }}

Verify the contents of the `/tmp/suid_sgid_files.txt` file:
`cat /tmp/suid_sgid_files.txt` {{ execute }}

> **NOTE:** With the generated lists, it is possible to review the files and determine if the SUID and/or SGID permissions are required. If the permissions are not required, they should be removed to improve the security of the system.

<br/>
<br/>

---
---

## Lab 5 - Using the Sticky Bit

This lab will cover how to use the sticky bit on an Ubuntu system. The sticky bit is commonly used on directories to prevent users from deleting files that they do not own. It is very useful for directories that are shared by multiple users.

<br/>

---

<br/>

### Create a Directory

Create a directory named `/tmp/sticky`:
`sudo mkdir -p /tmp/sticky` {{ execute }}

Set the permissions of the `/tmp/sticky` directory to read, write, and execute for the owner:
`sudo chmod 777 /tmp/sticky` {{ execute }}

Verify the permissions of the `/tmp/sticky` directory:
`ls -lah /tmp | grep sticky` {{ execute }}

> **NOTE:** The permissions of the `/tmp/sticky` directory should be set to `drwxrwxrwx`, with all users having read/write/execute access.

<br/>

---

<br/>

### Set the Sticky Bit

Set the sticky bit on the `/tmp/sticky` directory using numeric permissions:
`sudo chmod 1777 /tmp/sticky` {{ execute }}

Alternatively, set the sticky bit on the `/tmp/sticky` directory using symbolic permissions:
`sudo chmod +t /tmp/sticky` {{ execute }}

Verify the permissions of the `/tmp/sticky` directory:
`ls -lah /tmp | grep sticky` {{ execute }}

> **NOTE:** The permissions of the `/tmp/sticky` directory should be set to `drwxrwxrwt`, with the `t` at the end indicating the sticky bit is set.

<br/>

---

<br/>

### Verify the Sticky Bit is Set

Create the user `bob`:
`sudo useradd bob` {{ execute }}

Create a file named `bob.txt` in the `/tmp/sticky` directory and set the owner to `bob`:
`touch /tmp/sticky/bob.txt && sudo chown bob:bob /tmp/sticky/bob.txt` {{ execute }}

Set the permissions to allow all users read/write access to the `bob.txt` file:
`sudo chmod a+rw /tmp/sticky/bob.txt` {{ execute }}

Verify the file permissions of the `bob.txt` file:
`ls -lah /tmp/sticky | grep bob.txt` {{ execute }}

> **NOTE:** The permissions of the `bob.txt` file should be set to `-rw-rw-rw-`, with all users having read/write access.

Try to delete the `bob.txt` file as the current user:
`rm /tmp/sticky/bob.txt` {{ execute }}

> **NOTE:** An error will be displayed stating `rm: cannot remove 'bob.txt': Operation not permitted`.

> **NOTE:** Even though all users have read/write access to the file, the current user cannot delete the file because the sticky bit is set on the `/tmp/sticky` directory. This only allows the owner of the file, the `root` user, or a user with `sudo` access to delete the file.

<br/>

---

<br/>

### Remove the Sticky Bit

Remove the sticky bit on the `/tmp/sticky` directory using numeric permissions:
`sudo chmod 0777 /tmp/sticky` {{ execute }}

Alternatively, remove the sticky bit on the `/tmp/sticky` directory using symbolic permissions:
`sudo chmod -t /tmp/sticky` {{ execute }}

Now try to delete the `bob.txt` file as the current user:
`rm /tmp/sticky/bob.txt` {{ execute }}

> **NOTE:** The `bob.txt` file can now be deleted because the sticky bit has been removed from the `/tmp/sticky` directory.

<br/>
<br/>

---
---

**Congrats! You have completed the Security Basics - Data lab.**