# Security Basics - Monitoring

### This lab will cover the following topics:

- Log Management
- System Monitoring
- System Auditing
- System Intrusion Detection
- Rootkit Detection

<br/>
<br/>

---
---

## Lab 1 - Log Management

This lab will cover log management and the tools used to review and monitor log files. Log management is critical to ensure that systems are operating as expected and to detect unauthorized access to systems and data.

> **NOTE:** Log files are used to capture system events and store those events in a central location, typically in the `/var/log` directory. It is possible to configure a centralized logging server to collect log files from multiple Linux systems.

<br/>

---

<br/>

### Change to the Working Directory
`cd /ab/labs/security/basics/4-monitoring` {{ execute }}

<br/>

---

<br/>

### Review the System Log Files

View the last 10 lines of the `/var/log/syslog` file:<br/>
`sudo tail -n 10 /var/log/syslog` {{ execute }}

> **NOTE:** The format of the log file is: `Month Day Time Hostname Process: Message`

Continuously view the last 10 lines of the `/var/log/syslog` file:<br/>
`sudo tail -f /var/log/syslog` {{ execute }}

> **NOTE:** Press `CTRL+C` to exit the `tail` command.

View the first 10 lines of the `/var/log/syslog` file:<br/>
`sudo head -n 10 /var/log/syslog` {{ execute }}

Add a log message to the `/var/log/syslog` file:<br/>
`logger "This is an example of an error message in the log file."` {{ execute }}

> **NOTE:** The `logger` command is useful with used with scripts to log events.

Find all instances of the string "error" in the `/var/log/syslog` file:<br/>
`sudo grep error /var/log/syslog` {{ execute }}

> **NOTE:** Typical log levels are: `debug`, `info`, `notice`, `warning`, `error`, `crit`, `alert`, and `emerg`.

<br/>

---

<br/>

### Setup a User for Testing

Create a user named `bob`:<br/>
`sudo useradd bob` {{ execute }}

Set the password for the user `bob`:<br/>
`echo "bob:password" | sudo chpasswd` {{ execute }}

Login as the user `bob`, but use an incorrect password such as `asdf`:<br/>
`su - bob` {{ execute }}

> **NOTE:** If done correctly, the login will fail with an error message: `su: Authentication failure`.

Login as the user `bob`, using the correct password set earlier:<br/>
`su - bob` {{ execute }}

Logout of the user `bob`:<br/>
`exit` {{ execute }}

Finally, use `sudo` to restart the `sshd` service:<br/>
`sudo systemctl restart sshd.service` {{ execute }}

<br/>

---

<br/>

### Review the Authentication Log Files

View the login history of a node:<br/>
`last -f /var/log/wtmp` {{ execute }}

> **NOTE:** This will show all current and past login attempts for all users.

View failed login attempts on a node:<br/>
`sudo last -f /var/log/btmp` {{ execute }}

> **NOTE:** This will show the failed login attempts for the `bob` user.

Monitor the auth.log for the string `USER=root`:<br/>
`sudo cat /var/log/auth.log | grep "USER=root"` {{ execute }}

> **NOTE:** Monitoring the auth.log file for the string `USER=root` is useful for detecting brute force attacks, or any malicious activity that attempts to login as the root user.

> **NOTE:** The format for the auth.log file is: `Month Day Time Hostname Process: Message`

<br/>
<br/>

---
---

## Lab 2 - System Monitoring

This lab will cover system monitoring and the tools used to monitor system resources and processes. System monitoring can cover a wide range of activities, including monitoring system performance, resource utilization, and network activity.

<br/>

---

<br/>

### Review Real-Time System Performance

Add load to the system:<br/>
`yes > /dev/null &` {{ execute }}

> **NOTE:** The `yes` command is used to output a string repeatedly until killed. The `yes` command is useful for adding load to a system for testing purposes. A malicious user could use the `yes` command to add load to a system to cause a denial of service (DoS) attack.

Run `htop` to view real-time system performance:<br/>
`htop` {{ execute }}

> **NOTE:** Press `CTRL+C` to exit the `htop` command.

> **NOTE:** Watch over time as the load average increases. This is due to the `yes` process using 100% of a single CPU core.

Stop the `yes` process:<br/>
`killall yes` {{ execute }}

> **CRITICAL:** Be sure to stop the `yes` process to avoid using too much CPU.

> **NOTE:** In Linux, a load average of 1.00 is equivalent to one or more processes using 100% of a single CPU core. A load average of 2.00 is equivalent to one or more processes using 100% of two CPU cores.

Install the `dstat` package:<br/>
`sudo apt update && sudo apt install dstat -y` {{ execute }}

Run `dstat` to view system performance:<br/>
`dstat -cdnmgyr` {{ execute }}

> **NOTE:** Press `CTRL+C` to exit the `dstat` command.

<br/>

---

<br/>

### Review Input/Output (I/O) Statistics

The `iostat` command is very useful for understanding disk performance. It displays the number of reads and writes per second, the number of bytes read and written per second, and the average time it takes to read and write data to the disk.

Install the `sysstat` package:<br/>
`sudo apt install sysstat -y` {{ execute }}

Run `iostat` to view I/O statistics:<br/>
`watch -n1 iostat` {{ execute }}

> **NOTE:** Press `CTRL+C` to exit the `iostat` watch command.

<br/>

---

<br/>

### Review Listening Ports

The `ss` command is very useful for understanding network activity. It displays all TCP/UDP listening ports, the services associated with the listening ports, and the process ID (PID) of the process that is listening on the port.

Run `ss` to view all TCP/UDP listening ports:<br/>
`sudo ss -tuln | grep LISTEN` {{ execute }}

Use the `watch` command to continuously view all TCP/UDP listening ports. Press `CTRL+C` to exit:<br/>
`watch -n 1 "sudo ss -tuln | grep LISTEN"` {{ execute }}

> **NOTE:** Press `CTRL+C` to exit the `ss` watch command.

> **NOTE:** It is important to use `sudo` with the `ss` command to see the services associated with the listening ports.

<br/>
<br/>

---
---

## Lab 3 - System Auditing

This lab will cover system auditing and the tools used to audit system events. The lab will make extensive use of the `auditd` service for auditing system events. The service is essential for tracking user activity and detecting unauthorized access to systems and data.

<br/>

---

<br/>

### Install the Auditd Service

In Linux, the `auditd` service is used to audit system events. The service is essential for tracking user activity and detecting unauthorized access to systems and data.

Install the `auditd` package:<br/>
`sudo apt update && sudo apt install auditd -y` {{ execute }}

Enable and start the `auditd` service:<br/>
`sudo systemctl enable auditd --now` {{ execute }}

> **NOTE:** The `--now` flag is used to start the service immediately.

Verify the `auditd` service is running:<br/>
`sudo systemctl status auditd` {{ execute }}

<br/>

---

<br/>

### Test the Auditd Service

Purposefully fail to login as a user a few times, purposefully entering the wrong password:<br/>
`su - bob` {{ execute }}

Run the report command to view the summary report, noting the failed authentication attempts:<br/>
`sudo aureport` {{ execute }}

Review the User ID report to show what users executed commands:<br/>
`sudo aureport -u -i` {{ execute }}

<br/>

---

<br/>

### Monitor a Directory

Create a demo directory to monitor in the user's home directory:<br/>
`mkdir $HOME/demo_dir` {{ execute }}

Monitor the demo directory:<br/>
`sudo auditctl -w $HOME/demo_dir -k demo_watch` {{ execute }}

> **NOTE:** The `-w` flag is used to watch a file or directory. The `-k` flag is used to specify a key for the audit rule. The key is used to search for events in the audit log.

Verify the directory is being monitored:<br/>
`sudo ausearch -k demo_watch` {{ execute }}

<br/>

---

<br/>

### Test the Directory Monitoring

Create a file in the demo directory:<br/>
`touch $HOME/demo_dir/hello.txt` {{ execute }}

Write content to the file:<br/>
`echo "Hello World" > $HOME/demo_dir/hello.txt` {{ execute }}

Verify the directory was monitored:<br/>
`sudo ausearch -k demo_watch` {{ execute }}

Run a report on the search:<br/>
`sudo ausearch -k demo_watch | sudo aureport -f -i` {{ execute }}

> **NOTE:** The above command takes the output of the `ausearch` command and pipes it to the `aureport` command. The `aureport` command is used to generate a report on the audit events, making it easier to read by a person.

<br/>

---

<br/>

### Audit via the Auth Log

The `/var/log/auth.log` file can be used to audit system events as well.

Change to the `root` user using `sudo`:<br/>
`sudo su -` {{ execute }}

> **NOTE:** The `su` command is used to switch to another user. The `-` flag is used to simulate a full login, which is required to switch to the `root` user.

Using the `auth.log` file, find any use of `sudo` to access the `root` user:<br/>
`sudo grep 'sudo su -' /var/log/auth.log` {{ execute }}

Login as the user `bob` several times via SSH:<br/>
`ssh bob@localhost` {{ execute }}

Using the `auth.log` file, find any failed login attempts:<br/>
`sudo grep 'Connection closed' /var/log/auth.log | grep "user bob"` {{ execute }}

<br/>
<br/>

---
---

## Lab 4 - System Intrusion Detection

This lab will cover system intrusion detection and the tools used to detect system intrusions, specifically the `snort` application. Additionally, the lab will cover the use of the `nmap` application to scan for open ports on a system.

<br/>

---

<br/>

### Install the Snort Service

Install the `snort` and `nmap` packages:<br/>
`sudo apt update && sudo apt -y install snort nmap` {{ execute }}

> **CRITICAL:** When prompted for the network interface `snort` should listen on, enter `lo`. The `lo` interface is the loopback interface, which is used to communicate with the local system. For all other prompts, press `ENTER` to accept the default value.

Disable the `snort` service, as it will be run manually:<br/>
`sudo systemctl stop snort.service && sudo systemctl disable snort.service` {{ execute }}

<br/>

---

<br/>

### Test and Run the Snort Service

Test the `snort` configuration:<br/>
`sudo snort -T -c /etc/snort/snort.conf` {{ execute }}

> **NOTE:** If everything works correctly, the output should say `Snort successfully validated the configuration!`

Run the `snort` service in the console:<br/>
`sudo snort -q -c /etc/snort/snort.conf -A console -i lo -h 0.0.0.0/0` {{ execute }}

> **NOTE:** The `-q` flag is used to run the service in quiet mode. The `-A console` flag is used to specify the output mode.

<br/>

---

<br/>

### Use `nmap` to Scan for Open Ports

Right click on the terminal window and select `Split Terminal`. This will open a new terminal window in the lab environment.

In this new terminal, `nmap` will be used to scan for open ports. While running the `nmap` commands, observe the output in the `snort` node terminal window.

Generate XMAS packets with `nmap`:<br/>
`sudo nmap -sX localhost` {{ execute }}

Generate TCP SYN packets with `nmap`:<br/>
`sudo nmap -sS localhost` {{ execute }}

Generate TCP ACK packets with `nmap`:<br/>
`sudo nmap -sA localhost` {{ execute }}

Generate Maimon packets with `nmap`:<br/>
`sudo nmap -sM localhost` {{ execute }}

Generate FIN packets with `nmap`:<br/>
`sudo nmap -sF localhost` {{ execute }}

Generate Service Version packets with `nmap`:<br/>
`sudo nmap -sV localhost` {{ execute }}

Generate Script Scan packets with `nmap`:<br/>
`sudo nmap -sC localhost` {{ execute }}

#### Explanation of `nmap` Scan Types

| Packet Type | Description | Malicious Use |
| ----------- | ----------- | ------------- |
| XMAS | Sends packets with the FIN, URG, and PUSH flags set. | Used to bypass firewalls. |
| TCP SYN | Sends packets with the SYN flag set. | Used to determine if a port is open. |
| TCP ACK | Sends packets with the ACK flag set. | Used to determine if a port is filtered. |
| Maimon | Sends packets with the URG flag set. | Used to bypass firewalls. |
| FIN | Sends packets with the FIN flag set. | Used to determine if a port is open. |
| Service Version | Sends packets to determine the version of a service. | Used to determine the version of a service. |
| Script Scan | Sends packets to determine the version of a service. | Used to determine the version of a service. |


<br/>

---

<br/>

### Cleanup

Right click on the terminal where the `snort` console is running and select `Kill Terminal` to close the terminal window.

<br/>
<br/>

---
---

## Lab 5 - Rootkit Detection

This lab will cover rootkit detection and the tools used to detect rootkits on a system. Rootkits are designed to hide their presence and actions, making detection challenging.

<br/>

---

<br/>

### Scanning with `chkrootkit`

The `chkrootkit` application is used to scan for rootkits on a system. It can be configured via a cron job to run daily and output the results to a log file.

Install the `chkrootkit` package:<br/>
`sudo apt update && sudo apt install chkrootkit -y` {{ execute }}

Verify where the `chkrootkit` application is installed:<br/>
`which chkrootkit` {{ execute }}

Run `chkrootkit` to scan for rootkits:<br/>
`sudo chkrootkit | grep INFECTED` {{ execute }}

<br/>

---

<br/>

### Daily Rootkit Scanning

To ensure that `chkrootkit` is run daily, a cron job needs to be created which will run the application and output the results to a log file.

Review the script that will be used to execute `chkrootkit` via cron:<br/>
`/ab/labs/security/basics/4-monitoring/rootkitcheck` {{ open }}

> **NOTE:** The script will run `chkrootkit` and output the results to the `/var/log/chkrootkit.log` file.

Copy the script to the `/etc/cron.daily` directory:<br/>
`sudo cp /ab/labs/security/basics/4-monitoring/rootkitcheck /etc/cron.daily/` {{ execute }}

Set the permissions on the script:<br/>
`sudo chmod +x /etc/cron.daily/rootkitcheck` {{ execute }}

Verify the permissions on the script:<br/>
`ls -l /etc/cron.daily/rootkitcheck` {{ execute }}

> **NOTE:** For best results, a service should be monitoring the `/var/log/chkrootkit.log` file for infections. If an infection is detected, an alert should be sent to system administrators.

<br/>
<br/>

---
---

**Congrats! You have completed the Security Basics - Monitoring lab.**