# Security Basics - Access

### This lab will cover the following topics:

- User Privileges
- Strong Password Policies
- Password Aging
- Account Lockout Policies
- User Account Auditing
- Permission Management with Sudo

<br/>
<br/>

---
---

## Lab 1 - User Privileges

User privileges are the permissions that a user has to perform certain actions on a system. This lab will cover the basic commands to manage user privileges.

<br/>

---

<br/>

### Change to the Working Directory
`cd /ab/labs/security/basics/1-access` {{ execute }}

<br/>

---

<br/>

### Create Users

First, create two users named `bob` and `alice`.

Create the `bob` user:
`sudo useradd bob` {{ execute }}

Verify the `bob` user was created:
`getent passwd bob` {{ execute }}

Create the `alice` user:
`sudo useradd alice` {{ execute }}

Verify the `alice` user was created:
`getent passwd alice` {{ execute }}

#### Differences Between `useradd` and `adduser`
In Linux, `useradd` is a low-level utility for adding users while `adduser` is a high-level utility for adding users. `useradd` will only create the user while `adduser` will prompt for user information, create a home directory, and copy files from `/etc/skel` to the home directory.


<br/>

---

<br/>

### Create a Group and Add Users

Next, create a group named `developers`.

Create the `developers` group:
`sudo groupadd developers` {{ execute }}

Add the `bob` and `alice` users to the `developers` group.

Add the `bob` user to the `developers` group:
`sudo usermod -aG developers bob` {{ execute }}

> **NOTE:** In the usermod command, the `-a` option appends the `developers` group to the user's existing groups. The `-G` option specifies the group to add to the user.

Add the `alice` user to the `developers` group:
`sudo usermod -aG developers alice` {{ execute }}

List the users in the `developers` group:
`getent group developers` {{ execute }}

> **NOTE:** The output should show the `bob` and `alice` users in the `developers` group: developers:x:{groupId}:bob,alice


<br/>

---

<br/>

### Create a Directory and Change Ownership

Next, create a directory named `/var/www/html`. Then, change the ownership of the directory to the `developers` group. This will allow the `bob` and `alice` users to read and write to the directory.

Create the `/var/www/html` directory:
`sudo mkdir -p /var/www/html` {{ execute }}

Change the ownership of the `/var/www/html` directory to the `developers` group:
`sudo chown -R root:developers /var/www/html` {{ execute }}

Verify the ownership of the `/var/www/html` directory:
`sudo ls -la /var/www | grep html` {{ execute }}

> **NOTE:** Intially, the `root` user will be given read, write, and execute permissions, while the `developers` group will be given read and execute permissions, as shown in the output: `drwxr-xr-x`.


<br/>

---

<br/>

### Change Permissions of the Directory

Change the permissions of the `/var/www/html` directory to only allow read and write access to the `developers` group and remove all other permissions:
`sudo chmod -R 760 /var/www/html` {{ execute }}

Verify the permissions of the `/var/www/html` directory:
`sudo ls -la /var/www | grep html` {{ execute }}

> **NOTE:** The output should show the `developers` group with read and write permissions: `drwxrw----`.


<br/>
<br/>

---
---

## Lab 2 - Strong Password Policies

This lab will cover the enforcement of strong password policies, which is critical to prevent unauthorized access to systems.

<br/>

---

<br/>

### Install the Required PAM Modules

The `libpam-pwquality` module is required to enforce strong password policies.

Install the `libpam-pwquality` module:
`sudo apt update && sudo apt install libpam-pwquality -y` {{ execute }}

<br/>

---

<br/>

### Configure the Password Policy

Review the variables which will be added to the `/etc/pam.d/common-password` file:
`/ab/labs/security/basics/1-access/common-password` {{ open }}

Backup the `/etc/pam.d/common-password` file:
`sudo cp /etc/pam.d/common-password /etc/pam.d/common-password.bak` {{ execute }}

Copy the file over the existing `/etc/pam.d/common-password` file:
`cat /ab/labs/security/basics/1-access/common-password | sudo tee /etc/pam.d/common-password` {{ execute }}

Verify the lines were added to the `/etc/pam.d/common-password` file:
`sudo cat /etc/pam.d/common-password` {{ execute }}

Run a `diff` to verify the changes:
`diff /ab/labs/security/basics/1-access/common-password /etc/pam.d/common-password` {{ execute }}

> **NOTE:** The `diff` command will show the differences between the two files. If there are no differences, the command will not return any output.

<br/>

---

<br/>

### Restart the `logind` Service

Restart the `logind` service:
`sudo systemctl restart systemd-logind.service` {{ execute }}

Verify the `logind` service is running:
`sudo systemctl status systemd-logind.service` {{ execute }}

<br/>

---

<br/>

### Test the Password Policy

Now that the password policy has been configured, test it by changing the password for the `bob` user. The password should be rejected as it does not meet the password policy requirements.
`echo bob:password | sudo chpasswd` {{ execute }}

> **NOTE:** The `chpasswd` command is used to change the password for the `bob` user. The `echo` command is used to pass the password to the `chpasswd` command. The `:` is used to separate the username and password.

Try another password containing a digit:
`echo bob:password123 | sudo chpasswd` {{ execute }}

Try another password that meets the password policy requirements:
`echo bob:Password123@# | sudo chpasswd` {{ execute }}

Verify the password was changed by logging in as the `bob` user with the password `Password123@#`:
`su - bob` {{ execute }}

Close the `bob` user session:
`exit` {{ execute }}

> **NOTE:** An `warning` will be shown when logging in as the `bob` user explaining the `/home/bob` directory was not created. This is because the `adduser` command was not used to create the `bob` user. This is expected and can be ignored.

<br/>

---

<br/>

### Batch Update the Passwords for Multiple Users

It is also possible to batch update the passwords for multiple users. This is useful when creating multiple users at once.

Review the `pwdfile` file:
`/ab/labs/security/basics/1-access/pwdfile` {{ open }}

Update the passwords for the `bob` and `alice` users:
`sudo chpasswd < /ab/labs/security/basics/1-access/pwdfile` {{ execute }}

<br/>

---

<br/>

### Verify the Passwords Were Updated

Login as the `alice` user using the password `Password456@#`:
`su - alice` {{ execute }}

Close the `alice` user session:
`exit` {{ execute }}

<br/>

---

<br/>

### Securely Delete the `pwdfile` File

The `shred` command will securely delete the `pwdfile` file by overwriting the file 10 times with random data. It will be impossible to recover the file after it has been shredded.

Securely delete the `pwdfile` file:
`shred -u -n 10 /ab/labs/security/basics/1-access/pwdfile` {{ execute }}


<br/>
<br/>

---
---

## Lab 3 - Password Aging

This lab will cover password aging, which is critical to prevent unauthorized access to systems by forcing users to change their passwords on a regular basis.

<br/>

---

<br/>

### Configure Password Aging for Users

Set the password aging for the `bob` user to expire in 90 days:
`sudo chage -I -1 -m 0 -M 90 -E -1 bob` {{ execute }}

> **NOTE:** The `-I` option sets the number of days since January 1, 1970 when the password was last changed. The `-1` option sets the value to the current date. The `-m` option sets the minimum number of days between password changes to `0`. The `-M` option sets the maximum number of days between password changes to `90`. The `-E` option sets the number of days since January 1, 1970 when the account will expire. The `-1` option sets the value to never expire.

Set the password aging for the `alice` user to expire in 30 days:
`sudo chage -I -1 -m 0 -M 30 -E -1 alice` {{ execute }}

<br/>

---

<br/>

### Verify the Password Aging for Users

Verify the password aging for the `bob` and `alice` users:
`sudo chage -l bob` {{ execute }}
`sudo chage -l alice` {{ execute }}

> **NOTE:** Review the line `Maximum number of days between password change` in the output. The `bob` user should have a maximum of `90` days and the `alice` user should have a maximum of `30` days.


<br/>
<br/>

---
---

## Lab 4 - Account Lockout Policies

This lab will cover account lockout policies, which is critical to prevent brute force attacks due to compromised passwords.

<br/>

---

<br/>

### Configure Account Lockout Policies

Backup the `/etc/pam.d/common-auth` file:
`sudo cp /etc/pam.d/common-auth /etc/pam.d/common-auth.bak` {{ execute }}

Review the `common-auth` file:
`/ab/labs/security/basics/1-access/common-auth` {{ open }}

> **NOTE:** Lines 3, 7, and 8 have been added to a basic `common-auth` file to configure account lockout policies. This will lock accounts after 3 failed login attempts, and unlock accounts after 15 minutes.

Copy the file over the existing `/etc/pam.d/common-auth` file:
`cat /ab/labs/security/basics/1-access/common-auth | sudo tee /etc/pam.d/common-auth` {{ execute }}

Verify the lines were added to the `/etc/pam.d/common-auth` file:
`sudo cat /etc/pam.d/common-auth` {{ execute }}

Run a `diff` to verify the changes:
`diff /ab/labs/security/basics/1-access/common-auth /etc/pam.d/common-auth` {{ execute }}

<br/>

---

<br/>

### Restart the `logind` Service

Restart the `logind` service:
`sudo systemctl restart systemd-logind.service` {{ execute }}

Verify the `logind` service is running:
`sudo systemctl status systemd-logind.service` {{ execute }}

<br/>

---

<br/>

### Verify the Account Lockout Policies

It's important to verify that the lockout policies are working as expected. To do so, login as the `bob` user with the wrong password.

Run the following **4 times**, entering the wrong password each time:
`su - bob` {{ execute }}

> **NOTE:** On the fourth attempt, an error will be shown and the account will be locked for 15 minutes.

Verify the account is locked:
`sudo faillock --user bob` {{ execute }}

Unlock the account:
`sudo faillock --user bob --reset` {{ execute }}

<br/>
<br/>

---
---

## Lab 5 - User Account Auditing

This lab will cover user account auditing, which is critical to monitor user activity on systems and ensures only authorized users are accessing systems.

> **NOTE:** In the previous lab, the `audit` flag was added to the `/etc/pam.d/common-auth` file to log failed login attempts. This flag will be used in this lab to audit user account activity.

<br/>

---

<br/>

### Review the Audit Log

Open a new terminal tab and tail the audit log:
`sudo tail -f /var/log/auth.log` {{ execute }}

In the original terminal, attempt to login as the `bob` user over SSH while monitoring the audit log:
`ssh bob@localhost` {{ execute }}

Now try to login as the `alice` user over SSH, again while monitoring the audit log:
`ssh alice@localhost` {{ execute }}

> **NOTE:** The audit log will show the failed login attempts for the `bob` and `alice` users. The login attempts will be rejected as the `bob` and `alice` users are not using an SSH key to authenticate, hence the error `Permission denied (publickey)`.

Finally, try logging in as a non-existent user over SSH, again while monitoring the audit log:
`ssh robert@localhost` {{ execute }}

> **NOTE:** The audit log will show the failed login attempt for the `robert` user. The login attempt will be rejected as the `robert` user does not exist with the error `invalid user robert`.

<br/>
<br/>

---
---

## Lab 6 - Permissions Management with Sudo

This lab will cover permissions management with `sudo`, which is critical to ensure users have the correct permissions to perform certain actions on systems.

<br/>

---

<br/>

### Try to Run a Privileged Command with the `bob` User

Change to the `bob` user with the password `Password123@#`:
`su - bob` {{ execute }}

> **NOTE:** Notice in the audit log that a session was logged for the `bob` user.

As the user `bob`, try to list the contents of the `/root` directory:
`ls -la /root` {{ execute }}

> **NOTE:** The user `bob` does not have permission to list the contents of the `/root` directory, and will receive a permission denied error.

Exit back to the previous user:
`exit` {{ execute }}

> **NOTE:** Notice in the audit log that the session for the `bob` user was closed.

<br/>

---

<br/>

### Try to Run a Privileged Command with the `alice` User

Change to the user `alice`, using the password `Password456@#`:
`su - alice` {{ execute }}

Restart the `sshd` service as the user `alice`:
`sudo systemctl restart sshd.service` {{ execute }}

> **NOTE:** An error stating that the `alice is not in the sudoers file` will be shown. This is because the user `alice` does not have permission to run privileged commands. Note the error in the audit log.

Exit back to the previous user:
`exit` {{ execute }}

<br/>

---

<br/>

### Update the `sudoers` File

To allow the users `bob` and `alice` to run privileged commands, the `sudoers` file will need to be updated.

Backup the sudoers file:
`sudo cp /etc/sudoers /etc/sudoers.bak` {{ execute }}

Allow the user `bob` to run all privileged commands without a password in the main `sudoers` file:
`echo "bob ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers` {{ execute }}

> **NOTE:** With this configuration, the user `bob` will be able to run all privileged commands without a password.

Allow the user `alice` to restart the `sshd` service, using a file in the `/etc/sudoers.d` directory:
`echo "alice ALL=(ALL:ALL) NOPASSWD:/usr/bin/systemctl restart sshd.service" | sudo tee -a /etc/sudoers.d/alice` {{ execute }}

> **NOTE:** With this configuration, the user `alice` will be able to restart the `sshd` service without a password, but will not be able to run any other privileged commands without a password.

> **NOTE:** Instead of writing to the main `sudoers` file, the `tee` command is used to write to a file in the `/etc/sudoers.d` directory. This is the preferred method of updating the `sudoers` file and helps to prevent breaking the primary `sudoers` file.

<br/>

---

<br/>

### Verify the `sudoers` File

Verify the `bob` permissions were added to the `/etc/sudoers` file:
`sudo cat /etc/sudoers | grep "bob"` {{ execute }}

Verify the contents of the `/etc/sudoers.d/alice` file:
`sudo cat /etc/sudoers.d/alice` {{ execute }}

<br/>

---

<br/>

### Re-test Running Privileged Commands as the `bob` User

Change to the user `bob`, using the password `Password123@#`:
`su - bob` {{ execute }}

As the user `bob`, list the contents of the `/root` directory again:
`ls -la /root` {{ execute }}

Now try the command again, but this time with `sudo`:
`sudo ls -la /root` {{ execute }}

> **NOTE:** The user `bob` will now be able to list the contents of the `/root` directory without a password.

Exit back to the previous user:
`exit` {{ execute }}

<br/>

---

<br/>

### Re-test Running Privileged Commands as the `alice` User

Change to the user `alice`, using the password `Password456@#`:
`su - alice` {{ execute }}

Restart the `sshd` service as the user `alice`:
`sudo systemctl restart sshd.service` {{ execute }}

Restart the `systemd-login` service as the user `alice`:
`sudo systemctl restart systemd-logind.service` {{ execute }}

> **NOTE:** The user `alice` will be able to restart the `sshd` service without a password, but will not be able to run any other privileged commands without a password. Observe the details in the audit log to see the `sshd` service was restarted.

Exit back to the previous user:
`exit` {{ execute }}

<br/>
<br/>

---
---

**Congrats! You have completed the Security Basics - Access lab.**