# Security Basics - System

### This lab will cover the following topics:

- Disabling Unused Services
- Kernel Updates
- Kernel Configuration
- System and Security Updates

<br/>
<br/>

---
---

## Lab 1 - Disabling Unused Services

This lab will cover disabling unused services on a Linux system.  This is a common security practice to reduce the attack surface of a system. Leaving unused services running on a system can lead to vulnerabilities and exploits.

<br/>

---

<br/>

### Change to the Working Directory
`cd /ab/labs/security/basics/5-system` {{ execute }}

<br/>

---

<br/>

### List Running Services

List all services on the system, regardless of the state:<br/>
`sudo systemctl list-units --type=service --all` {{ execute }}

> **NOTE:** Press `q` to exit the list on this command and all subsequent commands.

List the running services on the system:<br/>
`sudo systemctl list-units --type=service` {{ execute }}

List all services that are enabled:<br/>
`sudo systemctl list-unit-files --type=service --state=enabled` {{ execute }}

<br/>

---

<br/>

### Disable Services

Install K3s:<br/>
`curl -sfL https://get.k3s.io | sh -` {{ execute }}

> **NOTE:** K3s automatically enables and starts the service post-installation.

List the running services on the system:<br/>
`sudo systemctl list-unit-files --type=service --state=enabled | grep k3s` {{ execute }}

Find the K3s service ports:<br/>
`sudo ss -tulpn | grep k3s` {{ execute }}

Stop the K3s service:<br/>
`sudo systemctl stop k3s.service` {{ execute }}

Disable the K3s service:<br/>
`sudo systemctl disable k3s.service` {{ execute }}

Verify the K3s service is disabled:<br/>
`sudo systemctl list-unit-files --type=service --state=disabled | grep k3s` {{ execute }}

<br/>

---

<br/>

### Lab Cleanup

As there is no further need for K3s, uninstall it:<br/>
`sudo /usr/local/bin/k3s-uninstall.sh` {{ execute }}

<br/>
<br/>

---
---

## Lab 2 - Kernel Updates

This lab will cover updating the kernel on a Linux system.  This is a common security practice to ensure the system is running the latest kernel version.  This is especially important for systems that are exposed to the Internet.

<br/>

---

<br/>

### Check for Kernel Updates

Show the current installed kernel version:<br/>
`uname -r` {{ execute }}

Update the apt package index:<br/>
`sudo apt update` {{ execute }}

List all available kernel updates:<br/>
`sudo apt list --upgradable | grep linux-image` {{ execute }}

> **NOTE:** Depending on security updates for the Ubuntu image, there may not be any kernel updates available.

<br/>

---

<br/>

### Install Kernel Updates

Upgrade to the latest kernel version:<br/>
`sudo apt install --only-upgrade linux-generic linux-headers-generic linux-image-generic -y` {{ execute }}

Remove old kernel versions:<br/>
`sudo apt autoremove --purge` {{ execute }}

> **NOTE:** Once installed, you will need to reboot the system for the new kernel to take effect. A message will be displayed on the terminal after the kernel update is installed indicating that a newer kernel is available and a reboot is required.

<br/>
<br/>

---
---

## Lab 3 - Kernel Configuration

This lab will cover configuring the kernel on a Linux system.  This is a common security practice to ensure the system is running with the appropriate kernel settings.  This is especially important for systems that are exposed to the Internet.

> **NOTE:** The following kernel settings are not exhaustive.  There are many more settings that can be configured.  This lab will only cover a few of the most common settings.

> **NOTE:** Settings change over time as new kernel versions are released and vulnerabilities are discovered.  It is important to keep up with the latest kernel settings.

<br/>

---

<br/>

### Using `sysctl`

The `sysctl` command is used to view and modify kernel settings at runtime or persistently in the `/etc/sysctl.conf` file.

Get a list of all kernel settings:<br/>
`sudo sysctl -a` {{ execute }}

To temporarily change a kernel setting, use the `-w` flag as seen in the example below:<br/>
```bash
## Example command to modify a kernel setting
sudo sysctl -w {parameterName}={parameterValue}
```

Reload the kernel settings:<br/>
`sudo sysctl -p` {{ execute }}

> **NOTE:** To permanently change a kernel setting, add the setting to the `/etc/sysctl.conf` file. Most Linux systems will have a default `/etc/sysctl.conf` file with some settings already configured.

> **NOTE:** Is is very possible that a change to one or more kernel settings will cause a system to become unstable.  It is important to test any changes to kernel settings in a test environment before applying them to a production environment. Do not make multiple changes to kernel settings at once.  Make one change at a time and test the behavior of the system before making another change.

<br/>

---

<br/>

### Change Common Kernel Settings

View the current settings for ICMP redirects:<br/>
`sudo sysctl -a | grep net.ipv4.conf.all.accept_redirects` {{ execute }}

Disable ICMP Redirects:<br/>
`sudo sysctl -w net.ipv4.conf.all.accept_redirects=0` {{ execute }}

Verify ICMP Redirects are disabled:<br/>
`sudo sysctl -a | grep net.ipv4.conf.all.accept_redirects` {{ execute }}

> **NOTE:** ICMP redirects are used to inform a host of a better route to a destination.  This can be used to redirect traffic to a malicious host.

Protect against SYN Flood Attacks:<br/>
`sudo sysctl -w net.ipv4.tcp_syncookies=1` {{ execute }}


> **NOTE:** A SYN flood attack is a form of denial-of-service attack in which an attacker sends a succession of SYN requests to a target's system in an attempt to consume enough server resources to make the system unresponsive to legitimate traffic.

Enable IP Spoofing Protection:<br/>
`sudo sysctl -w net.ipv4.conf.all.rp_filter=1` {{ execute }}

> **NOTE:** IP spoofing is the creation of Internet Protocol (IP) packets with a false source IP address, for the purpose of impersonating another computing system.

Log Martian packets:<br/>
`sudo sysctl -w net.ipv4.conf.all.log_martians=1` {{ execute }}

> **NOTE:** Marian packets are packets with impossible addresses.  These packets are often used in attacks.

Avoid a Smurf Attack:<br/>
`sudo sysctl -w net.ipv4.icmp_echo_ignore_broadcasts=1` {{ execute }}

> **NOTE:** A Smurf attack is a type of denial-of-service attack in which an attacker floods a target system via spoofed broadcast ping messages.

Enable protected symlinks:<br/>
`sudo sysctl -w fs.protected_symlinks=1` {{ execute }}

> **NOTE:** Protected symlinks are used to prevent symlink attacks.  A symlink attack is a type of privilege escalation attack that exploits the vulnerability of symbolic links.  A symbolic link is a file that points to another file or directory.

Set the maximum number of PIDs:<br/>
`sudo sysctl -w kernel.pid_max=65536` {{ execute }}

> **NOTE:** A PID is a process identifier.  This setting limits the number of PIDs that can be created on the system.  This setting can be used to prevent a fork bomb attack.  A fork bomb attack is a denial-of-service attack that uses up all available system resources.

Set the maximum number of files:<br/>
`sudo sysctl -w fs.file-max=2097152` {{ execute }}

> **NOTE:** This setting limits the number of files that can be opened on the system.  This setting can be used to prevent a file descriptor exhaustion attack.  A file descriptor exhaustion attack is a denial-of-service attack that uses up all available system resources.


<br/>
<br/>

---
---

## Lab 4 - System and Security Updates

This lab will cover updating the system on a Linux system.  This is a common security practice to ensure the system is running the latest packages.  This is especially important for systems that are exposed to the Internet.

<br/>

---

<br/>

### Update the System

Update the apt package index:<br/>
`sudo apt update` {{ execute }}

Upgrade all packages to the latest version:<br/>
`sudo apt upgrade -y` {{ execute }}

> **NOTE:** The use of `dist-upgrade` and `full-upgrade` can be dangerous, as it may remove packages to resolve complex dependency issues as it may not be intelligent enough to understand if the additions/removals could cause stability issues.  It may be better to resolve dependency issues manually instead of using these commands.

Perform an intelligent upgrade with dependency resolution:<br/>
`sudo apt dist-upgrade` {{ execute }}

Perform a full upgrade with dependency resolution:<br/>
`sudo apt full-upgrade` {{ execute }}

> **NOTE:** Always review the changes that will be made before performing an upgrade.  This is especially important when using `dist-upgrade` and `full-upgrade`.

<br/>

---

<br/>

### Installing a Package with a Pinned Version

List all available versions of the NGINX package:<br/>
`sudo apt-cache madison nginx` {{ execute }}

Install a specific version of the NGINX package:<br/>
```bash
## Example command to install a pinned version of a package
sudo apt install nginx={{ packageVersion }} -y

## Example of installing a specific version of the NGINX package
sudo apt install nginx=1.18.0-6ubuntu14.4 -y
```

> **NOTE:** Using pinned versions is important from a security context as it allows users to install a specific version of a package that has been tested and verified vs installing a bleeding edge version of a package that may not have been tested and verified.

<br/>

---

<br/>

### Configure Automated Security Updates

Show a list of all available updates:<br/>
`sudo apt update && sudo apt list --upgradable` {{ execute }}

List all currently available security updates:<br/>
`sudo apt list --upgradable | grep "\-security"` {{ execute }}

> **NOTE:** Packages with security updates will have `-security` in the package name. Grep can be used to filter the list of security packages. If running this command produces no results, there are no security updates available.

Install the unattended-upgrades package:<br/>
`sudo apt install unattended-upgrades -y` {{ execute }}

> **NOTE:** It is very possible that unattended-upgrades may already be enabled and running on the system.  If this is the case, the command above will state that `unattended-upgrades is already the newest version`.

Enable and start the unattended-upgrades service:<br/>
`sudo systemctl enable unattended-upgrades --now` {{ execute }}

Verify the unattended-upgrades service is running:<br/>
`sudo systemctl status unattended-upgrades` {{ execute }}

> **NOTE:** The unattended-upgrades service can be configured by editing the `/etc/apt/apt.conf.d/50unattended-upgrades` file.

<br/>

---

<br/>

### Test Automated Security Updates

Check the logs for the unattended-upgrades updates:<br/>
`sudo tail -n20 /var/log/unattended-upgrades/unattended-upgrades.log` {{ execute }}

> **NOTE:** The log is a great way to determine if the unattended-upgrades service is working as expected, and find what packages were updated with security updates.

Force an update to test the unattended-upgrades service:<br/>
`sudo unattended-upgrades --dry-run --debug` {{ execute }}

> **NOTE:** This command includes a debug flag to show the output of the command.  This is not required, but is useful for troubleshooting and understanding what the command is doing.

Check the logs again. The output from the command above should be in the logs:<br/>
`sudo tail -n20 /var/log/unattended-upgrades/unattended-upgrades.log` {{ execute }}

<br/>
<br/>

---
---

**Congrats! You have completed the Security Basics - System lab.**