# Security Basics - Network

### This lab will cover the following topics:

- Firewalls
- Find Listening Ports
- Disabling Unused Network Protocols
- SSH Security
- SSH Key Authentication

<br/>
<br/>

---
---

## Lab 1 - Firewalls

This lab will cover how to configure a firewall on an Ubuntu system. The firewall will be configured to allow only the required ports and protocols.

<br/>

---

<br/>

### Change to the Working Directory
`cd /ab/labs/security/basics/2-network` {{ execute }}

<br/>

---

<br/>

### Install the Required Packages

Install the `Uncomplicated Firewall` (ufw) package:
`sudo apt update && sudo apt install ufw -y` {{ execute }}

Enable the firewall:
`sudo ufw enable` {{ execute }}

<br/>

---

<br/>

### Configure the Firewall

Block all traffic by default:
`sudo ufw default deny` {{ execute }}

Allow all outgoing traffic:
`sudo ufw default allow outgoing` {{ execute }}

Allow SSH traffic on port 2222 and 22:
`sudo ufw allow 2222/tcp` {{ execute }}
`sudo ufw allow 22/tcp` {{ execute }}

Allow UDP traffic on port 53:
`sudo ufw allow 53/udp` {{ execute }}

> **NOTE:** Port 2222 is added, as a later lab will change the SSH port to 2222. Port 22 is left enabled to allow SSH access during the lab.

<br/>

---

<br/>

### Verify and Enable the Firewall

Review the firewall rules:
`sudo ufw status verbose` {{ execute }}

The firewall rules should look like the following:

```bash
## Output from the ufw status verbose command:
Status: active
Logging: on (low)
Default: deny (incoming), allow (outgoing), disabled (routed)
New profiles: skip

To                         Action      From
--                         ------      ----
2222/tcp                   ALLOW IN    Anywhere       ## IPv4
22/tcp                     ALLOW IN    Anywhere
53/udp                     ALLOW IN    Anywhere
2222/tcp (v6)              ALLOW IN    Anywhere (v6)  ## IPv6
22/tcp (v6)                ALLOW IN    Anywhere (v6)
53/udp (v6)                ALLOW IN    Anywhere (v6)
```

Enable the updated firewall rules:
`sudo ufw reload` {{ execute }}

> **NOTE:** The `ufw` tool is a simplified wrapper for the more complex `iptables` tool. The `iptables` tool is covered in the advanced security course.

Disable the firewall service:
`sudo ufw disable` {{ execute }}

> **NOTE:** The firewall is being disabled to prevent issues with the next lab.

<br/>
<br/>

---
---

## Lab 2 - Find Listening Ports

This lab will cover how to find all listening ports on a Linux system. The lab will be using the `ss` tool, but the `netstat` tool can also be used.

List all listening TCP ports:
`sudo ss -tulpn` {{ execute }}

List all listening UDP ports:
`sudo ss -ulpn` {{ execute }}

Filter the output to only show the SSH port:
`sudo ss -tulpn | grep sshd` {{ execute }}

> **NOTE:** It is possible to run the command without `sudo`, but the output will not show the process name to help identify the service listening on the port.

> **NOTE:** Local addresses matching 0.0.0.0 or :: are listening on all network interfaces, ie. eth0, eth1, etc. Local addresses matching a specific IP address are listening on a specific ethernet interface.


<br/>
<br/>

---
---

## Lab 3 - Disable Unused Network Protocols

This lab will cover how to disable unused network protocols on a Linux system. The lab will be disabling the IPv6 protocol.

<br/>

---

<br/>

### Manually Disable IPv6

The following commands will disable IPv6 in real time, but if the system is rebooted, IPv6 will be re-enabled.

Verify IPv6 is enabled. There should be an inet6 address listed:
`ip a | grep inet6` {{ execute }}

Disable IPv6 in real time:
`sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1` {{ execute }}
`sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1` {{ execute }}

Verify the IPv6 settings are disabled. There should be no inet6 address listed:
`ip a | grep inet6` {{ execute }}

Re-enable IPv6 in real time:
`sudo sysctl -w net.ipv6.conf.all.disable_ipv6=0` {{ execute }}
`sudo sysctl -w net.ipv6.conf.default.disable_ipv6=0` {{ execute }}

Verify the IPv6 settings are enabled. There should be inet6 addresses listed:
`ip a | grep inet6` {{ execute }}

<br/>

---

<br/>

### Disable IPv6 on Boot

To permanently disable IPv6, the IPv6 settings need to be added to the `/etc/sysctl.conf` file.

Review the sysctl settings in the `disable-ipv6` file:
`/ab/labs/security/basics/2-network/disable-ipv6` {{ open }}

Using the `disable-ipv6` file, append the following lines to the `/etc/sysctl.conf` file to disable IPv6 on boot:

`cat /ab/labs/security/basics/2-network/disable-ipv6 | sudo tee -a /etc/sysctl.conf` {{ execute }}

> **NOTE:** The `-a` flag is used with `tee` appends the output to the `/etc/sysctl.conf` file. If the `-a` flag is not used, the file will be overwritten.

Verify the IPv6 settings in the `/etc/sysctl.conf` file:
`tail -n 4 /etc/sysctl.conf` {{ execute }}

> **NOTE:** Now that these settings are in the `/etc/sysctl.conf` file, the IPv6 protocol will be disabled on boot.

<br/>
<br/>

---
---

**Congrats! You have completed the Security Basics - Network lab.**