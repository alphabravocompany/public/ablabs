# Security Vulnerabilities - Trivy

### This lab will cover the following topics:

- Scanning for Vulnerabilities
- Reviewing Vulnerabilities
- Formatting the Output using Templates

<br/>
<br/>

---
---

## Lab 1 - Scanning for Vulnerabilities

This lab will cover scanning for vulnerabilities using Trivy.  Trivy is a vulnerability scanner for container images and filesystems. The lab will cover scanning a container image and a filesystem.


<br/>

---

<br/>

### Change the Directory

Change the working directory:
`cd /ab/labs/security/vulnerabilities/2-trivy` {{ execute }}

<br/>

---

<br/>

### Scan a Container Image

Scan an older Alpine container image:<br/>
`trivy image alpine:3.12` {{ execute }}

Scan a newer Alpine container image:<br/>
`trivy image alpine:3.18` {{ execute }}

> **NOTE:** You may notice that the results of the Trivy scan may be different than the results of the Grype scan. This is due to the different vulnerability databases used by each tool.

> **CRITICAL:** Users should **_never_** trust a single vulnerability scanner, as each scanner may have different results.  Users should always use multiple vulnerability scanners to ensure that all vulnerabilities are found. The more scanners used, the more likely that all vulnerabilities will be found.

<br/>

---

<br/>

### Scan a Filesystem

Using Git, clone the Kubernetes Github repository into the `git` directory:<br/>
`git clone --depth 1 https://github.com/kubernetes/kubernetes.git git/kubernetes` {{ execute }}

Clone the K3s Github repository into the `git` directory:<br/>
`git clone --depth 1 https://github.com/k3s-io/k3s.git git/k3s` {{ execute }}

Scan the Kubernetes Github repository:<br/>
`trivy fs ./git/kubernetes` {{ execute }}

Scan the K3s Github repository:<br/>
`trivy fs ./git/k3s` {{ execute }}

<br/>

---

<br/>

### Scan a Git Repository

Unlike Grype, Trivy can scan a Git repository directly.  This is useful for scanning a Git repository without having to clone the repository first.

Scan the Kubernetes Github repository:<br/>
`trivy repo https://github.com/kubernetes/kubernetes.git` {{ execute }}

Scan the K3s Github repository:<br/>
`trivy repo https://github.com/k3s-io/k3s.git` {{ execute }}

> **NOTE:** The direct git repository scan may be significantly slower than scanning a cloned repository.  This is due to the fact that Trivy has to download the entire repository before scanning it, and doesn't have an option for setting the `--depth` of the repository.

<br/>

---

<br/>

### Formatting the Scan Output

Scan the Alpine container image and output the results in a table:<br/>
`trivy image -f table alpine:3.12` {{ execute }}

Scan and output the results in JSON format:<br/>
`trivy image -f json alpine:3.12` {{ execute }}

Use JQ to make the output more readable:<br/>
`trivy image -f json alpine:3.12 | jq` {{ execute }}

Use JQ to output only the package name, cve, severity, and fix:<br/>
`trivy image -f json alpine:3.12 | jq '.Results[] | .Vulnerabilities[] | {package: .PkgName, cve: .VulnerabilityID, severity: .Severity, fix: .FixedVersion}'` {{ execute }}

> **NOTE:** The `jq` command is this example is different from the Grype example.  This is due to the different output formats of the two tools.

Scan and save the results to a file:<br/>
`trivy image -f json -o json-alpine-3.12.json alpine:3.12` {{ execute }}

<br/>
<br/>

---
---

## Lab 2 - Reviewing Vulnerabilities

This lab will cover reviewing the vulnerabilities found by the vulnerability scanner.  The lab will cover reviewing the vulnerabilities found in an Alpine container image.

<br/>

---

<br/>

### Review the Vulnerabilities in JSON Format

Open the Alpine vulnerability JSON file:
`/ab/labs/security/vulnerabilities/2-trivy/example-json-alpine-3.12.json` {{ open }}

The JSON file contains a list of vulnerabilities found in the Alpine container image.  Each vulnerability contains the following information:

#### Metadata
The metadata of the container image that was scanned. This section contains items such as metadata, layers, the container image ID, etc.

#### Results
The list of vulnerabilities found in the container image.  Each vulnerability contains the following information:

- **VulnerabilityID** - The ID of the vulnerability.
- **PkgName** - The name of the package that contains the vulnerability.
- **InstalledVersion** - The version of the package that contains the vulnerability.
- **FixedVersion** - The version of the package that fixes the vulnerability.
- **PrimaryURL** - The URL of the vulnerability details.
- **Severity** - The severity of the vulnerability.
- **CVSS** - The CVSS score of the vulnerability.
- **Title** - The title of the vulnerability.
- **Description** - The description of the vulnerability.

> **NOTE:** As the SARIF standard guarantees the output matches from vulnerability tool to vulnerability tool, it will not be covered in this lab to avoid redundancy.

<br/>
<br/>

---
---

## Lab 3 - Formatting the Output using Templates

This lab will cover the use of templates to format the output of the vulnerability scanner.  The lab will cover formatting the output of the Alpine container image vulnerabilities using the SARIF and JSON templates.

> **NOTE:** Templates are written using the Go template language.  For more information, see the [Go template documentation](https://pkg.go.dev/text/template).

<br/>

---

<br/>

### SARIF Template

The `SARIF` template is a built-in template that will output the results in SARIF format.  This is useful for integrating the output into other tools or databases.

Run the vulnerability scanner and output the results using the SARIF template:<br/>
`trivy image --format sarif -o sarif-alpine-3.12.json alpine:3.12` {{ execute }}

Write the output to a file:<br/>
`trivy image --format sarif -o sarif-alpine-3.12.json alpine:3.12` {{ execute }}

Open and review the SARIF file:
`/ab/labs/security/vulnerabilities/2-trivy/sarif-alpine-3.12.json` {{ open }}

<br/>

---

<br/>

### JSON Template

The JSON template will take the output generated by the vulnerability scanner and output it in JSON format.  This is useful for integrating the output into other tools or databases.

Open the JSON template file:
`/ab/labs/security/vulnerabilities/2-trivy/template-json.tmpl` {{ open }}

> **NOTE:** As with Grype, the Trivy JSON template uses the Go template syntax. As a result, it is not very different from the Grype JSON template. The Trivy template is slightly more complex, as it must process data in a slightly different format due to the output of the vulnerability scanner.

Run the vulnerability scanner and output the results using the JSON template:<br/>
`trivy image --format template --template "@/ab/labs/security/vulnerabilities/2-trivy/template-json.tmpl" alpine:3.12` {{ execute }}

Write the output to a file:<br/>
`trivy image --format template --template "@/ab/labs/security/vulnerabilities/2-trivy/template-json.tmpl" -o alpine-3.12.json alpine:3.12` {{ execute }}

Open the JSON file:
`/ab/labs/security/vulnerabilities/2-trivy/alpine-3.12.json` {{ open }}

> **NOTE:** The JSON templates for Grype and Trivy allow the output of the different tools to match.  This is useful for integrating the output of the two tools into a single tool or database.

<br/>
<br/>

---
---

**Congrats! You have completed the Security Vulnerability - Trivy lab.**