# Security Vulnerabilities - Mitigation

### This lab will cover the following topics:

- Mitigating Vulnerabilities in a Container Image

<br/>
<br/>

---
---

## Lab 1 - Mitigating Vulnerabilities in a Container Image

This lab will cover mitigating a container base image vulnerability. The container will be scanned using a vulnerability scanner, and then the vulnerability will be mitigated by updating the container base image.

<br/>

---

<br/>

### Build the Container Image

Change the current working directory:<br/>
`cd /ab/labs/security/vulnerabilities/3-mitigate` {{ execute }}

Build the container image using the `Dockerfile`:<br/>
`docker build -t base:latest -f Dockerfile .` {{ execute }}

Scan the container image using Trivy:<br/>
`trivy image base:latest` {{ execute }}

> **NOTE:** A significant number of vulnerabilities exist in this container image due to its age.

<br/>

---

<br/>

### Mitigate the HIGH Vulnerabilities

Scan the container image using Trivy to get only the `HIGH` vulnerabilities:<br/>
`trivy image --severity HIGH base:latest` {{ execute }}

Open the Dockerfile in the editor:<br/>
`/ab/labs/security/vulnerabilities/3-mitigate/Dockerfile` {{ open }}

> **NOTE:** The Dockerfile is currently using the packages installed in the `ubuntu:bionic-20210118` base image, which is very old and has many vulnerabilities.

In the Dockerfile, uncomment lines 3 through 7 by highlighting the lines with the mouse, then selecting `Toggle Line Comment` from the VS Code menu.

> **NOTE:** For Windows, the keyboard shortcut is `Ctrl+/`. For macOS, the keyboard shortcut is `Cmd+/`.

After uncommenting the lines, build the Dockerfile again:<br/>
`docker build -t base:latest -f Dockerfile .` {{ execute }}

Scan the container image, filtering on HIGH vulnerabilities:<br/>
`trivy image --severity HIGH base:latest` {{ execute }}

> **NOTE:** The number of HIGH vulnerabilities should be zero.

#### What's Happening in the Dockerfile?

As noted in the vulnerability scan, fixes exist for the `HIGH` vulnerabilities. By utilizing the `apt install` command in the Dockerfil, the older packages are replaced with newer packages that do not contain the vulnerabilities.

It's important to note the package versions installed must be equal to or greater than the package versions listed in the vulnerability scan. If the package versions are lower than the package versions listed in the vulnerability scan, the vulnerability will still exist.

> **NOTE:** From a security standpoint, it is always recommended to pin the package versions to ensure that the same package versions are installed every time the container image is built. This ensures that the container image is always built with the same package versions, and that the container image is not built with newer package versions that may contain newer exploits/vulnerabilities.

> **NOTE:** Pinning versions is also useful to avoid breaking changes in newer versions of packages to guarantee system stability. For example, if a newer version of a package changes the default configuration, it may break the application.  Pinning the version ensures that the application is always built with the same version of the package, but contains the latest security patches.

<br/>

---

<br/>

### Mitigate the MEDIUM Vulnerabilities

Scan the container image using Trivy to get only the `MEDIUM` vulnerabilities:<br/>
`trivy image --severity MEDIUM base:latest` {{ execute }}

In the Dockerfile, uncomment lines 9 through 37 by highlighting the lines with the mouse, then selecting `Toggle Line Comment` from the VS Code menu.

> **NOTE:** For Windows, the keyboard shortcut is `Ctrl+/`. For macOS, the keyboard shortcut is `Cmd+/`.

After uncommenting the lines, build the Dockerfile again:<br/>
`docker build -t base:latest -f Dockerfile .` {{ execute }}

Scan the container image, filtering on MEDIUM vulnerabilities:<br/>
`trivy image --severity MEDIUM base:latest` {{ execute }}

> **NOTE:** The number of MEDIUM vulnerabilities should be zero.

<br/>

---

<br/>

### Mitigate the LOW Vulnerabilities

Scan the container image using Trivy to get only the `LOW` vulnerabilities:<br/>
`trivy image --severity LOW base:latest` {{ execute }}

In the Dockerfile, uncomment lines 39 through 66 by highlighting the lines with the mouse, then selecting `Toggle Line Comment` from the VS Code menu.

After uncommenting the lines, build the Dockerfile again:<br/>
`docker build -t base:latest -f Dockerfile .` {{ execute }}

Scan the container image, filtering on LOW vulnerabilities:<br/>
`trivy image --severity LOW base:latest` {{ execute }}

> **NOTE:** The number of LOW vulnerabilities should be zero.

<br/>

---

<br/>

### Run a Complete Scan

Scan the container image using Trivy to get all vulnerabilities:<br/>
`trivy image base:latest` {{ execute }}

Compare the Trivy results to a Grype scan:<br/>
`grype base:latest` {{ execute }}

> **NOTE:** Ideally, the result for the Grype scan should match the result for the Trivy scan. However, due to the differences in the vulnerability databases used by each tool, the results are likely to be different.

> **NOTE:** This exercise clearly demonstrates why it is important to use multiple vulnerability scanners to ensure that all vulnerabilities are found.  The more scanners used, the more likely that all vulnerabilities will be found.

<br/>
<br/>

---
---

**Congrats! You have completed the Security Vulnerability - Mitigation lab.**
