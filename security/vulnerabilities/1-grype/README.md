# Security Vulnerabilities - Grype

### This lab will cover the following topics:

- Scanning for Vulnerabilities
- Reviewing Vulnerabilities
- Formatting the Output using Templates

<br/>
<br/>

---
---

## Lab 1 - Scanning for Vulnerabilities

This lab will cover scanning for vulnerabilities using Grype.  Grype is a vulnerability scanner for container images and filesystems. The lab will cover scanning a container image and a filesystem.


<br/>

---

<br/>

### Change the Directory

Change the working directory:
`cd /ab/labs/security/vulnerabilities/1-grype` {{ execute }}

<br/>

---

<br/>

### Scan a Container Image

Scan an older Alpine container image:<br/>
`grype alpine:3.12` {{ execute }}

Scan a newer Alpine container image:<br/>
`grype alpine:3.18` {{ execute }}

> **NOTE:** The older Alpine container image has more vulnerabilities than the newer Alpine container image. This is due to the older Alpine container image being released before the vulnerabilities were discovered. Generally, users want to use the latest container image to ensure the latest security patches are available.

> **NOTE:** Generally, the latest version of the Alpine container image is very secure and has very few vulnerabilities. This is due to the Alpine container image being very minimal and only containing the bare minimum packages needed to run the container image.

<br/>

---

<br/>

### Scan a Filesystem

Using Git, clone the Kubernetes Github repository into the `git` directory:<br/>
`git clone --depth 1 https://github.com/kubernetes/kubernetes.git git/kubernetes` {{ execute }}

Clone the K3s Github repository into the `git` directory:<br/>
`git clone --depth 1 https://github.com/k3s-io/k3s.git git/k3s` {{ execute }}

Scan the Kubernetes Github repository:<br/>
`grype dir:git/kubernetes` {{ execute }}

Scan the K3s Github repository:<br/>
`grype dir:git/k3s` {{ execute }}

> **NOTE:** It's incredibly rare for a large project to be completely free of vulnerabilities.  Additionally, the context of the vulnerability is important.  While a vulnerability may be marked as `HIGH`, it's possible that the vulnerability is not exploitable in the context of the project and therefore may not be a concern.

<br/>

---

<br/>

### Formatting the Scan Output

Scan the Alpine container image and output the results in a table:<br/>
`grype alpine:3.12 --output table` {{ execute }}

Scan container and output the results in the SARIF format:<br/>
`grype alpine:3.12 --output sarif` {{ execute }}

Scan and output the results in JSON format:<br/>
`grype alpine:3.12 --output json` {{ execute }}

Use JQ to make the output more readable:<br/>
`grype alpine:3.12 --output json | jq` {{ execute }}

> **NOTE:** The `jq` command is a command-line JSON processor.  It is useful for formatting JSON output.

Use JQ to output only the package name, cve, severity, and fix:<br/>
`grype alpine:3.12 --output json | jq '.matches[] | {package: .artifact.name, cve: .vulnerability.id, severity: .vulnerability.severity, fix: .vulnerability.fix.versions}'` {{ execute }}

Scan and save the results to a file:<br/>
`grype alpine:3.12 --output json > alpine-3.12.json` {{ execute }}

<br/>
<br/>

---
---

## Lab 2 - Reviewing Vulnerabilities

This lab will cover reviewing the vulnerabilities found by the vulnerability scanner.  The lab will cover reviewing the vulnerabilities found in an Alpine container image.

<br/>

---

<br/>

### Review the Vulnerabilities in JSON Format

Open the Alpine vulnerability JSON file:
`/ab/labs/security/vulnerabilities/1-grype/example-json-alpine-3.12.json` {{ open }}

The JSON file contains a list of vulnerabilities found in the Alpine container image.  Each vulnerability contains the following information:

#### Matches
The `matches` section contains the list of vulnerabilities found in the container image.  Each vulnerability contains the following information:

- **id** - The CVE ID of the vulnerability. In the first example, the CVE ID is `CVE-2022-37434`.
- **dataSource** - The source of the vulnerability, ie. the national vulnerability database (NVD).
- **severity** - The severity of the vulnerability, ie. `Critical`.
- **urls** - A list of URLs related to the vulnerability, useful for additional information.
- **description** - A description of the vulnerability.
- **cvss** - The CVSS score of the vulnerability.
- **fix** - The fix for the vulnerability, ie. `1.2.12-r2`.
- **relatedVulnerabilities** - A list of related vulnerabilities.
- **matchDetails** - Additional details about the vulnerability.
- **artifact** - The artifact that contains the vulnerability, ie. `zlib` and its version.

#### Source
The `source` section contains the source of the vulnerability, ie. the Alpine 3.12 container image. It contains the following information:

- **type** - The type of source, ie. `image`.
- **target** - The target of the source, ie. `alpine:3.12` and its associated details

#### Distro
The `distro` section contains the details of the distribution, ie. Alpine 3.12.

#### Descriptor
The `descriptor` section contains the details of the scan itself, ie. the version of Grype used to scan the container image, the database version, the timestamp of the scan, and any flags used against the scan.

> **NOTE:** Different tools will output different formats in JSON, but the vulnerability information will always be consistent.

<br/>

---

<br/>

### Review the Vulnerabilities in SARIF Format

The `SARIF` format is a standard format for the output of static analysis tools.  This format is useful for integrating with other tools, guaranteeing that the output will be in a consistent format.

Open the Alpine vulnerability SARIF file:
`/ab/labs/security/vulnerabilities/1-grype/example-sarif-alpine-3.12.json` {{ open }}

As with the JSON file, the SARIF file contains a list of vulnerabilities found in the Alpine container image.  Each vulnerability contains the following information:

#### SARIF Details

- **version** - The version of the SARIF format.
- **$schema** - The schema of the SARIF format.
- **runs** - The list of vulnerabilities found in the container image.

#### Runs

- **tool** - The tool used to scan the container image, ie. Grype.
- **rules** - The list of vulnerabilities found in the container image and their associated details, such as the severity, package, description, fixes, etc.
- **results** - The list of vulnerabilities found in the container image and their associated details, such as the location of the vulnerability, and the artifact that contains the vulnerability.

> **NOTE:** While different vulnerability scanner tools may output different information and formats in JSON, but the SARIF format will always be consistent from vulnerability scanner to vulnerability scanner.

> **NOTE:** The SARIF format was initially created by Microsoft, but has since been adopted by many other vendors and is now an open standard.

<br/>
<br/>

---
---

## Lab 3 - Formatting the Output using Templates

This lab will cover the use of templates to format the output of the vulnerability scanner.  The lab will cover formatting the output of the Alpine container image vulnerabilities using the CVS and JSON templates.

> **NOTE:** Templates are written using the Go template language.  For more information, see the [Go template documentation](https://pkg.go.dev/text/template).

<br/>

---

<br/>

### CSV Template

The CSV template will take the output generated by the vulnerability scanner and output it in CSV format.  This is useful for importing the output into a spreadsheet program, such as Microsoft Excel.

Open the CSV template file:
`/ab/labs/security/vulnerabilities/1-grype/template-csv.tmpl` {{ open }}

> **NOTE:** The template uses the `range` to iterate over the list of vulnerabilities found in the container image. For each vulnerability, the template will output the artifact name, version, vulnerability ID, and severity.

Run the vulnerability scanner and output the results using the CSV template:<br/>
`grype alpine:3.12 --output template -t /ab/labs/security/vulnerabilities/1-grype/template-csv.tmpl` {{ execute }}

Write the output to a file:<br/>
`grype alpine:3.12 --output template -t /ab/labs/security/vulnerabilities/1-grype/template-csv.tmpl > alpine-3.12.csv` {{ execute }}

Open the CSV file:
`/ab/labs/security/vulnerabilities/1-grype/alpine-3.12.csv` {{ open }}

> **NOTE:** Right click on the CSV file and select the `Download` option to download the file to your local system. From there, you can open the file in a spreadsheet program, such as Microsoft Excel.

<br/>

---

<br/>

### JSON Template

The JSON template will take the output generated by the vulnerability scanner and output it in JSON format.  This is useful for integrating the output into other tools or databases.

Open the JSON template file:
`/ab/labs/security/vulnerabilities/1-grype/template-json.tmpl` {{ open }}

> **NOTE:** This template iterates through the output results, similar to the CSV template.  However, the JSON template outputs the results in JSON format.

Run the vulnerability scanner and output the results using the JSON template:<br/>
`grype alpine:3.12 --output template -t /ab/labs/security/vulnerabilities/1-grype/template-json.tmpl` {{ execute }}

Write the output to a file:<br/>
`grype alpine:3.12 --output template -t /ab/labs/security/vulnerabilities/1-grype/template-json.tmpl > alpine-3.12.json` {{ execute }}

Open the JSON file:
`/ab/labs/security/vulnerabilities/1-grype/alpine-3.12.json` {{ open }}

<br/>
<br/>

---
---

**Congrats! You have completed the Security Vulnerability - Grype lab.**