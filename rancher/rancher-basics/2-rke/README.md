# Rancher Basics - Rancher Kubernetes Engine (RKE)

### This lab will cover the following topics:

* Configuring RKE
* Deploying RKE
* Adding RKE to Rancher MCM

<br/>
<br/>

---
---

## Lab 1 - Configuring RKE

This lab will cover the configuration of Rancher Kubernetes Engine (RKE). RKE is a lightweight Kubernetes installer that can be used to deploy Kubernetes on a single node or on a cluster of nodes. RKE is a standalone binary that can be used to deploy Kubernetes on a variety of platforms.

> **NOTE:**<BR/>This lab environment has the RKE binary pre-installed. If you would like to install RKE on your own machine, please refer to the [RKE Installation Guide](https://rancher.com/docs/rke/latest/en/installation/).

> **NOTE:**<BR/>The version of the RKE binary supports and installs specific versions of Kubernetes. For more information on the supported versions, please refer to the RKE release notes.

<br/>

---

<br/>

### Useful Links

* [Rancher Kubernetes Engine (RKE)](https://rancher.com/products/rke/)

<br/>

---

<br/>

### Change the Directory

Change to the RKE labs directory:

`cd /ab/labs/rancher/rancher-basics/2-rke` {{ execute }}

<br/>

---

<br/>

### Run the RKE Configuration Wizard

Run the following command to start the RKE configuration wizard:

`rke config` {{ execute }}

This process will walk you through a series of questions. Answer the questions as shown below for this install.

> **NOTE:**<BR/>The example configuration shown is designed for this lab and shouldn't be used in a production environment.

> **NOTE:**<BR/>Options with the arrow, `->`, indicated below will require options other than the default provided. The remaining options can use the defaults. Simply press `ENTER` on these questions.

```bash
## RKE Options
[+] Cluster Level SSH Private Key Path [~/.ssh/id_rsa]:

-> [+] Number of Hosts [1]: 1
-> [+] SSH Address of host (1) [none]: localhost

[+] SSH Port of host (1) [22]:
[+] SSH Private Key Path of host (localhost) [none]:
[+] SSH Private Key of host (localhost) [none]:

-> [+] SSH User of host (localhost) [ubuntu]: abtraining
-> [+] Is host (localhost) a Control Plane host (y/n)? [y]: y
-> [+] Is host (localhost) a Worker host (y/n)? [n]: y
-> [+] Is host (localhost) an etcd host (y/n)? [n]: y

[+] Override Hostname of host (localhost) [none]:
[+] Internal IP of host (localhost) [none]:
[+] Docker socket path on host (localhost) [/var/run/docker.sock]:
[+] Network Plugin Type (flannel, calico, weave, canal, aci) [canal]:
[+] Authentication Strategy [x509]:
[+] Authorization Mode (rbac, none) [rbac]:
[+] Kubernetes Docker image [rancher/hyperkube:v1.27.5-rancher1]:
[+] Cluster domain [cluster.local]:
[+] Service Cluster IP Range [10.43.0.0/16]:
[+] Enable PodSecurityPolicy [n]:
[+] Cluster Network CIDR [10.42.0.0/16]:
[+] Cluster DNS Service IP [10.43.0.10]:
[+] Add addon manifest URLs or YAML files [no]:
```

<br/>

---

<br/>

### Review the Generated Cluster Configuration File

The RKE configuration wizard will generate a `cluster.yml` file in the current directory. This file contains the configuration for the Kubernetes cluster that will be deployed.

Review the `cluster.yml` file:

`/ab/labs/rancher/rancher-basics/2-rke/cluster.yml` {{ open }}

> **NOTE:**<BR/>The `cluster.yml` file contains the configuration for the Kubernetes cluster that will be deployed. The `cluster.yml` file can be used to deploy the cluster using the `rke up` command.

> **NOTE:**<BR/>Additional options are available in the `cluster.yml` file that are not available in the RKE configuration wizard. For more information on the `cluster.yml` file, please refer to the [RKE Configuration Options](https://rancher.com/docs/rke/latest/en/config-options/).

<br/>

---

<br/>

### SSH Configuration

As RKE is being installed on the localhost, the installation environment will need to SSH into itself. This requires the creation of an SSH key pair and the addition of the public key to the `authorized_keys` file.

Generate an SSH key pair:

`ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa -q -N ""` {{ execute }}

Add the public key to the `authorized_keys` file:

`cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys` {{ execute }}

> **NOTE:**<BR/>When actually installing RKE, a legitimate SSH key would be required to access the remote hosts in order to perform the Kubernetes installation

<br/>

---

<br/>

### Install RKE

Now the configuration is complete, RKE can be installed. Run the following command to install RKE:

`rke up` {{ execute }}

> **NOTE:**<BR/>The installation process will take a few minutes to complete.

> **NOTE:**<BR/>The installation process will output a `kube_config_cluster.yml` file. This file can be used to access the Kubernetes cluster using `kubectl`.

<br/>

---

<br/>

### Post Install Verification

Run the following command to verify the Kubernetes cluster is running:

`kubectl --kubeconfig kube_config_cluster.yml get nodes` {{ execute }}

> **NOTE:**<BR/>Under the `STATUS` section, the nodes should be listed as `Ready`. If the nodes are not listed as `Ready`, wait a few minutes and try again or use the `watch` command to monitor the status in real-time.

> **NOTE:**<BR/>The environment has successfully deployed a single node Kubernetes cluster using RKE. Nice work!

<br/>
<br/>

---
---

## Lab 2 - Adding RKE to Rancher MCM

This lab will cover adding the newly created RKE cluster to Rancher MCM. Rancher MCM natively understands RKE clusters and can manage them just like any other Kubernetes cluster, but with additional information about the cluster that is only available due to the tight integration between MCM and RKE.

### Review the RKE Kubeconfig File

The newly generated Kubeconfig file is located in the current directory. Open the `kube_config_cluster.yml` file and review it:

`/ab/labs/rancher/rancher-basics/2-rke/kube_config_cluster.yml` {{ open }}

<br/>

---

<br/>

### Set the Context to the RKE Cluster

Run the following command to set the context to the RKE cluster:

`export KUBECONFIG=/ab/labs/rancher/rancher-basics/2-rke/kube_config_cluster.yml` {{ execute }}

<br/>

---

<br/>

### Add the RKE Cluster to Rancher MCM

Add the cluster to your Rancher instance running on your lab server.

1. Open the Rancher UI at https://rancher-LABSERVERNAME
2. Use the Code Server password in the Lab Logins file, found in the Student Drive
3. In the upper left click the burger menu, then click `Cluster Management`
4. Click the button that says `Import Existing` and select `Generic`
5. Enter a the cluster name `rke-cluster` and click `Create`
6. Copy the `curl --insecure` command to the clipboard by clicking on the line of text
7. Paste this command into the lab terminal below
8. Wait for this process to complete. The status will update at the top of the window.
9. Once the process is complete, click on the burger menu and select `Cluster Management`

> **NOTE:**<BR/>It is recommended to have a Rancher MCM UI running for each connected network of clusters. For example, if three (3) Kubernetes clusters are running on the same network or Impact Level (IL), these clusters can be managed via a single Rancher Manager UI. Running multiple disconnected clusters can be managed via a single Rancher MCM instance per cluster.

<br/>
<br/>

---
---

**Congrats! You have completed the Rancher RKE labs. You may now proceed with the rest of the course.**
