# Rancher Basics - Rancher Multi-Cluster Manager

### This lab will cover the following topics:

* Installing Rancher Multi-Cluster Manager
* Exploring Rancher Multi-Cluster Manager
* Adding a Kubernetes cluster to Rancher MCM

<br/>
<br/>

---
---

## Lab Environment Setup
1. In this VSCode window, look for the 'burger' menu in the upper left corner of the window. The menu typically appears as three stacked horizontal lines.

2. Navigate through this menu, clicking on `Terminal`, then `New Terminal`. This action will bring up a new terminal window at the bottom of the VSCode environment.

3. All commands mentioned in this guide will be executed within this terminal window. To run these commands, do one of the following:

    * Type the commands in manually. This is the recommended method, as it will help build muscle memory for the commands.

    * Copy and paste the commands from this guide into the terminal window. To copy, highlight the text to copy, then right-click and select Copy. To paste, right-click and select Paste.

    * Click on the `Execute` button next to the command. This will automatically copy and paste the command into the terminal window and execute it.

<br/>
<br/>

---
---

## Stopping HAproxy Service

Before starting the Rancher labs, it is important to ensure the required ports are available on the lab server.

Run the following command to release ports 80/443 so to allow Kubernetes ingress on those ports.

`sudo systemctl stop haproxy` {{ execute }}

<br/>
<br/>

---
---

## Lab 1 - Deploying Rancher Multi-Cluster Manager

This lab will cover the deployment and configuration of Rancher Multi-Cluster Manager (MCM). Rancher MCM is a centralized management plane for multiple Kubernetes clusters. It provides a single pane of glass for managing multiple Kubernetes clusters, regardless of where they are running.

<br/>

---

<br/>

### Useful Links

* [Rancher Multi-Cluster Manager](https://rancher.com/products/rancher/)
* [Rancher Single Node Install](https://ranchermanager.docs.rancher.com/v2.7/pages-for-subheaders/rancher-on-a-single-node-with-docker)
* [Rancher HA Install](https://ranchermanager.docs.rancher.com/v2.7/pages-for-subheaders/install-upgrade-on-a-kubernetes-cluster)
* [Helm](https://helm.sh/)

<br/>

---

<br/>

### Review the Docker Compose File

In this lab environment, Rancher MCM is deployed as a single node installation using Docker. Please review the the Docker Compose file used to deploy Rancher MCM.

`/ab/labs/rancher/rancher-basics/1-mcm/rancher-docker-compose.yml` {{ open }}

> **NOTE:**<BR/>The Docker Compose file includes a local persistent volume using a bind mount to a local path which includes pre-generated certificates. This allows the user interface (UI) to support HTTPS.

> **NOTE:**<BR/>This Rancher MCM instance will be used later in this lab to navigate the Rancher MCM UI and import a second Kubernetes cluster.

<br/>

---

<br/>

### Deploy a Kubernetes Cluster

Run the following command to bring a Kubernetes cluster online:

`k3d cluster create lab-cluster --volume /ab/k3dvol:/tmp/k3dvol --api-port 16443 --servers 1 --agents 3 -p 80:80@loadbalancer -p 443:443@loadbalancer -p "30000-30010:30000-30010@server:0"` {{ execute }}

#### What's Happening Here?
The above command is performing the following actions:
1. Using K3d to create a cluster called `lab-cluster`.
2. Creating a local persistent volume using a bind mount to a local path which includes pre-generated certificates.
3. Exposing the Kubernetes API on port `16443`.
4. Creating one (1) server node.
5. Creating three (3) agent nodes.
6. Exposing ports `80` and `443` on the local host.
7. Exposing ports `30000-30010` on the server node.

<br/>

---

<br/>

### Set the Cluster Context

Run the following command to set the cluster context. This will ensure that all subsequent commands are executed against the correct lab cluster.

`kubectl config use-context k3d-lab-cluster` {{ execute }}

Verify the correct cluster context is set by running the following command:

`kubectl config current-context` {{ execute }}

> **NOTE:**<BR/>It's good practice to always verify the correct cluster context is set. If the incorrect cluster context is set, it may result in commands being executed against the wrong cluster.

<br/>

---

<br/>

### Helm Installation Documentation

The goal of this lab is to provide the experience of deploying Rancher in a highly available (HA) configuration. To accomplish this, Helm will be used to install Rancher MCM on the K3d/K3s Cluster running locally.

> **NOTE:**<BR/>This lab follows the official Rancher MCM documentation to perform the Helm installation. The documentation [can be found here](https://ranchermanager.docs.rancher.com/v2.7/pages-for-subheaders/install-upgrade-on-a-kubernetes-cluster).

<br/>

---

<br/>

### Add the Rancher Helm Repo

In order to install Rancher MCM using Helm, the Rancher Helm repo must be added to the local environment.

`helm repo add rancher-stable https://releases.rancher.com/server-charts/stable` {{ execute }}

<br/>

---

<br/>

### Create the Rancher Namespace

Run the following command to create the Rancher namespace. The namespace is where the Rancher MCM components will be deployed, and must be created before the Helm install.

`kubectl create namespace cattle-system` {{ execute }}

> **NOTE:**<BR/>Some Helm charts will automatically create the namespace if it does not exist. In this case, the Namespace must exist to install the required certificates prior to the Helm install, as shown in the next step.

<br/>

---

<br/>

### Certificate Installation

Create a secret containing a self-signed certificate and key pair. This secret will be used to secure the Rancher MCM UI.

`kubectl -n cattle-system create secret tls tls-rancher-ingress --cert=/ab/certs/live/LABSERVERNAME/fullchain.pem --key=/ab/certs/live/LABSERVERNAME/privkey.pem` {{ execute }}

> **NOTE:**<BR/>The certificate and key pair are located in the `/ab/certs/live/LABSERVERNAME` directory. This directory was created during the lab environment setup and populated with certificates for use with Rancher MCM.

> **NOTE:**<BR/>The TLS secret is used in Kubernetes specifically to store TLS certificates and associated keys.  This secret will be referenced when deploying the Rancher MCM Helm chart.

<br/>

---

<br/>

### Install Rancher MCM Using Helm

The following command will install Rancher MCM using Helm.

`helm upgrade --install rancher rancher-stable/rancher --namespace cattle-system --set hostname=rancher.LABSERVERNAME --set ingress.tls.source=secret --set bootstrapPassword=supersecretpassword` {{ execute }}

#### What's Happening Here?
The above command is performing the following actions:
1. Installing Rancher MCM using the Rancher Helm repo.
2. Installing Rancher MCM in the `cattle-system` namespace.
3. Setting the hostname to `rancher.LABSERVERNAME`.
4. Setting the TLS source to `secret`, which references the TLS secret created in the previous step.
5. Setting the bootstrap password to `supersecretpassword`.

<br/>

---

<br/>

### Verify Rancher MCM is Running

Using Kubectl, verify that Rancher MCM is running by running the following command:

`kubectl -n cattle-system rollout status deployment/rancher` {{ execute }}

> **NOTE:**<BR/>The deployment of Rancher MCM can take some time. It may be required to run the above command multiple times until the deployment is complete, or to pair it with the `watch` command.

<br/>

---

<br/>

### Visit the Rancher MCM UI

In addition to verifying the deployment via `kubectl`, it's also possible to verify Rancher MCM is running by visiting the Rancher MCM UI.

1. Verify Rancher MCM is running by visiting the Rancher MCM UI at https://rancher.LABSERVERNAME.
2. When asked to enter the `admin` password, use the password `supersecretpassword`.

<br/>

---

<br/>

### Cleanup

Now that the deployment is complete, cleanup the lab environment by deleting the Kubernetes cluster.

`k3d cluster delete lab-cluster` {{ execute }}

> **NOTE:**<BR/>Deleting the cluster will automatically remove the cluster and any software installed on it. This includes Rancher MCM. However, it will not remove the Helm chart from the local environment. This must be done manually.

<br/>
<br/>

---
---

## Lab 2 - Exploring Rancher MCM

This lab will cover the initial configuration of  Rancher MCM and how to navigate the UI.

<br/>

---

<br/>

### Visit the Rancher MCM UI

Visit the Rancher MCM UI at https://rancher-LABSERVERNAME.

> **NOTE:**<BR/>Please note that this URL is ***different*** than the URL used in the previous lab. This URL includes a hyphen (`-`) between `rancher` and `LABSERVERNAME`, whereas the previous lab used a period (`.`) between `rancher` and `LABSERVERNAME`.<BR/><BR/>The hyphen URL will be used for the remainder of the labs.

<br/>

---

<br/>

### Initial Configuration

The first time the Rancher MCM UI is accessed, the user will be prompted to configure the UI.

1. Enter the password found in the `Lab Logins` document provided in the `Student Share` directory.
2. Click `Continue`.
3. Select the checkbox next to `I agree to the terms and conditions...`
4. Click `Continue`.

> **NOTE:**<BR/> Once logged in, the interface shows information about the cluster that Rancher MCM is installed on. This cluster is referred to as the `local` cluster.

<br/>

---

<br/>

### Navigating the Rancher MCM UI

Navigation in the Rancher MCM UI is done using the menu on the left side of the screen. The menu is broken down into the following sections:

- **Home**<BR/>
  - Directs to the main dashboard.
- **Explore Cluster**<BR/>
  - Provide a list of all clusters managed by Rancher MCM.
  - Each cluster can be selected to view information about that cluster.
  - Clicking on a cluster link will navigate to the dashboard of that cluster.
- **Global Apps**<BR/>
  - Continuous Delivery with Fleet
  - Cluster management, creating/importing and managing clusters
  - Virtualization management with Harvester
- **Configuration**<BR/>
  - Users & Authentication
  - Global MCM settings

## Navigating the Cluster Dashboard

The `Cluster Dashboard` provides a centralized view of the cluster. This includes information about the cluster, as well as the ability to manage the cluster.

1. Click burger menu in the upper left.

2. Under `Explore Cluster`, click `local`.
    - This links to the `Cluster Dashboard` for the `local` cluster.
    - The `local` cluster referenced here is the K3d cluster Rancher MCM was install on to.

### Cluster Dashboard
- Note the Provider and Kubernetes Version.
- Rancher MCM understands the cluster is K3s and what version of Kubernetes is running.

### Cluster Metrics
Scroll to the bottom of the `local` dashboard to find the `Cluster Metrics`.

- Rancher is actively gathering information about the `local` cluster.
- Understands CPU, Memory, Disk Utilization, Network Traffic, API Server requests, Pod scheduling status, and more.<BR/><BR/>


### Cluster Nodes

In the menu on the left, under `Cluster`, click `Nodes`.

- Rancher lists out all of the nodes in the `local` cluster.
- Clicking on a `Node` will provide information about that Node.
- Nodes can be edited to add `labels` and `annotations`.
- On other clusters that are not `local`, options will be made available to allow node `Cordon` and `Drain`
- This is useful when maintenance is required on one or more nodes.
- Clicking on a node provides additional information about the node, including scheduled Pods, metrics, taints, current conditions, and more.

### Workloads

In the menu on the left, click `Workloads`.

- Rancher lists all possible workloads (ie. Pods, Deployments, StatefulSets, etc.) that can be deployed to the cluster.
- Clicking on a Workload will provide information about that Workload.
- Namespaces can be selected, as well as Projects, which will show the Workloads running in those Namespaces/Projects.


### Applications

In the menu on the left, click on `Apps`.

- Rancher MCM lists all possible applications that can be deployed to the cluster.
- All applications are Helm charts.
- Additional Helm repositories can be added to Rancher MCM to provide additional applications.
- Applications can be searched, and clicking on an application will provide information about that application.
- The `Cluster Tools` button on the bottom left will provide additional Rancher managed tools that can be deployed to the cluster.

### Service Discovery

In the menu on the left, click on `Service Discovery`.

- Allows for management of Services, Ingress, and Horizontal Pod Autoscalers (HPA).
- Services can be created, edited, and deleted.
- Ingress can be created, edited, and deleted.
- HPAs can be created, edited, and deleted.

### Storage

In the menu on the left, click on `Storage`.
- Rancher MCM provides a centralized view of all storage classes, persistent volumes, and persistent volume claims.
- Storage classes can be created, edited, and deleted.
- Persistent volumes can be created, edited, and deleted.
- Persistent volume claims can be created, edited, and deleted.

### Policy
In the menu on the left, click on `Policy`.

- Rancher MCM provides a centralized view of all Limit Ranges, Resource Quotas, Network Policies, and Pod Disruption Budgets (PDBs).
- Limit Ranges and Resource Quotas can be set via a YAML file.
- Network Policies can be created, edited, and deleted.
- PDBs can be created, edited, and deleted.

<br/>
<br/>

---
---

### Lab 3 - Adding a Kubernetes cluster to Rancher MCM

This lab will cover the process of adding a Kubernetes cluster to Rancher MCM. The Kubernetes cluster will be added using the Rancher MCM UI and will be referred to as the `remote` cluster.

<br/>

---

<br/>

### Deploying the Remote Kubernetes Cluster

In order to import Kubernetes cluster into Rancher MCM, a Kubernetes cluster must be deployed. This cluster will be called `lab-cluster`.

`k3d cluster create lab-cluster --volume /ab/k3dvol:/tmp/k3dvol --api-port 16443 --servers 1 --agents 3 -p 80:80@loadbalancer -p 443:443@loadbalancer -p "30000-30010:30000-30010@server:0"` {{ execute }}

<br/>

---

<br/>

### Switch the Cluster Context

Run the following command to set the cluster context to `k3d-lab-cluster`.

`kubectl config use-context k3d-lab-cluster` {{ execute }}

Verify the correct cluster context is set by running the following command:

`kubectl config current-context` {{ execute }}

> **NOTE:**<BR/>The output of the command should show `k3d-lab-cluster`.

<br/>

---

<br/>

### Add the Remote Cluster to Rancher MCM

Add the `remote` cluster to Rancher MCM by following the steps below:

1. In the upper left, click the burger menu.
2. Click on `Cluster Management`.
3. Click on `Import Existing` in the upper right.
4. Select `Generic` from the list of options.
5. Enter a the cluster name `lab-cluster`.
6. Enter a description for the `remote` cluster.
7. Click `Create` to add the `remote` cluster to Rancher MCM.
8. Copy the first `kubectl` command by clicking on the line of text.
9. In the lab command line, paste the command and press `Enter`.
10. Return to the Rancher MCM UI, clicking on `Clusters` in the menu on the left.
11. Click on the `Explore` button next to the `lab-cluster` cluster.

> **NOTE:**<BR/>The `remote` cluster has been successfully added to Rancher MCM and can be managed from the Rancher MCM UI. Take some time to explore the newly imported cluster, and compare it to the `local` cluster.

<br/>

---

<br/>

### Add a Node to the Remote Cluster

In the `lab-cluster` cluster dashboard, click on the `Nodes` link in the menu on the left. Note the four (4) nodes that are currently running in the cluster.

In the lab terminal, run the following command to add a node to the `lab-cluster` cluster:

`k3d node create lab-cluster-agent-4 --cluster lab-cluster` {{ execute }}

> **NOTE:**<BR/>After running the command, switch back to the Rancher MCM UI and watch the new Node appear in the list of Nodes. Clicking on the Node will provide additional information about the Node.

<br/>

---

<br/>

### Cleanup

Now that the deployment is complete, cleanup the lab environment by deleting the Kubernetes cluster from the Rancher MCM UI.

1. In the Rancher MCM UI, click on the burger menu in the upper left.
2. Click on `Cluster Management`.
3. Click the vertical three dot menu button (ie. kebab menu) next to the `lab-cluster` cluster.
4. Click on `Delete`.
5. Enter the cluster name `lab-cluster` in the text box.
6. Click `Delete` to remove the `lab-cluster` cluster from Rancher MCM.

Now that the cluster has been removed from Rancher MCM, delete the Kubernetes cluster from the lab environment terminal.

`k3d cluster delete lab-cluster` {{ execute }}

<br/>
<br/>

---
---

**Congrats! You have completed the Rancher MCM labs. You may now proceed with the rest of the course.**
