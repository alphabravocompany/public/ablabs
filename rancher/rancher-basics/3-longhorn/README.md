# Rancher Basics - Longhorn

### This lab will cover the following topics:

* Deploying Longhorn
* Creating a Pod with a PV on Longhorn
* Cluster Cleanup

<br/>
<br/>

---
---

## Lab 1 - Deploying Longhorn

This lab will cover the installation of Longhorn, a lightweight, reliable, and easy-to-use distributed block storage system for Kubernetes. The lab will use the RKE cluster created in the previous lab and will deploy Longhorn via the Rancher MCM UI.

> **NOTE:**<BR/>Longhorn is being deployed on the RKE cluster due to limitations of the K3d cluster. K3d has technical limitations which prevent Longhorn from being deployed on it. This is not a limitation of Longhorn, but rather a limitation of the K3d cluster.

<br/>

---

<br/>

### Useful Links

* [Rancher Longhorn](https://rancher.com/products/longhorn/)
* [Longhorn Documentation](https://longhorn.io/docs/)
* [Best Practices for Longhorn](https://longhorn.io/docs/1.5.1/best-practices/)
* [Longhorn GitHub](https://github.com/longhorn/longhorn)

<br/>

---

<br/>

### Change Directories

Run the following command to change to the correct directory:

`cd /ab/labs/rancher/rancher-basics/3-longhorn` {{ execute }}

<br/>

---

<br/>

### Deploy Longhorn via Rancher Apps

Deploy Longhorn from the Rancher UI onto our RKE cluster:

1. Open the Rancher UI at https://rancher-LABSERVERNAME
2. Use the Code Server password in the Lab Logins file, found in the Student Drive
3. In the upper left click the burger menu, then click `rke-cluster`
4. On the left menu, click on `Cluster Tools` at the bottom of the menu
5. Find the Longhorn app and click on the `Install` button
6. Longhorn configuration options will appear. Leave the defaults and click `Install`
7. A terminal window will appear on the bottom showing the progress of the installation.
8. Wait for the message: `Longhorn is now installed on the cluster!`
9. Longhorn is now installed and ready for use.

<br/>

---

<br/>

### Access the Longhorn Dashboard
1. To access the Longhorn UI, click `Longhorn` in the left menu
2. Click the `Longhorn - Manage storage system via UI` and a new tab will open.
3. Click on the new tab to access the Longhorn dashboard.

<br/>

---

<br/>

### Change the Available Storage

1. The dashboard will show the available storage. The available storage can be increased.
2. Click on the `Node` tab at the top of the dashboard.
3. Next to the `localhost` node, under `Operation` on the right, click the burger menu and select `Edit Node and Disk`
4. At the bottom, look for `Storage Reserved`.
5. Change the value to `5` and click `Save`
6. Visit the main dashboard. The storage has been increased.

<br/>

---

<br/>

### Explore the Longhorn UI

Feel free to explore the Longhorn UI. It is possible to configure additional options for the Longhorn system, such as backups, and observe the storage/volumes currently running on the cluster.

<br/>
<br/>

---
---

## Lab 2 - Creating a Pod with a PV on Longhorn

This lab will cover the creation of a Pod with a Persistent Volume (PV) on Longhorn and fixing the degraded state of the PV.

<br/>

---

<br/>

### Deploy an NGINX Pod with a Persistent Volume

Run the following command to deploy a basic NGINX Pod and Persistent Volume which can be managed via Longhorn:

`kubectl apply -f nginx.yaml` {{ execute }}

<br/>

---

<br/>

### Review the Volume

Navigate back to the Longhorn UI and click on "Volume" at the top. A 1Gi volume is now bound to the NGINX Pod.

<br/>

---

<br/>

### Fix the Degraded State

The volume is currently in a `Degraded` state. This is due to Longhorn expecting three (3) replicas of the volume to be created, but only one (1) replica is available. This is due to the RKE cluster only having a single node. In a production environment, this would be resolved by adding additional nodes to the cluster.

To fix the degraded state:
1. Click on the menu on the right of the volume, under `Operation`, and select `Update Replicas Count`
2. Change the `Replica Count` to `1` and click `Save`
3. Navigate back to the main dashboard and observe the volume is now healthy

<BR/>

> **NOTE:**<BR/>Three (3) replicas is the default setting for Longhorn and recommended to provide resilience in the event of a worker node failure. In a production environment, it is recommended to have at least three (3) worker nodes in a cluster to ensure the Longhorn volumes are correctly replicated and healthy.

<br/>
<br/>

---
---

## Lab 3 - Destroy the RKE Cluster

The RKE cluster is no longer required and can be removed. It is critical to destroy the RKE cluster at this point, otherwise the environment will conflict with upcoming labs. The following steps will remove the RKE cluster from the Rancher MCM UI and destroy the cluster.

<br/>

---

<br/>

### Remove the RKE cluster:

`cd /ab/labs/rancher/rancher-basics/3-longhorn && rke remove` {{ execute }}

> **NOTE:**<BR/>Enter `y` at the prompt to confirm the removal of the cluster.

<br/>

---

<br/>

### Post RKE Cluster Cleanup

The RKE cluster has been removed, but there are still some artifacts left over from the cluster. Run the following command to remove the artifacts:

`chmod +x /ab/labs/rancher/rancher-basics/2-rke/cleanup.sh && sudo bash /ab/labs/rancher/rancher-basics/3-longhorn/cleanup.sh` {{ execute }}

> **NOTE:**<BR/>The cleanup script will remove the RKE cluster artifacts, and ensure there are no conflicts with upcoming labs. Please be sure to run the cleanup script before proceeding with the rest of the course.

> **NOTE:**<BR/>Due to a restart of the Docker service, the Rancher MCM browser window may need to be refreshed.

<br/>

---

<br/>

### Remove the RKE Cluster from Rancher MCM

1. Return to the Rancher MCM UI via https://rancher-LABSERVERNAME
2. Under `Cluster Management`, note `rke-cluster` is now failing health checks
3. Click on the kebab menu on the right of the cluster and select `Delete`
4. Enter `rke-cluster` in the confirmation box and click `Delete`

<BR/>

> **NOTE:**<BR/>Under normal circumstances, the health check failure would be an indicator of a serious problem with a cluster, and would require investigation to determine why the Rancher UI and target cluster cannot communicate.

<br/>
<br/>

---
---

**Congrats! You have completed the Rancher Longhorn labs. You may now proceed with the rest of the course.**
