# AlphaBravo - RKE2 HA Lab

## In this section we learned about:

* [RKE2](https://rancher.com/docs/rke/latest/en/)

---

## Lab Overview

This lab will walk you through the process of deploying a highly available Kubernetes cluster using RKE2. RKE2 is a lightweight Kubernetes distribution that is designed to be deployed at scale. It is a great option for those looking to deploy Kubernetes at scale in a production environment.

**IMPORTANT**: This lab is very hands on and complex. It involves `ssh`ing into multiple hosts and running specific commands on each one. Please be sure to follow the instructions carefully and read the documentation provided.

---

## Bare Metal RKE2 Deployment

We have provided a baremetal server which has a virtual infrastructure deployed. Here, you will be able to deploy a full RKE2 HA cluster and test deployments.

### Baremetal IP Address

The Baremetal server IP address can be found in the Student Lab Assignment file. In the row containing your name, you will find an IP address. This is the address for your personal Baremetal server.

### Copy the RKE2 instruction file to your local environment

First, we need to copy the RKE2 installation instructions to your local environment. This will allow you to follow the instructions to install RKE2 on the Baremetal server.

Make sure you replace <yourBaremetalIP> with the IP address for your Baremetal server ffrom the student lab assignment file.

`scp -i ~/.ssh/ablabs_id_rsa.pem ubuntu@<yourBaremetalIP>:/home/ubuntu/rke2-install.txt /ab/labs/rancher/rke2-ha/rke2-install.txt` {{ copy }}

### Review the file

Open the file in VS Code and review the instructions. You will need to follow these instructions to install RKE2 on the Baremetal server. You will want to keep this file open as you will need to reference it throughout the lab.

`/ab/labs/rancher/rke2-ha/rke2-install.txt` {{ open }}

### Accessing the Baremetal Server via SSH

You will need to access the Baremetal server via SSH to complete the lab. You can use the following command to access the server. Make sure you replace <yourBaremetalIP> with the IP address for your Baremetal server ffrom the student lab assignment file.

`ssh -i ~/.ssh/ablabs_id_rsa.pem ubuntu@<yourBaremetalIP>` {{ copy }}

---

## Congratulations! You have completed the RKE2 HA lab!

