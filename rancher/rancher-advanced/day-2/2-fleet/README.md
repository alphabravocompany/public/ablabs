# Rancher Advanced: Day 2 - Continuous Delivery

### This lab will cover the following topics:

* GitOps
* Continuous Delivery
* Configuring Fleet to use GitOps
* Ingress Configuration
* Load Balancing Between Multiple Clusters
* Labeling Clusters for Specific Deployments

<br/>
<br/>

---
---

## Lab 1 - Infrastructure Setup

In this lab, Rancher MCM will make use of the `Continuous Delivery` feature to deploy an application to multiple clusters. The lab will make use of the two (2) K3d clusters that were deployed in the previous K3d lab. The following steps will configure the K3d clusters for the `Continuous Delivery` lab.

<br/>

---

<br/>

### Verify the K3d Clusters are Running

Before deploying an application, verify that the K3d clusters are running:

`k3d cluster list` {{ execute }}

> **NOTE:**<BR/>The output should show two (2) clusters, `lab-cluster1` and `lab-cluster2`.

<br/>

---

<br/>

### Update the Kubeconfig File

Update the `kubeconfig` file to include both K3d clusters by merging both cluster configurations into the default `kubeconfig` file, located at `~/.kube/config`:

Set the `KUBECONFIG` environment variable to the default `kubeconfig` file:
`export KUBECONFIG=~/.kube/config` {{ execute }}

<br/>

---

<br/>

### Merge the lab-cluster1 Configuration

Next, merge the `lab-cluster1` configuration into the default `kubeconfig` file:

`k3d kubeconfig merge lab-cluster1 -d` {{ execute }}

<br/>

---

<br/>

### Merge the lab-cluster2 Configuration

Next, merge the `lab-cluster2` configuration into the default `kubeconfig` file:

`k3d kubeconfig merge lab-cluster2 -d` {{ execute }}

<br/>

---

<br/>

### Set the Context to lab-cluster1

Set the `kubectl` context to `lab-cluster1`:

`kubectl config use-context k3d-lab-cluster1` {{ execute }}

<br/>

---

<br/>

### Verify the Context is Set to lab-cluster1

Verify the context is set to `lab-cluster1`:

`kubectl config current-context` {{ execute }}

<br/>

---

<br/>

### Add lab-cluster1 to Rancher MCM

1. Open the Rancher UI at https://rancher-LABSERVERNAME.
2. Use the Code Server password in the Lab Logins file, found in the Student Drive.
3. In the upper left click the burger menu, then click `Cluster Management`
4. Click the button that says `Import Existing` and select `Generic`
5. Enter a the cluster name `lab-cluster1` and click `Create`.
6. Follow the instructions to add the cluster to Rancher MCM.

<br/>

---

<br/>

### Add lab-cluster2 to Rancher MCM

Switch the `kubectl` context to `lab-cluster2`:

`kubectl config use-context k3d-lab-cluster2` {{ execute }}

> **NOTE:**<BR/>Follow the same steps as above to add `lab-cluster2` to Rancher MCM, but use the cluster name `lab-cluster2` on step 5.

<br/>
<br/>

---
---

## Lab 2 - Deploying an Application with Continuous Delivery

Rancher MCM's `Continuous Delivery` feature allows you to deploy applications to multiple clusters from a single source of truth. This source of truth is a Git repository. Rancher MCM will monitor the Git repository for changes and automatically deploy those changes to the clusters that are configured to receive them. Hence the term `GitOps`.

Rancher uses Fleet, which is GitOps at scale. Fleet is designed to manage up to a million clusters, but is also lightweight enough  for single cluster deployments as well. However, it really shines when a large scale of clusters are deployed and require management of multiple applications across those clusters. A single change to a Git repository can be deployed to all clusters, or a subset of clusters, depending on the configuration.

> **NOTE:**<BR/>Fleet is a separate project from Rancher, and can be installed on any Kubernetes cluster with Helm.

<br/>

---

<br/>

### Useful Links

* [Continuous Delivery with Fleet](https://ranchermanager.docs.rancher.com/how-to-guides/new-user-guides/deploy-apps-across-clusters/fleet)
* [Fleet](https://fleet.rancher.io/)
* [Fleet Github](https://github.com/rancher/fleet)

<br/>

---

<br/>

### Review the Continuous Delivery Manifest

This lab includes a YAML manifest that will be used to configure the `Continuous Delivery` feature. Open the `ranchercd.yml` file and review it:

`/ab/labs/rancher/rancher-advanced/day-2/2-fleet/ranchercd.yml` {{ open }}

#### What's Happening Here?
1. The API version is `fleet.cattle.io/v1alpha1`, which is the Fleet API version deployed in Kubernetes.
2. The KIND is `GitRepo`, which is the type of resource to be created.
3. The metadata defines the name of the resource and the namespace to deploy it to. In this use-case, the namespace is `fleet-default`.
4. The specification section defines the Git repository to monitor for changes, the branch to monitor, and the path to the directory to monitor for changes.
5. The `targets` section defines the clusters to deploy the application to. In this use-case, the application will be deployed to all clusters that have the label `env: dev` or `env: prod`.

> **NOTE:**<BR/>This manifest will be used in the next section to configure the `Continuous Delivery` feature.

<br/>

---

<br/>

### Configure Continuous Delivery

1. Open the Rancher UI at https://rancher-LABSERVERNAME.
2. Use the Code Server password in the Lab Logins file, found in the Student Drive.
3. In the upper left click the burger menu, then click `Continuous Delivery`.
4. Click the `Git Repos` link on the left menu.
5. Click the `Add Repository` button.
6. Click the `Edit as YAML` button in the lower right corner.
7. Delete all of the existing contents shown.
8. Copy the contents of the `ranchercd.yml` file and paste it into the YAML editor.
9. Click the `Create` button in the lower right corner to finish the configuration.

> **NOTE:**<BR/>The Git repo should show up in an Active state with `0/0` clusters ready. The next section will cover adding clusters to the Git repo.

<br/>

---

<br/>

### Configure the Clusters to Sync the Application

Now the Git Repo has been successfully configured, clusters can be configured to allow the application to be deployed to them. This is done by adding labels to the clusters. The labels are defined in the `targets` section of the `ranchercd.yml` file.

1. Open the Rancher UI at https://rancher-LABSERVERNAME.
2. Use the Code Server password in the Lab Logins file, found in the Student Drive.
3. In the upper left click the burger menu, then click `Cluster Management`.
4. Click the kebab menu on the far right of the `lab-cluster1` cluster.
5. Click the `Edit Config` button.
6. Under `Labels & Annotations`, click the `Add Label` button.
7. Enter `env` for the key and `dev` for the value.
8. Click the `Save` button at the bottom of the page.
9. Repeat steps 6-10 for the `lab-cluster2` cluster.
10. In the upper left click the burger menu, then click `Continuous Delivery`.
11. Click the `Clusters` link on the left menu. Both clusters should now be listed and in an `Active` state.
12. Click the `Git Repos` link on the left menu. The Git repo should now show `2/2` clusters ready.

> **NOTE:**<BR/>By adding the labels to the clusters, the application has been automatically deployed to each cluster. As the Git repo is updated, the application will be automatically be synced and updated on each cluster.

<br/>

---

<br/>

### Verify the Application is Deployed

It is possible to confirm the application has been deployed to each cluster by viewing the `Pods` for each cluster.

1. Click on the burger menu in the upper left corner and select `lab-cluster1` from the `Explore Cluster` list.
2. Click on `Workloads` in the left menu.
3. Click on `Pods`. Two Pods should be listed, `frontend` and `redis-master`.
4. Click on `Deployments`. Two Deployments should be listed, `frontend` and `redis-master`.
5. Repeat steps 1-4 for `lab-cluster2`.

> **NOTE:**<BR/>The `frontend` Pod is the application that was deployed by the `Continuous Delivery` feature. The `redis-master` Pod is the Redis database that is used by the application.

<br/>
<br/>

---
---

## Lab 3 - Creating Ingress for the Application

There are now two (2) clusters running the application, but there is no way to access the application from outside the cluster. Ingress will be used to expose the application to the outside world. This lab will cover creating an Ingress route for the currently deployed `Guestbook` application as well as a new application called `Hello` that will be deployed in a later lab.

<br/>

---

<br/>

### Useful Links

* [Traefik Ingress](https://doc.traefik.io/traefik/providers/kubernetes-ingress/)

### Create an Ingress Route for the Guestbook Application

In Rancher MCM, create an Ingress rule on each cluster to point to the `frontend` service:

1. Open the Rancher UI at https://rancher-LABSERVERNAME.
2. Use the Code Server password in the Lab Logins file, found in the Student Drive.
3. In the upper left click the burger menu, then click `lab-cluster1` from the `Explore Cluster` list.
4. Click the `Service Discovery` link on the left menu.
5. Click the `Ingresses` link on the left menu.
6. Click the `Create` button.
7. Fill in the required fields:
    * Namespace: `fleet-mc-helm-example`
    * Name: `guestbook`
    * Request Host: `guestbook.LABSERVERNAME`
    * Path: Leave as prefix. Enter the value: `/`
    * Target Service: Select `frontend` from the dropdown
    * Port: Select `80` from the dropdown
8. Click the `Create` button in the lower right corner.
9. Repeat steps 3-8 for `lab-cluster2`, using all of the same values.

<br/>

---

<br/>

### Create an Ingress Route for the Hello Application

In Rancher MCM, create an Ingress rule on the `lab-cluster2` server to point to the `hello` service. Follow the same steps as above, but use the following values:

```
* Namespace: `fleet-mc-helm-example`
* Name: `hello`
* Request Host: `hello.LABSERVERNAME`
* Path: Leave as prefix. Enter the value: `/`
* Target Service: Select `frontend` from the dropdown
* Port: Select `80` from the dropdown
```

<br/>

---

<br/>

### Verify Guestbook is Accessible

Now that the Ingress routes has been defined, visit the Guestbook application at the following URL:

http://guestbook.LABSERVERNAME

> **NOTE:**<BR/>It may take a few seconds for the page to display. Additionally, there is a caching feature and a short duration cookie in place. Refreshing the web page every 10 seconds will update the `PodIP` and `PodName` values.

> **NOTE:**<BR/>The lab is load balancing through the HAProxy instance set up in a previous lab, which sends traffic to the frontend pods in 2 separate K3s clusters. This is why the `PodIP` and `PodName` change when the page is refreshed.

<br/>
<br/>

---
---

## Lab 4 - Simulating Cluster Failure

Now that a highly available multi-cluster configuration has been deployed, it is time to simulate the failure of a cluster. This lab will cover simulating the failure of `lab-cluster2` and verifying the application is still accessible. The following configuration is currently in place:

* Two (2) K3d clusters have been deployed with K3s
* The clusters are behind a load balancer (HAproxy)
* Continuous Delivery has been configured to deploy an application to both clusters automatically
* The guestbook application has been deployed to both clusters due to the labels on the clusters
* Ingress has been deployed on both clusters that points to the `frontend` service

<br/>

---

<br/>

### Verify the Distribution of Traffic

Run the command below to see the distribution of traffic between the clusters. A rough 50/50 split should be seen between the two (2) `PodIP` values:

`for ((i=1;i<=10;i++)); do curl -L -0 -v http://guestbook.LABSERVERNAME 2>&1; done | grep PodName | awk -F " " {' print $1 '} | awk -F "=" {' print $2 '} | awk -F "<" {' print $1 '} | sort | uniq -c | column -t` {{ execute }}

#### What's Happening Here?
1. The `for` loop will run the command 10 times.
2. The `curl` command will send a request to the `guestbook` URL and follow any redirects.
3. The `grep` command will filter the output to only show the `PodName` value.
4. The `awk` command will filter the output to only show the `PodIP` value.
5. The `sort` command will sort the output.
6. The `uniq` command will remove duplicate lines.
7. The `column` command will format the output into columns.

<br/>

---

<br/>

### View the Cluster Health

It's possible to observe the health of the cluster via the Web interface of the HAproxy load balancer. Click the link below and login with the following credentials:

| | |
| --- | --- |
| URL: | http://haproxy.LABSERVERNAME/stats |
| User: | admin |
| Password: | changemeplease!! |

<br/>

---

<br/>

### Take a Cluster Offline

In the terminal on the lab server, stop the `lab-cluster2` cluster:

`k3d cluster stop lab-cluster2` {{ execute }}

<br/>

---

<br/>

### Verify the Cluster is Offline

On the HAproxy web interface, verify the `lab-cluster2` cluster is offline. The `lab-cluster2` cluster should be red and the `lab-cluster1` cluster should be green.

Visit the Guestbook application a few times to verify the application is still accessible:

http://guestbook.LABSERVERNAME

> **NOTE:**<BR/>Due to the configuration of HAproxy, it may take a few seconds for the cluster to be marked as offline in the HAproxy web interface. Refresh the page a few times to verify the application is still accessible.

<br/>

---

<br/>

### Verify the Distribution of Traffic

Run the curl test again:
`for ((i=1;i<=10;i++)); do curl -L -0 -v http://guestbook.LABSERVERNAME 2>&1; done | grep PodName | awk -F " " {' print $1 '} | awk -F "=" {' print $2 '} | awk -F "<" {' print $1 '} | sort | uniq -c | column -t` {{ execute }}

> **NOTE:**<BR/>Observe all traffic is now returning from a single PodIP via the lab-cluster1, which is the only cluster still online.

> **NOTE:**<BR/>With some straight forward cluster configuration, multi-cluster GitOps deployments, and robust infrastructure engineering, the Guestbook application was able to survive an entire cluster failure without impacting any customer facing services.

<br/>

---

<br/>

### Bring the Cluster Back Online

Run the command below to bring the `lab-cluster2` cluster back online:

`k3d cluster start lab-cluster2` {{ execute }}

> **NOTE:**<BR/>Visit the HAproxy web interface to verify the `lab-cluster2` cluster is back online. It may take a few seconds for the cluster to come back online due to the HAproxy health checks.

<br/>
<br/>

---
---

## Lab 5 - Continuous Delivery Updates and Cluster "Tiers" (Dev/Test/Prod/etc)

Updating one or more applications can often be a struggle, consuming large amounts of time between development and infrastructure teams. With Gitops, the Continuous Delivery system is able to monitor the Git repo for changes and automatically apply them when updates are published a specific branch.


In this lab, the clusters will be labeled to represent different tiers of clusters, such as `dev` and `prod`. The `Continuous Delivery` feature has been configured configured to deploy the application to the `dev` and `prod` clusters based on the `env` label of the cluster. The `dev` label deploys the `Guestbook` application, while the `prod` label deploys the `Hello` application.

<br/>

---

<br/>

### Verify the Load Balancing Between Clusters

First, visit the Guestbook site a few times to make sure we are seeing the traffic switching back and forth.

http://guestbook.LABSERVERNAME

<br/>

---

<br/>

### Switching Labels to Deploy a Different Application

Assume `lab-cluster1` is `dev`, and `lab-cluster2` is `prod`. Update the `lab-cluster2` cluster to reflect the expected `prod` label.

1. Open the Rancher UI at https://rancher-LABSERVERNAME.
2. Use the Code Server password in the Lab Logins file, found in the Student Drive.
3. In the upper left click the burger menu, then click `Cluster Management`.
4. Click the kebab menu on the far right of the `lab-cluster2` cluster.
5. Click the `Edit Config` button.
6. Update the `env` label value from `dev` to `prod`.
7. Click the `Save` button at the bottom of the page.
8. In the upper left click the burger menu, then click `Continuous Delivery`.
9. Click the `Clusters` link on the left menu. Both clusters should now be listed and in an `Active` state.
10. Click the checkbox next to `lab-cluster2`.
11. Click on the `Force Update` button above. This will force the `Continuous Delivery` feature to re-evaluate the cluster labels and deploy the application to the `lab-cluster2` cluster.
12. Wait until the cluster is in an `Active` state.
13. In the upper left click the burger menu, then click `lab-cluster2` from the `Explore Cluster` list.
14. Click on `Workloads` in the left menu.
15. Click on `Pods`. Two Pods should be listed, `frontend` and `redis-master`.
16. Observe the frontend Pod is now running the `alphabravo/hello-world` image.

> **NOTE:**<BR/>The `Continuous Delivery` feature has automatically deployed the `Hello` application to the `lab-cluster2` cluster due to the `prod` label being applied. Multiple applications can be deployed to multiple clusters from a single source of truth.

<br/>

---

<br/>

### Remove Ingress for the Guestbook Application

Now that the `Hello` application has been deployed to the `lab-cluster2` cluster, remove the Ingress route for the `Guestbook` application:

1. Open the Rancher UI at https://rancher-LABSERVERNAME.
2. Use the Code Server password in the Lab Logins file, found in the Student Drive.
3. In the upper left click the burger menu, then click `lab-cluster2` from the `Explore Cluster` list.
4. Click the `Service Discovery` link on the left menu.
5. Click the `Ingresses` link on the left menu.
6. Click the kebab menu on the far right of the `guestbook` Ingress.
7. Click the `Delete` button.

<br/>

---

<br/>

### Verify the Applications are Accessible

The clusters have been separated into `dev` and `prod` environments. The `dev` labeled cluster is running the `Guestbook` application, while the `prod` labeled cluster is running the `Hello` application. Visit the following URLs to verify the applications are accessible:

| | |
| --- | --- |
| Guestbook: | http://guestbook.LABSERVERNAME |
| Hello World: | http://hello.LABSERVERNAME |

> **NOTE:**<BR/>It will be possible to visit the Hello application after a few minutes. If the `Hello from AlphaBravo Training` message is not displayed, wait a few minutes and try again.

<br/>

---

<br/>

### Conclusion

Not only does Continuous Delivery with Rancher Fleet allow for deploying the ***SAME*** configuration across multiple clusters from the same source Git repository, it can also deploy a ***DIFFERENT*** configuration from the same source Git repository to ***DIFFERENT*** tiers of clusters, all from a single configuration source.

<br/>
<br/>

---
---

**Congrats! You have completed the Rancher Advanced: Day 2 - Continuous Delivery labs. You have completed Rancher Advanced training!**