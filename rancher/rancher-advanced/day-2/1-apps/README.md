# Rancher Advanced: Day 2 - Applications

### This lab will cover the following topics:

* CIS Benchmarks
* Monitoring with Prometheus and Grafana

> **NOTE:**<BR/>Backups are a fairly straight forward affair simply requiring a S3 target bucket and authentication credentials. As this is a simple process, the labs will not cover this topic.

> **NOTE:**<BR/>Rancher MCM does make the initial installation of Istio very easy, but the configuration and implementation of Istio is beyond the scope of this lab. Istio is a very complex topic and requires it's own training and labs to cover the topics in depth.

<br/>
<br/>

---
---

## Lab 1 - CIS Benchmarks

The Center for Internet Security (CIS) is a non-profit organization that develops and publishes security benchmarks for various technologies. The CIS Kubernetes Benchmark provides a set of best practices for securing Kubernetes clusters.

The rancher-cis-benchmark app leverages `kube-bench`, an open-source tool from Aqua Security, to check clusters for CIS Kubernetes Benchmark compliance. Also, to generate a cluster-wide report, the application utilizes sonobuoy, and open-source tool from VMware, for report aggregation.

### Useful Links

* [Apps / Helm Charts](https://rancher.com/docs/rancher/v2.6/en/helm-charts/)
* [CIS Benchmarks](https://rancher.com/docs/rancher/v2.6/en/cis-scans/)
* [Aqua Security's kube-bench](https://github.com/aquasecurity/kube-bench)
* [VMware Sonobuoy](https://github.com/vmware-tanzu/sonobuoy)
* [RKE2 Self Assessment Guide for CIS 1.7 and K8s 1.25](https://ranchermanager.docs.rancher.com/reference-guides/rancher-security/hardening-guides/rke2-hardening-guide/rke2-self-assessment-guide-with-cis-v1.7-k8s-v1.25)

<br/>

---

<br/>

### Deploy an RKE2 Cluster

In order to deploy the CIS Benchmark app, we need a cluster to deploy it to. To show the difference between a permissive and hardened cluster, deploy an RKE2 cluster with the following command:

First, destroy any existing RKE2 clusters:
`sudo /usr/local/bin/rke2-uninstall.sh && sudo rm /ab/kubeconfig/rke2.yml` {{ execute }}

Next, deploy a permissive RKE2 cluster:
`sudo curl -sfL https://get.rke2.io | sudo sh - && sudo systemctl start rke2-server.service && sudo cp /etc/rancher/rke2/rke2.yaml /ab/kubeconfig/rke2.yml && sudo chmod 0644 /ab/kubeconfig/rke2.yml && export KUBECONFIG=/ab/kubeconfig/rke2.yml`  {{ execute }}

#### What's Happening Here?
1. Using `curl`, download the RKE2 installer script from the RKE2 Github repository.
2. Run the installer script via `sudo sh -`.
3. Start the RKE2 service.
4. Copy the RKE2 configuration file to the `/ab/kubeconfig` directory.
5. Set the permissions on the configuration file.
6. Set the `KUBECONFIG` environment variable to the RKE2 configuration file.

<br/>

---

<br/>

### Add the RKE2 Cluster to Rancher MCM

Go to the Rancher MCM UI and add this as a new cluster called `rke2-cluster`.

1. Open the Rancher UI at https://rancher-LABSERVERNAME
2. Use the Code Server password in the Lab Logins file, found in the Student Drive
3. In the upper left click the burger menu, then click `Cluster Management`
4. Follow the instructions to add a new cluster, naming the cluster `rke2-cluster`.
5. Verify the cluster is online and ready to use.

<br/>

---

<br/>

### Deploy the CIS Benchmark App

Deploy the CIS Helm Chart and run a scan:

1. In the upper left, click the burger menu, then click `rke2-cluster` under `Explore Cluster`.
2. At the bottom of the left menu, click `Cluster Tools`.
3. Search for `CIS Benchmark` and click the blue `Install` button.
4. Click `Next` as the default settings are fine for this lab.
5. Click `Install` to install the chart with the default settings.

> **NOTE:**<BR/>Once the installation begins, a terminal window will open and begin to show the progress of the installation. Installation is complete when the terminal window shows the `SUCCESS` message. The terminal window can be closed once the installation is complete.

> **NOTE:**<BR/>In the left menu, the `CIS Benchmark` link will now be available.

<BR/>

---

<BR/>

### View the CIS Benchmark Version

It is important to understand the what versions of Kubernetes the CIS Benchmark version supports. This will help you determine if the version of Kubernetes you are running is supported by the CIS Benchmark version you are using.

1. Click on the arrow next to `CIS Benchmark` link in the left menu.
2. Click on `Benchmark Version` in the left menu.
3. Scroll to the bottom of the page and look for the `rke2-cis-1.23-profile-permissive` version.
4. Note the minimum and maximum versions of Kubernetes supported by this version of the CIS Benchmark.

> **NOTE:**<BR/>The `rke2-cis-1.23-profile-permissive` version of the CIS Benchmark supports Kubernetes versions 1.22 through current releases. If older versions of Kubernetes are in use, previous benchmark versions can be installed and used.

<BR/>

---

<BR/>

### Run a Permissive Scan

A permissive scan skips a set of tests because they are too obstructive for a user who is getting started with Kubernetes.

Run a permissive scan against the cluster:
1. Click the `Scan` link under CIS Benchmark in the left menu.
2. Click the `Create` button.
3. Select the `rke2-cis-1.23-profile-permissive` scan and click `Create`.

> **NOTE:**<BR/>The scan will take a few minutes to complete. Once complete, the results will be displayed in the browser, showing the `State` of the scan result.

<BR/>

---

<BR/>

### Review the Scan Results

Review the scan results:

1. Click on the name of the scan.
2. Scroll through the results and review the `State` of each item.

> **NOTE:**<BR/>Each item shown in the list will have a description of the corrective action which must be taken to resolve the issue. This is a great starting point for developing a security plan for your cluster.

<BR/>

---

<BR/>

### Run a Hardened Scan

A hardened scan runs all tests without skipping. This option is recommended for production clusters, advanced users, and security professionals.

Run a hardened scan against the cluster:
1. Click the `Scan` link under CIS Benchmark in the left menu.
2. Click the `Create` button.
3. Select the `rke2-cis-1.23-profile-hardened` scan and click `Create`.
4. Allow the scan to complete.
5. Review the results of the hardened scan.

> **NOTE:**<BR/>Is it recommened to set a regular scanning schedule and periodically review the results of the scans as part of the ongoing maintenance of a cluster. Additionally, this will help to identify any changes over time.

<br/>
<br/>

---
---

## Lab 2 - Monitoring and Alerting

Deploying a full monitoring and alerting stack with Rancher MCM is incredibly easy. Out of the box, Rancher MCM will deploy a full monitoring and alerting stack consisting of Prometheus, Alertmanager and Grafana along with useful pre-configured dashboards. This is a great starting point for developing a monitoring and alerting plan for your cluster and applications.

> **NOTE:**<BR/>While the build-in dashboards are a great start, there is still a large amount of planning and implementation that goes into deciding what elements of a cluster are important to monitor, what the monitoring thresholds are, and what the alerting workflow looks like before a complete solution can be implemented.

<br/>

---

<br/>

### Useful Links
* [Apps / Helm Charts](https://rancher.com/docs/rancher/v2.6/en/helm-charts/)
* [Monitoring and Alerting](https://rancher.com/docs/rancher/v2.6/en/monitoring-alerting/)
* [Prometheus](https://prometheus.io/)
* [Alertmanager](https://prometheus.io/docs/alerting/alertmanager/)
* [Grafana](https://grafana.com/)

<br/>

---

<br/>

### Deploy the Monitoring Stack

Deploy the monitoring stack to the RKE2 cluster:

1. Open the Rancher UI at https://rancher-LABSERVERNAME.
2. Use the Code Server password in the Lab Logins file, found in the Student Drive.
3. In the upper left click the burger menu.
4. Click on `rke2-cluster` under `Explore Cluster`.
5. At the bottom of the left menu, click `Cluster Tools`.
6. Search for `Monitoring` and click the blue `Install` button.
7. Click `Next` as the default settings are fine for this lab.
8. Ensure the `Cluster Type` is set to `RKE2`.
8. Click `Install` to install the chart.

> **NOTE:**<BR/>Once the installation begins, a terminal window will open and begin to show the progress of the installation. Installation is complete when the terminal window shows the `SUCCESS` message. The terminal window can be closed once the installation is complete.

> **NOTE:**<BR/>The monitoring stack will take a few minutes to deploy. Once complete, the `Monitoring` link will be available in the left menu.

<br/>

---

<br/>

### Reviewing the Main Grafana Dashboard

1. Once complete, click `Monitoring` in the left menu.
2. Five (5) options will be presented. For now, click on `Grafana`.
3. This will open a new browser tab with the Grafana dashboard.

> **NOTE:**<BR/>On the Grafana dashboard, metrics will begin to populate after a few minutes. The metrics will show the CPU, memory and disk usage of the cluster. If you don't see any updates after a minute or two, try clicking the refresh button in your browser.

<br/>

---

<br/>

### Reviewing the Pre-Defined Dashboards

Rancher has included a number of pre-defined dashboards that can be used to monitor the cluster. These dashboards can be used as-is or as a starting point for developing custom dashboards.

1. In the Grafana Dashboard, click on the 4 boxes on the left and select `Dashboards`.
2. In the updated window, click on the `General` folder to show the pre-defined dashboards.
3. Click on the `Kubernetes/API Server` to review the cluster API server metrics.
4. Observe how new metric data is populated in the dashboard as time passes.

> **NOTE:**<BR/>There are a number of pre-defined dashboards available. Take some time to review them and see what metrics are available. Add and remove workloads to see how the metrics change.

> **NOTE:**<BR/>Kubernetes Monitoring is a very complex topic and will take time to understand all of the components of the system. Visit the Rancher links provided and collaborate with your team and AlphaBravo to help develop an action plan for full implementation.

<br/>

---

<br/>

### Cleanup

Remove the `rke2-cluster` from Rancher MCM as it's no longer required for the lab. Once the cluster has been removed from MCM, remove it from the lab environment:

`sudo /usr/local/bin/rke2-uninstall.sh` {{ execute }}

<br/>
<br/>

---
---

**Congrats! You have completed the Rancher Advanced: Day 2 - Applications labs. You may now proceed with the rest of the course.**