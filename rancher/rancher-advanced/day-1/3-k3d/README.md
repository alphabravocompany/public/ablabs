# Rancher Advanced: Day 1 - K3d

### This lab will cover the following topics:

* Deploying local clusters with K3d
* Load balancing multiple clusters with HAProxy

<br/>
<br/>

---
---

## Lab 1 - Deploying K3d

This lab will cover the deployment of K3d, a lightweight Kubernetes distribution by Rancher. It uses Docker to deploy virtual "nodes" via containers on a local environment, which allows users to simulate a single or multi-node infrastructure. Once the virtual nodes are deployed, K3d will deploy a Kubernetes cluster on those nodes using K3s.

> **NOTE:**<BR/>There are numerous configuration switches available to create a cluster. To learn more about them [visit the K3d cluster create documentation](https://k3d.io/v5.6.0/usage/commands/k3d_cluster_create/).

<br/>

---

<br/>

### Useful Links

* [K3d](https://k3d.io/)
* [K3d Guides](https://k3d.io/v5.6.0/usage/configfile/)
* [K3d Github](https://github.com/k3d-io/k3d)

<br/>

---

<br/>

### Change the Directory

Change to the K3d labs directory:

`cd /ab/labs/rancher/rancher-advanced/day-1/3-k3d` {{ execute }}

<br/>

---

<br/>

### Determine if a Cluster is Running

Before deploying a new cluster, determine if a cluster is already running:

`k3d cluster list` {{ execute }}

If any clusters are shown, remove them before continuing:

`k3d cluster delete --all` {{ execute }}

<br/>

---

<br/>

### Deploy a Fresh Cluster

Run the following command to deploy a new cluster:

`k3d cluster create lab-cluster1 --volume /ab/k3dvol:/tmp/k3dvol --api-port 16443 --servers 1 --agents 3 -p 18080:80@loadbalancer -p 18443:443@loadbalancer -p "30000-30010:30000-30010@server:0" && k3d kubeconfig get lab-cluster1 > /ab/kubeconfig/k3d-lab-cluster1.yaml` {{ execute }}


#### What's Happening Here?
1. K3d is creating a new cluster named `lab-cluster1`.
2. The cluster mounts the `/ab/k3dvol` from the local machine to the cluster at `/tmp/k3dvol`.
3. The cluster will use port `16443` for the Kubernetes API.
4. The cluster will have one (1) server node and three (3) agent nodes.
5. The cluster will open ports `18080` and `18443` on the local machine to access the load balancer.
6. The cluster will open ports `30000` through `30010` on the server node to access the cluster services.
7. K3d outputs the `kubeconfig` file to the `/ab/kubeconfig` directory.

<br/>

---

<br/>

### Set KUBECONFIG Environment Variable

Using the `export` command, set the `KUBECONFIG` environment variable to the newly created `kubeconfig` file:

`export KUBECONFIG=/ab/kubeconfig/k3d-lab-cluster1.yaml` {{ execute }}

<br/>

---

<br/>

### Determine if the Cluster is Running

Run the following command to determine if the cluster is running by reviewing the nodes:

`kubectl get nodes` {{ execute }}

> **NOTE:**<BR/>This is a full development grade K3s Kubernetes cluster running on the local machine, and will perform as such. It is not recommended to run K3d in production.

<br/>
<br/>

---
---

## Lab 2 - Deploying a Second K3d Cluster

The K3d cluster is running locally in the lab environment using Docker containers. Unlike K3s, it is possible can run multiple clusters on the same machine at the same time, assuming the machine has enough resources available to do so. This provides an opportunity to test some multi-cluster operations.

<br/>

---

<br/>

### Deploy a Second Cluster

Apply the example configuration file to create a 2nd cluster:

`k3d cluster create lab-cluster2 --volume /ab/k3dvol:/tmp/k3dvol --api-port 16444 --servers 1 --agents 3 -p 18081:80@loadbalancer -p 18444:443@loadbalancer -p "30011-30020:30011-30020@server:0" && k3d kubeconfig get lab-cluster2 > /ab/kubeconfig/k3d-lab-cluster2.yaml` {{ execute }}

<br/>

---

<br/>

### Verify the Clusters are Running

Run the following command to see that 2 clusters are now up and running on the lab server:

`k3d cluster list` {{ execute }}

> **NOTE:**<BR/>Two clusters should be listed: `lab-cluster1` and `lab-cluster2`.

<br/>

---

<br/>

### Change the Context to the Second Cluster

Export the `KUBECONFIG` environment variable:

`export KUBECONFIG=/ab/kubeconfig/k3d-lab-cluster2.yaml` {{ execute }}

<br/>

---

<br/>

### Verify the Second Cluster is Running

Run the following command to view the nodes on the second cluster:

`kubectl get nodes` {{ execute }}

> **NOTE:**<BR/>The `get nodes` command should show four (4) nodes: one (1) server node and three (3) agent nodes.

<br/>
<br/>

---
---

## Lab 3 - Deploying a Load Balancer

This lab will cover the deployment of a load balancer to manage traffic between the two K3d clusters. This will demonstrate a multi-cluster environment with a single DNS name and load balancer.

<br/>

---

<br/>

### Start the Load Balancer

Start the load balancer with the following command:

`sudo systemctl start haproxy` {{ execute }}

<br/>

---

<br/>

### Review the Load Balancer Configuration

The lab environment has a load balancer pre-installed and configured. The load balancer uses a specifically crafted HAProxy configuration referencing the local K3d clusters as backend targets.

Visit the HAProxy UI to see this in action:
https://haproxy.LABSERVERNAME/stats

And login with the following credentials:
**User:** admin
**Pass:** changemeplease!!

<br/>

---

<br/>

### K3d Cluster Status

In the HAProxy UI, note the status of the two K3d clusters. The status should be `UP` in green for `k8s1` and `k8s2`.

> **NOTE:**<BR/>The HAProxy config is used for other labs. As a result, other endpoints will be shown in a `DOWN` state in red. For now, this lab is only concerned with the status of the two K3d clusters.

<br/>

---

<br/>

### Cluster Cleanup

The two K3d clusters will not be cleaned up at this time, as they will be used in an upcoming lab. Please leave them running.


<br/>
<br/>

---
---

**Congrats! You have completed the Rancher Advanced: Day 1 - K3d labs and finished all of the Day 1 lab courses.**