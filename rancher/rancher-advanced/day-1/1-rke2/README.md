# Rancher Advanced: Day 1 - RKE2 and NeuVector


### This lab will cover the following topics:

* Deploying RKE2
* RKE2 Configuration File Deployment
* Implementing NeuVector

<br/>
<br/>

---
---

### Introduction
This lab dives deeper into RKE2, also known as RKE Government. This next-generation Kubernetes distribution by Rancher is tailored to meet the strict security and compliance requirements of the U.S. Federal Government sector.

#### Key Details About RKE2
* RKE2 provides configurations that comply with the CIS Kubernetes Benchmark v1.5 or v1.6.
* It supports FIPS 140-2 compliance.
* The build pipeline uses Trivy to scan components for CVEs regularly.

<br/>
<br/>

---
---

## Lab 1: Deploying RKE2

This lab covers the deployment of RKE2 and the essential features it offers to ensure a secure and compliant Kubernetes environment.

> **NOTE:**<BR/>The commands in this lab download the `stable` version of RKE2.

> **NOTE:**<BR/>Specific versions of RKE2 can be downloaded to run a specific version of Kubernetes. For more information on the supported versions, please refer to [Configuring the Linux Installation Script](https://docs.rke2.io/install/configuration#configuring-the-linux-installation-script).

<br/>

---

<br/>

### Useful Links
* [Official RKE2 Documentation](https://docs.rke2.io/)
* [Trivy Security Tool](https://github.com/aquasecurity/trivy)

<br/>

---

<br/>

### Change the Directory

Change to the RKE2 labs directory:

`cd /ab/labs/rancher/rancher-advanced/day-1/1-rke2` {{ execute }}

<br/>

---

<br/>

### Running Commands to Deploy RKE2

Run the following command below to set up a single-node RKE2 deployment:

`sudo curl -sfL https://get.rke2.io | sudo sh - && sudo systemctl start rke2-server.service && sudo mkdir -p /ab/kubeconfig && sudo chmod 0777 /ab/kubeconfig && sudo cp /etc/rancher/rke2/rke2.yaml /ab/kubeconfig/rke2.yml && sudo chmod 0644 /ab/kubeconfig/rke2.yml && export KUBECONFIG=/ab/kubeconfig/rke2.yml` {{ execute }}

#### What's Happening Here?
The above command is performing the following actions:
1. Using `curl`, download the RKE2 installer script.
2. Run the installer script.
3. Create a directory to store the kubeconfig file.
4. Change the permissions of the directory to allow all users to access it.
5. Copy the `kubeconfig` file to the directory.
6. Change the permissions of the `kubeconfig` file to allow all users to access it.
7. Set the `KUBECONFIG` environment variable to the `kubeconfig` file so that `kubectl` can access the RKE2 cluster.

<br/>

---

<br/>

### Verify the RKE2 Deployment

Using Kubectl, verify that RKE2 is deployed and running:

`kubectl get nodes` {{ execute }}

> **NOTE:**<BR/>The node will be in the `NotReady` state until the installation is complete. It may be required to run the command multiple times until the node is in the `Ready` state, or to pair it with the `watch` command.

<br/>

---

<br/>

### Cleanup

Now that RKE2 is deployed, this portion of the lab is complete. In the next lab, RKE2 will be deployed using a configuration file.

Execute the following commands to clean up the resources:

`sudo /usr/local/bin/rke2-uninstall.sh && sudo rm /ab/kubeconfig/rke2.yml` {{ execute }}`

#### What's Happening Here?
The above commands are performing the following actions:
1. Uninstalling RKE2 using the uninstall script that is automatically generated during the installation process.
2. Removing the kubeconfig file that was created as part of the initial provisioning commands.

<br/>
<br/>

---
---

## Lab 2 - RKE2 Configuration File Deployment

This lab will cover deploying RKE2 using a configuration file. This method of deployment is useful for automating the deployment of RKE2 and ensuring that the same configuration is used across multiple deployments.

Review the [RKE2 config file official documentation](https://docs.rke2.io/install/configuration) to understand the configuration options available.

<br/>

---

<br/>

### Review the RKE2 Configuration File

View the sample configuration file located at:

`/ab/labs/rancher/rancher-advanced/day-1/1-rke2/rke2-config.yaml` {{ open }}

#### What's In This File?
This configuration file is setting the following settings:
1. The `kubeconfig` access permissions.
2. The `kubeconfig` output file location.
3. The CNI plugin to use.
4. The TLS SAN (Subject Alternative Name) to use for the server certificate.
5. Node labels to apply to the server node.
6. The frequency of the cluster's etcd snapshots.
7. The NodePort range to use for services.
8. Whether to enable debug logging.

<br/>

---

<br/>

### Copy the Configuration File To The Correct Location

Run the following commands to bring up the RKE2 server:

`sudo mkdir -p /etc/rancher/rke2/config.yaml.d && sudo cp /ab/labs/rancher/rancher-advanced/day-1/1-rke2/rke2-config.yaml /etc/rancher/rke2/config.yaml.d/config.yaml && sudo curl -sfL https://get.rke2.io | sudo sh - && sudo systemctl start rke2-server.service` {{ execute }}

#### What's Happening Here?
The above commands are performing the following actions:
1. Making a directory to store the configuration file.
2. Copying the configuration file to the directory.
3. Downloading the RKE2 installer script.
4. Running the installer script.

<br/>

---

<br/>

Confirm that the RKE2 server node is in the `READY` state:

`export KUBECONFIG=/ab/kubeconfig/rke2-lab.yml && kubectl get nodes` {{ execute }}

> **NOTE:**<BR/>The node will be in the `NotReady` state until the installation is complete. It may be required to run the command multiple times until the node is in the `Ready` state, or to pair it with the `watch` command.

<br/>

---

<br/>

Finally, integrate this cluster into the Rancher UI and name it `rke2-lab`. This step should be familiar at this point, but the exact steps can be found in previous labs.

> **NOTE:**<BR/>DO NOT SKIP THIS STEP. Integration with Rancher MCM is required for the next lab.

> **NOTE:**<BR/>The Rancher UI can be accessed at: [Rancher Lab UI](https://rancher-LABSERVERNAME).

<br/>
<br/>

---
---

## Lab 3: Implementing NeuVector

This lab will cover deploying the NeuVector container security platform, which provides functionalities like a container firewall, vulnerability scanning, and compliance, using the Rancher UI.

<br/>

---

<br/>

### Deploying NeuVector Using the Rancher UI

1. Open the Rancher UI at https://rancher-LABSERVERNAME.
2. Use the Code Server password in the Lab Logins file, found in the Student Drive.
3. Click on the burger menu in the top left corner.
4. Select the `rke2-lab` cluster under `Explore Cluster`.
5. Click on `Cluster Tools` in the bottom left corner.
6. Find `NeuVector` and click the `Install` button.
7. Choose `Customize Helm options before install` and proceed.
8. **IMPORTANT:** Under `Edit Options`, select `Container Runtime`.
9. **IMPORTANT:** Deselect `Docker Runtime` and select `k3s Container Runtime`.
10. Click `Next` to continue.
11. Click `Install` to begin the installation.
12. Complete the installation process.
13. After successful installation, access the NeuVector UI from the left menu. It might take a couple of minutes to load.
14. Accept the EULA, and you will be directed to the console.
15. For further guidance on navigating the NeuVector UI, visit [the NeuVector Documentation](https://open-docs.neuvector.com/navigation/navigation).

<br/>

---

<br/>

### Cleanup: Lab 3
Execute the given commands and ensure you remove the `rke2-lab` cluster from the Rancher UI.

`sudo /usr/local/bin/rke2-uninstall.sh && sudo rm /ab/kubeconfig/rke2.yml` {{ execute }}

> **NOTE:**<BR/>Remember to remove the `rke2-lab` cluster from the Rancher user interface.

#### What's Happening Here?
The above commands are performing the following actions:
1. Uninstalling RKE2 using the uninstall script that is automatically generated during the installation process.
2. Removing the kubeconfig file that was created as part of the initial provisioning commands.

<br/>
<br/>

---
---

**Congrats! You've successfully completed the RKE2 and NeuVector labs. You may now proceed with the rest of the course.**