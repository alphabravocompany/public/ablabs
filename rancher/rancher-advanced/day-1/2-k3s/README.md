# Rancher Advanced: Day 1 - K3s

### This lab will cover the following topics:

* Deploying K3s
* Upgrading K3s from the Rancher UI

<br/>
<br/>

---
---

### Introduction
This lab dives deeper into K3s, a lightweight Kubernetes distribution by Rancher. K3s is ideal for resource-constrained environments such as IoT and edge computing. It is also a great option for CI/CD pipelines, development, and testing.

<br/>
<br/>

---
---

## Lab 1: Deploying K3s

This lab will cover the deployment of K3s and the essential features it offers to ensure a secure and compliant Kubernetes environment.

> **NOTE:**<BR/>The commands in this lab download a specific version of K3s.

> **NOTE:**<BR/>The versioning of K3s is directly tied to the version of Kubernetes it will deploy. For a list of supported K3s releases, please refer to [K3s Releases](https://github.com/k3s-io/k3s/releases) and [K3s Environment Variables](https://docs.k3s.io/reference/env-variables) on how to specify a specific version of K3s to download.

<br/>

---

<br/>

### Useful Links

* [K3s](https://k3s.io/)
* [K3s Documentation](https://docs.k3s.io/)
* [K3s GitHub](https://github.com/k3s-io/k3s)

<br/>

---

<br/>

### Change the Directory

Change to the K3s labs directory:

`cd /ab/labs/rancher/rancher-advanced/day-1/2-k3s` {{ execute }}

<br/>

---

<br/>

### Running Commands to Deploy K3s

Make sure RKE2 was uninstalled in the Cleanup section of the last lab, then run the command below to install K3s.

`curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" K3S_KUBECONFIG_OUTPUT="/ab/kubeconfig/k3s.yml" INSTALL_K3S_VERSION=v1.22.16+k3s1 sh - && export KUBECONFIG=/ab/kubeconfig/k3s.yml`  {{ execute }}

#### What's Happening Here?
The command above is doing the following:
1. Downloading the K3s install script
2. Setting the K3S_KUBECONFIG_MODE environmental variable to 644
3. Setting the K3S_KUBECONFIG_OUTPUT environmental variable to /ab/kubeconfig/k3s.yml
4. Setting the INSTALL_K3S_VERSION environmental variable to v1.22.16+k3s1
5. Running the install script
6. Exporting the KUBECONFIG environmental variable to /ab/kubeconfig/k3s.yml so that `kubectl` can access the K3s cluster

> **NOTE:**<BR/>The K3s installation is very similar to RKE2. Both applications share the similar install methods and have similar commands. K3s has additional environmental variables and can autostart on install.

> **NOTE:**<BR/>Unlike RKE2, K3s has diverged from upstream Kubernetes in order to optimize for edge deployments. RKE2 stays closely aligned with upstream Kubernetes.

<br/>

---

<br/>

### Verify the K3s Deployment

Using Kubectl, verify that K3s is deployed and running:

`kubectl get nodes`  {{ execute }}

> **NOTE:**<BR/>The node may be in the `NotReady` state until the installation is complete. It may be required to run the command multiple times until the node is in the `Ready` state, or to pair it with the `watch` command.

> **NOTE:**<BR/>While it is recommended to run K3s with multiple nodes, the K3s cluster demo deployed here has a single node due to this lab environment running on a single node. Multiple instances of K3s cannot be ran on the same machine due to port conflicts.

<br/>

---

<br/>

### Information on K3s and High Availability

The steps required to deploy K3s in a highly available configuration are more involved, but still straight forward. Below are links to the K3s documentation on how to deploy K3s in a highly available configuration.

* [HA with External DB](https://rancher.com/docs/k3s/latest/en/architecture/#high-availability-k3s-server-with-an-external-db)
* [HA with Embedded DB](https://rancher.com/docs/k3s/latest/en/installation/ha-embedded/)

> **NOTE:**<BR/>Leveraging automation tools such as Terraform and Ansible allows users to create repeatable and consistent deployments, as well as expanding and upgrading clusters to one or more environments as needed.

<br/>
<br/>

---
---

## Lab 2 - Upgrading K3s from the Rancher UI

K3s currently supports upgrading from the Rancher UI. This is a great feature for edge deployments where you may not have access to the command line.

1. Add the K3s cluster to Rancher using the name `k3s-cluster`
2. Once the `k3s-cluster` shows as `Active` in the Rancher UI, click the kebab menu (3 vertical dots) on the right next to the cluster and select `Edit config`.
3. At the bottom, expand `K3S Options`.
4. Under `Kubernetes Version`, click the dropdown.
5. Select a newer version of K3s.
6. Click the blue `Save` button at the bottom of the screen.

> **NOTE:**<BR/>The lab installed K3s version `v1.22.16+k3s1`. Rancher will suggest newer versions of K3s to deploy, such as the same minor version but incremented with security fixes or additional major/minor versions with new features.

> **NOTE:**<BR/>The upgrade process will take a few minutes to complete. You can watch the progress in the Rancher UI. The upgrade process will also cause all workloads running on the server during the upgrade to be unavailable until the upgrade is complete. This is because this is a single node cluster. To avoid this, a multi-node cluster should be used.


<br/>

---

<br/>

### Cleanup: Uninstall K3s
Execute the following commands to remove K3s from the lab server:

`sudo /usr/local/bin/k3s-uninstall.sh && rm /ab/k3s*` {{ execute }}

#### What's Happening Here?
The above commands are performing the following actions:
1. Uninstalling K3s using the uninstall script that is automatically generated during the installation process.
2. Removing the kubeconfig file that was created as part of the initial provisioning commands.

<br/>
<br/>

---
---

**Congrats! You have successfully completed the K3s lab! You may now proceed with the rest of the course.**
